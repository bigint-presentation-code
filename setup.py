from setuptools import setup, find_packages
from pathlib import Path

README = Path(__file__).with_name('README.md').read_text("UTF-8")

version = '0.0.1'

cprop = "git+https://git.libre-soc.org/git/cached-property.git@1.5.2" \
        "#egg=cached-property-1.5.2"

install_requires = [
    "libresoc-nmutil",
    'libresoc-openpower-isa',
    # git url needed for having `pip3 install -e .` install from libre-soc git
    'cached-property@'+cprop,
]

# git url needed for having `setup.py develop` install from libre-soc git
dependency_links = [
    cprop,
]

setup(
    name='libresoc-bigint-presentation',
    version=version,
    description="code for big-integer presentation for OpenPOWER Summit 2022",
    long_description=README,
    long_description_content_type='text/markdown',
    classifiers=[
        "Topic :: Software Development :: Libraries",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
        "Programming Language :: Python :: 3",
    ],
    author='Jacob Lifshay',
    author_email='programmerjake@gmail.com',
    url='https://git.libre-soc.org/?p=bigint-presentation-code.git',
    license='GPLv3+',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    include_package_data=True,
    zip_safe=False,
    install_requires=install_requires,
    dependency_links=dependency_links,
)
