The old python register allocator is too slow and has poor abstractions and produces poor output because it starts by splitting all input variables with copies and hopes the register allocator can merge them back together, the new allocator will split as needed rather than splitting everything and hoping for the best.

Register allocator written in Rust, since Python is just too slow -- we can't expect everyone working on our project to understand how the register allocator works (too much deep compiler magic (TM)), so not having it written in Python isn't as much of a problem.

Rust also has better low-cost abstractions, allowing me to more easily write the register allocation algorithm.

As part of this rewrite, I'm using a register allocation algorithm inspired by the regalloc2 crate and combining it with the previously tested algorithms for allocating ranges.

The register allocator will not use PyO3 or Maturin, since those have caused problems with others needing help to figure out how they work.

Instead, it will accept the problem in JSON on stdin, and write the solution or error as JSON on stdout. The python code will use a sub-process to call the register allocator.