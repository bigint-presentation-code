#![no_main]
use bigint_presentation_code_register_allocator::{
    interned::{GlobalState, Intern},
    loc_set::LocSet,
};
use libfuzzer_sys::fuzz_target;

fuzz_target!(|data: (LocSet, LocSet)| {
    GlobalState::scope(|| {
        GlobalState::get(|fast_global_state| {
            GlobalState::scope(|| {
                GlobalState::get(|reference_global_state| {
                    let (a, b) = data;
                    let a_fast = a.to_interned(fast_global_state);
                    let a_reference = a.into_interned(reference_global_state);
                    let b_fast = b.to_interned(fast_global_state);
                    let b_reference = b.into_interned(reference_global_state);
                    if let Some(loc) = b_fast.iter().next() {
                        let fast = a_fast.clone().max_conflicts_with(loc, fast_global_state);
                        let reference = a_reference
                            .clone()
                            .max_conflicts_with(loc, reference_global_state);
                        assert_eq!(fast, reference, "a={a_fast:?} loc={loc:?}");
                    }
                    let fast = a_fast
                        .clone()
                        .max_conflicts_with(b_fast.clone(), fast_global_state);
                    let reference =
                        a_reference.max_conflicts_with(b_reference, reference_global_state);
                    assert_eq!(fast, reference, "a={a_fast:?} b={b_fast:?}");
                })
            })
        })
    })
});
