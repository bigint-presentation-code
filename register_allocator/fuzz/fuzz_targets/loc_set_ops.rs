#![no_main]
use bigint_presentation_code_register_allocator::{loc::Loc, loc_set::LocSet};
use libfuzzer_sys::fuzz_target;
use std::{
    collections::HashSet,
    ops::{BitAnd, BitAndAssign, BitOr, BitOrAssign, BitXor, BitXorAssign, Sub, SubAssign},
};

fn check_op(
    lhs_hash_set: &HashSet<Loc>,
    rhs_hash_set: &HashSet<Loc>,
    op: impl Fn(LocSet, LocSet) -> LocSet,
    expected: impl IntoIterator<Item = Loc>,
) {
    let lhs = LocSet::from_iter(lhs_hash_set.iter().copied());
    let rhs = LocSet::from_iter(rhs_hash_set.iter().copied());
    let result = op(lhs, rhs);
    let result: Vec<Loc> = result.iter().collect();
    let mut expected = Vec::from_iter(expected);
    expected.sort();
    assert_eq!(result, expected);
}

macro_rules! check_all_op_combos {
    (
        $lhs_hash_set:expr, $rhs_hash_set:expr, $expected:expr,
        $bin_op:ident::$bin_op_fn:ident(),
        $bin_assign_op:ident::$bin_assign_op_fn:ident(),
        $(
            #[rev]
            $bin_assign_rev_op_fn:ident(),
        )?
    ) => {
        check_op(
            $lhs_hash_set,
            $rhs_hash_set,
            |lhs, rhs| $bin_op::$bin_op_fn(lhs, rhs),
            $expected,
        );
        check_op(
            $lhs_hash_set,
            $rhs_hash_set,
            |lhs, rhs| $bin_op::$bin_op_fn(&lhs, rhs),
            $expected,
        );
        check_op(
            $lhs_hash_set,
            $rhs_hash_set,
            |lhs, rhs| $bin_op::$bin_op_fn(lhs, &rhs),
            $expected,
        );
        check_op(
            $lhs_hash_set,
            $rhs_hash_set,
            |lhs, rhs| $bin_op::$bin_op_fn(&lhs, &rhs),
            $expected,
        );
        check_op(
            $lhs_hash_set,
            $rhs_hash_set,
            |mut lhs, rhs| {
                $bin_assign_op::$bin_assign_op_fn(&mut lhs, &rhs);
                lhs
            },
            $expected,
        );
        check_op(
            $lhs_hash_set,
            $rhs_hash_set,
            |mut lhs, rhs| {
                $bin_assign_op::$bin_assign_op_fn(&mut lhs, rhs);
                lhs
            },
            $expected,
        );
        $(check_op(
            $lhs_hash_set,
            $rhs_hash_set,
            |lhs, mut rhs| {
                rhs.$bin_assign_rev_op_fn(&lhs);
                lhs
            },
            $expected,
        );)?
    };
}

fuzz_target!(|data: (HashSet<Loc>, HashSet<Loc>)| {
    let (lhs_hash_set, rhs_hash_set) = data;
    check_all_op_combos!(
        &lhs_hash_set,
        &rhs_hash_set,
        lhs_hash_set.intersection(&rhs_hash_set).copied(),
        BitAnd::bitand(),
        BitAndAssign::bitand_assign(),
    );
    check_all_op_combos!(
        &lhs_hash_set,
        &rhs_hash_set,
        lhs_hash_set.union(&rhs_hash_set).copied(),
        BitOr::bitor(),
        BitOrAssign::bitor_assign(),
    );
    check_all_op_combos!(
        &lhs_hash_set,
        &rhs_hash_set,
        lhs_hash_set.symmetric_difference(&rhs_hash_set).copied(),
        BitXor::bitxor(),
        BitXorAssign::bitxor_assign(),
    );
    check_all_op_combos!(
        &lhs_hash_set,
        &rhs_hash_set,
        lhs_hash_set.difference(&rhs_hash_set).copied(),
        Sub::sub(),
        SubAssign::sub_assign(),
    );
});
