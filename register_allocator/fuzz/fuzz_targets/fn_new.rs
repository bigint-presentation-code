#![no_main]

use bigint_presentation_code_register_allocator::{
    function::{FnFields, Function},
    interned::GlobalState,
};
use libfuzzer_sys::{
    arbitrary::{Arbitrary, Unstructured},
    fuzz_target, Corpus,
};

fuzz_target!(|data: &[u8]| -> Corpus {
    GlobalState::scope(|| {
        let u = Unstructured::new(data);
        let Ok(fields) = FnFields::arbitrary_take_rest(u) else {
            return Corpus::Reject;
        };
        let fields_str = format!("{fields:#?}");
        if let Err(e) = Function::new(fields) {
            panic!("{fields_str}\nerror: {e}");
        }
        Corpus::Keep
    })
});
