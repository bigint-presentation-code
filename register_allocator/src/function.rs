use crate::{
    error::{BlockParamIdxOutOfRange, Error, OperandIdxOutOfRange, Result},
    index::{
        BlockIdx, BlockParamIdx, Entries, InstIdx, InstRange, OperandIdx, SSAValIdx, TryIndex,
        TryIndexMut,
    },
    interned::{GlobalState, Intern, Interned},
    loc::{BaseTy, Loc, Ty},
    loc_set::LocSet,
};
use arbitrary::Arbitrary;
use core::fmt;
use enum_map::Enum;
use hashbrown::HashSet;
use petgraph::{
    algo::dominators,
    visit::{GraphBase, GraphProp, IntoNeighbors, VisitMap, Visitable},
    Directed,
};
use serde::{Deserialize, Serialize};
use smallvec::SmallVec;
use std::{
    collections::{btree_map, BTreeMap, BTreeSet},
    mem,
};

#[derive(Copy, Clone, PartialEq, Eq, Debug, Hash, Serialize, Deserialize)]
pub enum SSAValDef {
    BlockParam {
        block: BlockIdx,
        param_idx: BlockParamIdx,
    },
    Operand {
        inst: InstIdx,
        operand_idx: OperandIdx,
    },
}

impl SSAValDef {
    pub const fn invalid() -> Self {
        SSAValDef::BlockParam {
            block: BlockIdx::ENTRY_BLOCK,
            param_idx: BlockParamIdx::new(!0),
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Hash, Serialize, Deserialize)]
pub struct BranchSuccParamUse {
    pub branch_inst: InstIdx,
    pub succ: BlockIdx,
    pub param_idx: BlockParamIdx,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Hash, Serialize, Deserialize)]
pub struct OperandUse {
    pub inst: InstIdx,
    pub operand_idx: OperandIdx,
}

#[derive(Clone, PartialEq, Eq, Debug, Hash, Serialize, Deserialize)]
pub struct SSAVal {
    pub ty: Ty,
    #[serde(skip, default = "SSAValDef::invalid")]
    pub def: SSAValDef,
    #[serde(skip)]
    pub operand_uses: BTreeSet<OperandUse>,
    #[serde(skip)]
    pub branch_succ_param_uses: BTreeSet<BranchSuccParamUse>,
}

impl SSAVal {
    fn validate(&self, ssa_val_idx: SSAValIdx, func: &FnFields) -> Result<()> {
        let Self {
            ty: _,
            def,
            operand_uses,
            branch_succ_param_uses,
        } = self;
        match *def {
            SSAValDef::BlockParam { block, param_idx } => {
                let block_param = func.try_get_block_param(block, param_idx)?;
                if ssa_val_idx != block_param {
                    return Err(Error::MismatchedBlockParamDef {
                        ssa_val_idx,
                        block,
                        param_idx,
                    });
                }
            }
            SSAValDef::Operand { inst, operand_idx } => {
                let operand = func.try_get_operand(inst, operand_idx)?;
                if ssa_val_idx != operand.ssa_val {
                    return Err(Error::SSAValDefIsNotOperandsSSAVal {
                        ssa_val_idx,
                        inst,
                        operand_idx,
                    });
                }
            }
        }
        for &OperandUse { inst, operand_idx } in operand_uses {
            let operand = func.try_get_operand(inst, operand_idx)?;
            if ssa_val_idx != operand.ssa_val {
                return Err(Error::SSAValUseIsNotOperandsSSAVal {
                    ssa_val_idx,
                    inst,
                    operand_idx,
                });
            }
        }
        for &BranchSuccParamUse {
            branch_inst,
            succ,
            param_idx,
        } in branch_succ_param_uses
        {
            if ssa_val_idx != func.try_get_branch_target_param(branch_inst, succ, param_idx)? {
                return Err(Error::MismatchedBranchTargetBlockParamUse {
                    ssa_val_idx,
                    branch_inst,
                    tgt_block: succ,
                    param_idx,
                });
            }
        }
        Ok(())
    }
}

#[derive(
    Copy,
    Clone,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Debug,
    Hash,
    Serialize,
    Deserialize,
    Arbitrary,
    Enum,
)]
#[repr(u8)]
pub enum InstStage {
    Early = 0,
    Late = 1,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(try_from = "SerializedProgPoint", into = "SerializedProgPoint")]
pub struct ProgPoint(usize);

impl ProgPoint {
    pub const fn new(inst: InstIdx, stage: InstStage) -> Self {
        const_unwrap_res!(Self::try_new(inst, stage))
    }
    pub const fn try_new(inst: InstIdx, stage: InstStage) -> Result<Self> {
        let Some(inst) = inst.get().checked_shl(1) else {
            return Err(Error::InstIdxTooBig);
        };
        Ok(Self(inst | stage as usize))
    }
    pub const fn inst(self) -> InstIdx {
        InstIdx::new(self.0 >> 1)
    }
    pub const fn stage(self) -> InstStage {
        if self.0 & 1 != 0 {
            InstStage::Late
        } else {
            InstStage::Early
        }
    }
    pub const fn next(self) -> Self {
        Self(self.0 + 1)
    }
    pub const fn prev(self) -> Self {
        Self(self.0 - 1)
    }
    pub const fn as_usize(self) -> usize {
        self.0
    }
    pub const fn from_usize(value: usize) -> Self {
        Self(value)
    }
}

impl fmt::Debug for ProgPoint {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("ProgPoint")
            .field("inst", &self.inst())
            .field("stage", &self.stage())
            .finish()
    }
}

#[derive(Serialize, Deserialize)]
struct SerializedProgPoint {
    inst: InstIdx,
    stage: InstStage,
}

impl From<ProgPoint> for SerializedProgPoint {
    fn from(value: ProgPoint) -> Self {
        Self {
            inst: value.inst(),
            stage: value.stage(),
        }
    }
}

impl TryFrom<SerializedProgPoint> for ProgPoint {
    type Error = Error;

    fn try_from(value: SerializedProgPoint) -> Result<Self, Self::Error> {
        ProgPoint::try_new(value.inst, value.stage)
    }
}

#[derive(
    Copy,
    Clone,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Debug,
    Hash,
    Serialize,
    Deserialize,
    Arbitrary,
    Enum,
)]
#[repr(u8)]
pub enum OperandKind {
    Use = 0,
    Def = 1,
}

#[derive(
    Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Hash, Serialize, Deserialize, Arbitrary,
)]
pub enum Constraint {
    /// any register or stack location
    Any,
    /// r1-r32
    BaseGpr,
    /// r2,r4,r6,r8,...r126
    SVExtra2VGpr,
    /// r1-63
    SVExtra2SGpr,
    /// r1-127
    SVExtra3Gpr,
    /// any stack location
    Stack,
    FixedLoc(Loc),
}

impl Constraint {
    pub fn is_any(&self) -> bool {
        matches!(self, Self::Any)
    }
    pub fn fixed_loc(&self) -> Option<Loc> {
        match *self {
            Constraint::Any
            | Constraint::BaseGpr
            | Constraint::SVExtra2VGpr
            | Constraint::SVExtra2SGpr
            | Constraint::SVExtra3Gpr
            | Constraint::Stack => None,
            Constraint::FixedLoc(v) => Some(v),
        }
    }
    pub fn non_fixed_choices_for_ty(ty: Ty) -> &'static [Constraint] {
        match (ty.base_ty, ty.reg_len.get()) {
            (BaseTy::Bits64, 1) => &[
                Constraint::Any,
                Constraint::BaseGpr,
                Constraint::SVExtra2SGpr,
                Constraint::SVExtra2VGpr,
                Constraint::SVExtra3Gpr,
                Constraint::Stack,
            ],
            (BaseTy::Bits64, _) => &[
                Constraint::Any,
                Constraint::SVExtra2VGpr,
                Constraint::SVExtra3Gpr,
                Constraint::Stack,
            ],
            (BaseTy::Ca, _) | (BaseTy::VlMaxvl, _) => &[Constraint::Any, Constraint::Stack],
        }
    }
    pub fn arbitrary_with_ty(
        ty: Ty,
        u: &mut arbitrary::Unstructured<'_>,
    ) -> arbitrary::Result<Self> {
        let non_fixed_choices = Self::non_fixed_choices_for_ty(ty);
        if let Some(&retval) = non_fixed_choices.get(u.choose_index(non_fixed_choices.len() + 1)?) {
            Ok(retval)
        } else {
            Ok(Constraint::FixedLoc(Loc::arbitrary_with_ty(ty, u)?))
        }
    }
    pub fn check_for_ty_mismatch(&self, ty: Ty) -> Result<(), ()> {
        match self {
            Constraint::Any | Constraint::Stack => {}
            Constraint::BaseGpr | Constraint::SVExtra2SGpr => {
                if ty != Ty::scalar(BaseTy::Bits64) {
                    return Err(());
                }
            }
            Constraint::SVExtra2VGpr | Constraint::SVExtra3Gpr => {
                if ty.base_ty != BaseTy::Bits64 {
                    return Err(());
                }
            }
            Constraint::FixedLoc(loc) => {
                if ty != loc.ty() {
                    return Err(());
                }
            }
        }
        Ok(())
    }
}

impl Default for Constraint {
    fn default() -> Self {
        Self::Any
    }
}

#[derive(
    Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Hash, Serialize, Deserialize, Default,
)]
#[serde(try_from = "OperandKind", into = "OperandKind")]
pub struct OperandKindDefOnly;

impl TryFrom<OperandKind> for OperandKindDefOnly {
    type Error = Error;

    fn try_from(value: OperandKind) -> Result<Self, Self::Error> {
        match value {
            OperandKind::Use => Err(Error::OperandKindMustBeDef),
            OperandKind::Def => Ok(Self),
        }
    }
}

impl From<OperandKindDefOnly> for OperandKind {
    fn from(_value: OperandKindDefOnly) -> Self {
        Self::Def
    }
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Hash, Serialize, Deserialize)]
#[serde(untagged)]
pub enum KindAndConstraint {
    Reuse {
        kind: OperandKindDefOnly,
        reuse_operand_idx: OperandIdx,
    },
    Constraint {
        kind: OperandKind,
        #[serde(default, skip_serializing_if = "Constraint::is_any")]
        constraint: Constraint,
    },
}

impl KindAndConstraint {
    pub fn kind(self) -> OperandKind {
        match self {
            Self::Reuse { .. } => OperandKind::Def,
            Self::Constraint { kind, .. } => kind,
        }
    }
    pub fn is_reuse(self) -> bool {
        matches!(self, Self::Reuse { .. })
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug, Hash, Serialize, Deserialize)]
pub struct Operand {
    pub ssa_val: SSAValIdx,
    #[serde(flatten)]
    pub kind_and_constraint: KindAndConstraint,
    pub stage: InstStage,
}

impl Operand {
    pub fn try_get_reuse_src<'f>(
        &self,
        inst: InstIdx,
        func: &'f FnFields,
    ) -> Result<Option<&'f Operand>> {
        if let KindAndConstraint::Reuse {
            reuse_operand_idx, ..
        } = self.kind_and_constraint
        {
            Ok(Some(func.try_get_operand(inst, reuse_operand_idx)?))
        } else {
            Ok(None)
        }
    }
    pub fn try_constraint(&self, inst: InstIdx, func: &FnFields) -> Result<Constraint> {
        Ok(match self.kind_and_constraint {
            KindAndConstraint::Reuse {
                kind: _,
                reuse_operand_idx,
            } => {
                let operand = func.try_get_operand(inst, reuse_operand_idx)?;
                match operand.kind_and_constraint {
                    KindAndConstraint::Reuse { .. }
                    | KindAndConstraint::Constraint {
                        kind: OperandKind::Def,
                        ..
                    } => {
                        return Err(Error::ReuseTargetOperandMustBeUse {
                            inst,
                            reuse_target_operand_idx: reuse_operand_idx,
                        })
                    }
                    KindAndConstraint::Constraint {
                        kind: OperandKind::Use,
                        constraint,
                    } => constraint,
                }
            }
            KindAndConstraint::Constraint { constraint, .. } => constraint,
        })
    }
    pub fn constraint(&self, inst: InstIdx, func: &Function) -> Constraint {
        self.try_constraint(inst, func).unwrap()
    }
    fn validate(
        self,
        block: BlockIdx,
        inst: InstIdx,
        operand_idx: OperandIdx,
        func: &FnFields,
        global_state: &GlobalState,
    ) -> Result<()> {
        let Self {
            ssa_val: ssa_val_idx,
            kind_and_constraint,
            stage: _,
        } = self;
        let ssa_val = func
            .ssa_vals
            .try_index(ssa_val_idx)
            .map_err(|e| e.with_block_inst_and_operand(block, inst, operand_idx))?;
        match kind_and_constraint.kind() {
            OperandKind::Use => {
                if !ssa_val
                    .operand_uses
                    .contains(&OperandUse { inst, operand_idx })
                {
                    return Err(Error::MissingOperandUse {
                        ssa_val_idx,
                        inst,
                        operand_idx,
                    });
                }
            }
            OperandKind::Def => {
                let def = SSAValDef::Operand { inst, operand_idx };
                if ssa_val.def != def {
                    return Err(Error::OperandDefIsNotSSAValDef {
                        ssa_val_idx,
                        inst,
                        operand_idx,
                    });
                }
            }
        }
        if let KindAndConstraint::Reuse {
            kind: _,
            reuse_operand_idx,
        } = self.kind_and_constraint
        {
            let reuse_src = func.try_get_operand(inst, reuse_operand_idx)?;
            let reuse_src_ssa_val = func
                .ssa_vals
                .try_index(reuse_src.ssa_val)
                .map_err(|e| e.with_block_inst_and_operand(block, inst, reuse_operand_idx))?;
            if ssa_val.ty != reuse_src_ssa_val.ty {
                return Err(Error::ReuseOperandTyMismatch {
                    inst,
                    tgt_operand_idx: operand_idx,
                    src_operand_idx: reuse_operand_idx,
                    src_ty: reuse_src_ssa_val.ty,
                    tgt_ty: ssa_val.ty,
                });
            }
        }
        let constraint = self.try_constraint(inst, func)?;
        constraint
            .check_for_ty_mismatch(ssa_val.ty)
            .map_err(|()| Error::ConstraintTyMismatch {
                ssa_val_idx,
                inst,
                operand_idx,
            })?;
        if let Some(fixed_loc) = constraint.fixed_loc() {
            if func
                .insts
                .try_index(inst)?
                .clobbers
                .clone()
                .conflicts_with(fixed_loc, global_state)
            {
                return Err(Error::FixedLocConflictsWithClobbers { inst, operand_idx });
            }
        }
        Ok(())
    }
}

/// copy concatenates all `srcs` together and de-concatenates the result into all `dests`.
#[derive(Clone, PartialEq, Eq, Debug, Hash, Serialize, Deserialize)]
pub struct CopyInstKind {
    pub src_operand_idxs: Vec<OperandIdx>,
    pub dest_operand_idxs: Vec<OperandIdx>,
    pub copy_ty: Ty,
}

impl CopyInstKind {
    fn calc_copy_ty(
        operand_idxs: &[OperandIdx],
        inst: InstIdx,
        func: &FnFields,
    ) -> Result<Option<Ty>> {
        let mut retval: Option<Ty> = None;
        for &operand_idx in operand_idxs {
            let operand = func.try_get_operand(inst, operand_idx)?;
            let ssa_val = func
                .ssa_vals
                .try_index(operand.ssa_val)
                .map_err(|e| e.with_inst_and_operand(inst, operand_idx))?;
            retval = Some(match retval {
                Some(retval) => retval.try_concat(ssa_val.ty)?,
                None => ssa_val.ty,
            });
        }
        Ok(retval)
    }
}

#[derive(Clone, PartialEq, Eq, Debug, Hash, Serialize, Deserialize)]
pub struct BlockTermInstKind {
    pub succs_and_params: BTreeMap<BlockIdx, Vec<SSAValIdx>>,
}

#[derive(Clone, PartialEq, Eq, Debug, Hash, Serialize, Deserialize)]
pub enum InstKind {
    Normal,
    Copy(CopyInstKind),
    BlockTerm(BlockTermInstKind),
}

impl InstKind {
    pub fn is_normal(&self) -> bool {
        matches!(self, Self::Normal)
    }
    pub fn is_block_term(&self) -> bool {
        matches!(self, Self::BlockTerm { .. })
    }
    pub fn is_copy(&self) -> bool {
        matches!(self, Self::Copy { .. })
    }
    pub fn block_term(&self) -> Option<&BlockTermInstKind> {
        match self {
            InstKind::BlockTerm(v) => Some(v),
            _ => None,
        }
    }
    pub fn block_term_mut(&mut self) -> Option<&mut BlockTermInstKind> {
        match self {
            InstKind::BlockTerm(v) => Some(v),
            _ => None,
        }
    }
    pub fn copy(&self) -> Option<&CopyInstKind> {
        match self {
            InstKind::Copy(v) => Some(v),
            _ => None,
        }
    }
}

impl Default for InstKind {
    fn default() -> Self {
        InstKind::Normal
    }
}

fn loc_set_is_empty(clobbers: &Interned<LocSet>) -> bool {
    clobbers.is_empty()
}

fn empty_loc_set() -> Interned<LocSet> {
    GlobalState::get(|global_state| LocSet::default().into_interned(global_state))
}

#[derive(Clone, PartialEq, Eq, Debug, Hash, Serialize, Deserialize)]
pub struct Inst {
    #[serde(default, skip_serializing_if = "InstKind::is_normal")]
    pub kind: InstKind,
    pub operands: Vec<Operand>,
    #[serde(default = "empty_loc_set", skip_serializing_if = "loc_set_is_empty")]
    pub clobbers: Interned<LocSet>,
}

impl Inst {
    fn validate(
        &self,
        block: BlockIdx,
        inst: InstIdx,
        func: &FnFields,
        global_state: &GlobalState,
    ) -> Result<()> {
        let Self {
            kind,
            operands,
            clobbers: _,
        } = self;
        let is_at_end_of_block = func.blocks[block].insts.last() == Some(inst);
        if kind.is_block_term() != is_at_end_of_block {
            return Err(if is_at_end_of_block {
                Error::BlocksLastInstMustBeTerm { term_idx: inst }
            } else {
                Error::TermInstOnlyAllowedAtBlockEnd { inst_idx: inst }
            });
        }
        for (operand_idx, operand) in operands.entries() {
            operand.validate(block, inst, operand_idx, func, global_state)?;
        }
        match kind {
            InstKind::Normal => {}
            InstKind::Copy(CopyInstKind {
                src_operand_idxs,
                dest_operand_idxs,
                copy_ty,
            }) => {
                let mut seen_dest_operands = SmallVec::<[bool; 16]>::new();
                seen_dest_operands.resize(operands.len(), false);
                for &dest_operand_idx in dest_operand_idxs {
                    let seen_dest_operand = seen_dest_operands
                        .get_mut(dest_operand_idx.get())
                        .ok_or_else(|| {
                            OperandIdxOutOfRange {
                                idx: dest_operand_idx,
                            }
                            .with_inst(inst)
                        })?;
                    if mem::replace(seen_dest_operand, true) {
                        return Err(Error::DupCopyDestOperand {
                            inst,
                            operand_idx: dest_operand_idx,
                        });
                    }
                }
                if Some(*copy_ty) != CopyInstKind::calc_copy_ty(&src_operand_idxs, inst, func)? {
                    return Err(Error::CopySrcTyMismatch { inst });
                }
                if Some(*copy_ty) != CopyInstKind::calc_copy_ty(&dest_operand_idxs, inst, func)? {
                    return Err(Error::CopyDestTyMismatch { inst });
                }
            }
            InstKind::BlockTerm(BlockTermInstKind { succs_and_params }) => {
                for (&succ_idx, params) in succs_and_params {
                    let succ = func.blocks.try_index(succ_idx)?;
                    if !succ.preds.contains(&block) {
                        return Err(Error::SrcBlockMissingFromBranchTgtBlocksPreds {
                            src_block: block,
                            branch_inst: inst,
                            tgt_block: succ_idx,
                        });
                    }
                    if succ.params.len() != params.len() {
                        return Err(Error::BranchSuccParamCountMismatch {
                            inst,
                            succ: succ_idx,
                            block_param_count: succ.params.len(),
                            branch_param_count: params.len(),
                        });
                    }
                    for ((param_idx, &branch_ssa_val_idx), &block_ssa_val_idx) in
                        params.entries().zip(&succ.params)
                    {
                        let branch_ssa_val = func
                            .ssa_vals
                            .try_index(branch_ssa_val_idx)
                            .map_err(|e| e.with_inst_succ_and_param(inst, succ_idx, param_idx))?;
                        let block_ssa_val = func
                            .ssa_vals
                            .try_index(block_ssa_val_idx)
                            .map_err(|e| e.with_block_and_param(succ_idx, param_idx))?;
                        if !branch_ssa_val
                            .branch_succ_param_uses
                            .contains(&BranchSuccParamUse {
                                branch_inst: inst,
                                succ: succ_idx,
                                param_idx,
                            })
                        {
                            return Err(Error::MissingBranchSuccParamUse {
                                ssa_val_idx: branch_ssa_val_idx,
                                inst,
                                succ: succ_idx,
                                param_idx,
                            });
                        }
                        if block_ssa_val.ty != branch_ssa_val.ty {
                            return Err(Error::BranchSuccParamTyMismatch {
                                inst,
                                succ: succ_idx,
                                param_idx,
                                block_param_ty: block_ssa_val.ty,
                                branch_param_ty: branch_ssa_val.ty,
                            });
                        }
                    }
                }
            }
        }
        Ok(())
    }
    pub fn try_get_operand(&self, inst: InstIdx, operand_idx: OperandIdx) -> Result<&Operand> {
        self.operands
            .try_index(operand_idx)
            .map_err(|e| e.with_inst(inst).into())
    }
}

#[derive(Clone, PartialEq, Eq, Debug, Hash, Serialize, Deserialize)]
pub struct Block {
    pub params: Vec<SSAValIdx>,
    pub insts: InstRange,
    pub preds: BTreeSet<BlockIdx>,
    pub immediate_dominator: Option<BlockIdx>,
}

impl Block {
    fn validate(&self, block: BlockIdx, func: &FnFields, global_state: &GlobalState) -> Result<()> {
        let Self {
            params,
            insts,
            preds,
            immediate_dominator: _, // validated by Function::new_with_global_state
        } = self;
        const _: () = assert!(BlockIdx::ENTRY_BLOCK.get() == 0);
        let expected_start = if block == BlockIdx::ENTRY_BLOCK {
            InstIdx::new(0)
        } else {
            func.blocks[block.prev()].insts.end
        };
        if insts.start != expected_start {
            return Err(Error::BlockHasInvalidStart {
                start: insts.start,
                expected_start,
            });
        }
        let term_inst_idx = insts.last().ok_or(Error::BlockIsEmpty { block })?;
        func.insts
            .get(term_inst_idx.get())
            .ok_or(Error::BlockEndOutOfRange { end: insts.end })?;
        if block.get() == func.blocks.len() - 1 && insts.end.get() != func.insts.len() {
            return Err(Error::InstHasNoBlock { inst: insts.end });
        }
        if block == BlockIdx::ENTRY_BLOCK {
            if !params.is_empty() {
                return Err(Error::EntryBlockCantHaveParams);
            }
            if !preds.is_empty() {
                return Err(Error::EntryBlockCantHavePreds);
            }
        }
        for inst in *insts {
            func.insts[inst].validate(block, inst, func, global_state)?;
        }
        for (param_idx, &ssa_val_idx) in params.entries() {
            let ssa_val = func
                .ssa_vals
                .try_index(ssa_val_idx)
                .map_err(|e| e.with_block_and_param(block, param_idx))?;
            let def = SSAValDef::BlockParam { block, param_idx };
            if ssa_val.def != def {
                return Err(Error::MismatchedBlockParamDef {
                    ssa_val_idx,
                    block,
                    param_idx,
                });
            }
        }
        for &pred in preds {
            let (term_inst, BlockTermInstKind { succs_and_params }) =
                func.try_get_block_term_inst_and_kind(pred)?;
            if !succs_and_params.contains_key(&block) {
                return Err(Error::PredMissingFromPredsTermBranchsTargets {
                    src_block: pred,
                    branch_inst: term_inst,
                    tgt_block: block,
                });
            }
            if preds.len() > 1 && succs_and_params.len() > 1 {
                return Err(Error::CriticalEdgeNotAllowed {
                    src_block: pred,
                    branch_inst: term_inst,
                    tgt_block: block,
                });
            }
        }
        Ok(())
    }
}

validated_fields! {
    #[fields_ty = FnFields]
    #[derive(Clone, PartialEq, Eq, Debug, Hash)]
    pub struct Function {
        pub ssa_vals: Vec<SSAVal>,
        pub insts: Vec<Inst>,
        pub blocks: Vec<Block>,
        #[serde(skip)]
        /// map from blocks' start instruction's index to their block index, doesn't contain the entry block
        pub start_inst_to_block_map: BTreeMap<InstIdx, BlockIdx>,
    }
}

impl Function {
    pub fn new(fields: FnFields) -> Result<Self> {
        GlobalState::get(|global_state| Self::new_with_global_state(fields, global_state))
    }
    pub fn new_with_global_state(mut fields: FnFields, global_state: &GlobalState) -> Result<Self> {
        fields.fill_start_inst_to_block_map();
        fields.fill_ssa_defs_uses()?;
        let FnFields {
            ssa_vals,
            insts: _,
            blocks,
            start_inst_to_block_map: _,
        } = &fields;
        blocks
            .get(BlockIdx::ENTRY_BLOCK.get())
            .ok_or(Error::MissingEntryBlock)?;
        for (block_idx, block) in blocks.entries() {
            block.validate(block_idx, &fields, global_state)?;
        }
        let dominators = dominators::simple_fast(&fields, BlockIdx::ENTRY_BLOCK);
        for (block_idx, block) in blocks.entries() {
            let expected = dominators.immediate_dominator(block_idx);
            if block.immediate_dominator != expected {
                return Err(Error::IncorrectImmediateDominator {
                    block_idx,
                    found: block.immediate_dominator,
                    expected,
                });
            }
        }
        for (ssa_val_idx, ssa_val) in ssa_vals.entries() {
            ssa_val.validate(ssa_val_idx, &fields)?;
        }
        Ok(Self(fields))
    }
    pub fn entry_block(&self) -> &Block {
        &self.blocks[0]
    }
    pub fn block_term_kind(&self, block: BlockIdx) -> &BlockTermInstKind {
        self.insts[self.blocks[block].insts.last().unwrap()]
            .kind
            .block_term()
            .unwrap()
    }
}

impl FnFields {
    pub fn fill_start_inst_to_block_map(&mut self) {
        self.start_inst_to_block_map.clear();
        for (block_idx, block) in self.blocks.entries() {
            if block_idx != BlockIdx::ENTRY_BLOCK {
                self.start_inst_to_block_map
                    .insert(block.insts.start, block_idx);
            }
        }
    }
    pub fn fill_ssa_defs_uses(&mut self) -> Result<()> {
        for ssa_val in &mut self.ssa_vals {
            ssa_val.branch_succ_param_uses.clear();
            ssa_val.operand_uses.clear();
            ssa_val.def = SSAValDef::invalid();
        }
        for (block_idx, block) in self.blocks.entries() {
            for (param_idx, &param) in block.params.entries() {
                self.ssa_vals
                    .try_index_mut(param)
                    .map_err(|e| e.with_block_and_param(block_idx, param_idx))?
                    .def = SSAValDef::BlockParam {
                    block: block_idx,
                    param_idx,
                };
            }
        }
        for (inst_idx, inst) in self.insts.entries() {
            for (operand_idx, operand) in inst.operands.entries() {
                let ssa_val = self
                    .ssa_vals
                    .try_index_mut(operand.ssa_val)
                    .map_err(|e| e.with_inst_and_operand(inst_idx, operand_idx))?;
                match operand.kind_and_constraint.kind() {
                    OperandKind::Use => {
                        ssa_val.operand_uses.insert(OperandUse {
                            inst: inst_idx,
                            operand_idx,
                        });
                    }
                    OperandKind::Def => {
                        ssa_val.def = SSAValDef::Operand {
                            inst: inst_idx,
                            operand_idx,
                        };
                    }
                }
            }
            match &inst.kind {
                InstKind::Normal | InstKind::Copy(_) => {}
                InstKind::BlockTerm(BlockTermInstKind { succs_and_params }) => {
                    for (&succ, params) in succs_and_params {
                        for (param_idx, &param) in params.entries() {
                            let ssa_val = self.ssa_vals.try_index_mut(param).map_err(|e| {
                                e.with_inst_succ_and_param(inst_idx, succ, param_idx)
                            })?;
                            ssa_val.branch_succ_param_uses.insert(BranchSuccParamUse {
                                branch_inst: inst_idx,
                                succ,
                                param_idx,
                            });
                        }
                    }
                }
            }
        }
        Ok(())
    }
    pub fn try_get_operand(&self, inst: InstIdx, operand_idx: OperandIdx) -> Result<&Operand> {
        Ok(self
            .insts
            .try_index(inst)?
            .operands
            .try_index(operand_idx)
            .map_err(|e| e.with_inst(inst))?)
    }
    pub fn try_get_block_param(
        &self,
        block: BlockIdx,
        param_idx: BlockParamIdx,
    ) -> Result<SSAValIdx> {
        Ok(*self
            .blocks
            .try_index(block)?
            .params
            .try_index(param_idx)
            .map_err(|e| e.with_block(block))?)
    }
    pub fn try_get_block_term_inst_idx(&self, block: BlockIdx) -> Result<InstIdx> {
        self.blocks
            .try_index(block)?
            .insts
            .last()
            .ok_or(Error::BlockIsEmpty { block })
    }
    pub fn try_get_block_term_inst_and_kind(
        &self,
        block: BlockIdx,
    ) -> Result<(InstIdx, &BlockTermInstKind)> {
        let term_idx = self.try_get_block_term_inst_idx(block)?;
        let term_kind = self
            .insts
            .try_index(term_idx)?
            .kind
            .block_term()
            .ok_or(Error::BlocksLastInstMustBeTerm { term_idx })?;
        Ok((term_idx, term_kind))
    }
    pub fn try_get_block_term_inst_and_kind_mut(
        &mut self,
        block: BlockIdx,
    ) -> Result<(InstIdx, &mut BlockTermInstKind)> {
        let term_idx = self.try_get_block_term_inst_idx(block)?;
        let term_kind = self
            .insts
            .try_index_mut(term_idx)?
            .kind
            .block_term_mut()
            .ok_or(Error::BlocksLastInstMustBeTerm { term_idx })?;
        Ok((term_idx, term_kind))
    }
    pub fn try_get_branch_target_params(
        &self,
        branch_inst: InstIdx,
        succ: BlockIdx,
    ) -> Result<&[SSAValIdx]> {
        let inst = self.insts.try_index(branch_inst)?;
        let BlockTermInstKind { succs_and_params } = inst
            .kind
            .block_term()
            .ok_or(Error::InstIsNotBlockTerm { inst: branch_inst })?;
        Ok(succs_and_params
            .get(&succ)
            .ok_or(Error::BranchTargetNotFound {
                branch_inst,
                tgt_block: succ,
            })?)
    }
    pub fn try_get_branch_target_param(
        &self,
        branch_inst: InstIdx,
        succ: BlockIdx,
        param_idx: BlockParamIdx,
    ) -> Result<SSAValIdx> {
        Ok(*self
            .try_get_branch_target_params(branch_inst, succ)?
            .try_index(param_idx)
            .map_err(|e: BlockParamIdxOutOfRange| e.with_inst_and_succ(branch_inst, succ))?)
    }
    pub fn inst_to_block(&self, inst: InstIdx) -> BlockIdx {
        self.start_inst_to_block_map
            .range(..=inst)
            .next_back()
            .map(|v| *v.1)
            .unwrap_or(BlockIdx::ENTRY_BLOCK)
    }
}

impl GraphBase for FnFields {
    type EdgeId = (BlockIdx, BlockIdx);
    type NodeId = BlockIdx;
}

pub struct Neighbors<'a> {
    iter: Option<btree_map::Keys<'a, BlockIdx, Vec<SSAValIdx>>>,
}

impl Iterator for Neighbors<'_> {
    type Item = BlockIdx;

    fn next(&mut self) -> Option<Self::Item> {
        Some(*self.iter.as_mut()?.next()?)
    }
}

impl<'a> IntoNeighbors for &'a FnFields {
    type Neighbors = Neighbors<'a>;

    fn neighbors(self, block_idx: Self::NodeId) -> Self::Neighbors {
        Neighbors {
            iter: self
                .try_get_block_term_inst_and_kind(block_idx)
                .ok()
                .map(|(_, BlockTermInstKind { succs_and_params })| succs_and_params.keys()),
        }
    }
}

pub struct VisitedMap(HashSet<BlockIdx>);

impl VisitMap<BlockIdx> for VisitedMap {
    fn visit(&mut self, block: BlockIdx) -> bool {
        self.0.insert(block)
    }

    fn is_visited(&self, block: &BlockIdx) -> bool {
        self.0.contains(block)
    }
}

impl Visitable for FnFields {
    type Map = VisitedMap;

    fn visit_map(&self) -> Self::Map {
        VisitedMap(HashSet::new())
    }

    fn reset_map(&self, map: &mut Self::Map) {
        map.0.clear();
    }
}

impl GraphProp for FnFields {
    type EdgeType = Directed;
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::loc::TyFields;
    use std::num::NonZeroU32;

    #[test]
    fn test_constraint_non_fixed_choices_for_ty() {
        macro_rules! seen {
            (
                enum ConstraintWithoutFixedLoc {
                    $($field:ident,)*
                }
            ) => {
                #[derive(Default)]
                #[allow(non_snake_case)]
                struct Seen {
                    $($field: bool,)*
                }

                impl Seen {
                    fn add(&mut self, constraint: &Constraint) {
                        match constraint {
                            Constraint::FixedLoc(_) => {}
                            $(Constraint::$field => self.$field = true,)*
                        }
                    }
                    fn check(self) {
                        $(assert!(self.$field, "never seen field: {}", stringify!($field));)*
                    }
                }
            };
        }
        seen! {
            enum ConstraintWithoutFixedLoc {
                Any,
                BaseGpr,
                SVExtra2VGpr,
                SVExtra2SGpr,
                SVExtra3Gpr,
                Stack,
            }
        }
        let mut seen = Seen::default();
        for base_ty in 0..BaseTy::LENGTH {
            let base_ty = BaseTy::from_usize(base_ty);
            for reg_len in [1, 2, 100] {
                let reg_len = NonZeroU32::new(reg_len).unwrap();
                let ty = Ty::new_or_scalar(TyFields { base_ty, reg_len });
                let non_fixed_choices = Constraint::non_fixed_choices_for_ty(ty);
                assert_eq!(non_fixed_choices.first(), Some(&Constraint::Any));
                assert_eq!(non_fixed_choices.last(), Some(&Constraint::Stack));
                for constraint in non_fixed_choices {
                    assert_eq!(constraint.fixed_loc(), None);
                    seen.add(constraint);
                    if constraint.check_for_ty_mismatch(ty).is_err() {
                        panic!("constraint ty mismatch: constraint={constraint:?} ty={ty:?}");
                    }
                }
            }
        }
        seen.check();
    }
}
