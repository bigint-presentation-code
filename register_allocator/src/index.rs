use serde::{de::DeserializeOwned, Deserialize, Serialize};
use std::{
    fmt,
    hash::Hash,
    iter::FusedIterator,
    ops::{Index, IndexMut, Range},
};

use crate::{
    error::{
        BlockIdxOutOfRange, BlockParamIdxOutOfRange, InstIdxOutOfRange, OperandIdxOutOfRange,
        SSAValIdxOutOfRange,
    },
    function::{Block, Inst, Operand, SSAVal},
    live_range::LiveRange,
};

pub trait DisplayOptionIdx {
    type Type: fmt::Display;
    fn display_option_idx(self) -> Self::Type;
}

pub trait IndexTy: Copy + fmt::Debug + Ord + Hash + Serialize + DeserializeOwned {
    fn new(value: usize) -> Self;
    fn get(self) -> usize;
    fn next(self) -> Self {
        Self::new(self.get() + 1)
    }
    fn prev(self) -> Self {
        Self::new(self.get() - 1)
    }
}

#[derive(Debug, Clone)]
pub struct RangeIter<T: IndexTy>(pub Range<T>);

impl<T: IndexTy> RangeIter<T> {
    pub fn as_usize_range(&self) -> Range<usize> {
        self.0.start.get()..self.0.end.get()
    }
    pub fn from_usize_range(range: Range<usize>) -> Self {
        Self(T::new(range.start)..T::new(range.end))
    }
    pub fn with_self_as_usize_range<R, F: FnOnce(&mut Range<usize>) -> R>(&mut self, f: F) -> R {
        let mut range = self.as_usize_range();
        let retval = f(&mut range);
        *self = Self::from_usize_range(range);
        retval
    }
}

impl<T: IndexTy> Iterator for RangeIter<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.with_self_as_usize_range(|r| r.next().map(T::new))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.as_usize_range().size_hint()
    }

    fn nth(&mut self, n: usize) -> Option<Self::Item> {
        self.with_self_as_usize_range(|r| r.nth(n).map(T::new))
    }

    fn fold<B, F>(self, init: B, mut f: F) -> B
    where
        F: FnMut(B, Self::Item) -> B,
    {
        self.as_usize_range()
            .fold(init, move |a, v| f(a, T::new(v)))
    }
}

impl<T: IndexTy> FusedIterator for RangeIter<T> {}

impl<T: IndexTy> ExactSizeIterator for RangeIter<T> {
    fn len(&self) -> usize {
        self.as_usize_range().len()
    }
}

impl<T: IndexTy> DoubleEndedIterator for RangeIter<T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.with_self_as_usize_range(|r| r.next_back().map(T::new))
    }

    fn nth_back(&mut self, n: usize) -> Option<Self::Item> {
        self.with_self_as_usize_range(|r| r.nth_back(n).map(T::new))
    }

    fn rfold<B, F>(self, init: B, mut f: F) -> B
    where
        F: FnMut(B, Self::Item) -> B,
    {
        self.as_usize_range()
            .rfold(init, move |a, v| f(a, T::new(v)))
    }
}

macro_rules! define_index {
    ($name:ident) => {
        #[derive(
            Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize,
        )]
        #[serde(transparent)]
        pub struct $name {
            value: usize,
        }

        impl IndexTy for $name {
            fn new(value: usize) -> Self {
                Self { value }
            }
            fn get(self) -> usize {
                self.value
            }
        }

        impl $name {
            pub const fn new(value: usize) -> Self {
                Self { value }
            }
            pub const fn get(self) -> usize {
                self.value
            }
            pub const fn next(self) -> Self {
                Self {
                    value: self.value + 1,
                }
            }
            pub const fn prev(self) -> Self {
                Self {
                    value: self.value - 1,
                }
            }
        }

        const _: () = {
            #[derive(Copy, Clone)]
            pub struct DisplayOptionIdxImpl(Option<$name>);

            impl fmt::Debug for DisplayOptionIdxImpl {
                fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                    fmt::Display::fmt(self, f)
                }
            }

            impl fmt::Display for DisplayOptionIdxImpl {
                fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                    match &self.0 {
                        Some(v) => fmt::Display::fmt(v, f),
                        None => write!(f, "none"),
                    }
                }
            }

            impl DisplayOptionIdx for Option<$name> {
                type Type = DisplayOptionIdxImpl;

                fn display_option_idx(self) -> Self::Type {
                    DisplayOptionIdxImpl(self)
                }
            }
        };
    };
}

define_index!(SSAValIdx);
define_index!(InstIdx);
define_index!(BlockIdx);
define_index!(BlockParamIdx);
define_index!(OperandIdx);
define_index!(LiveRangeIdx);

pub trait Entries<'a, I>: Index<I>
where
    I: IndexTy,
    Self::Output: 'a,
{
    type Iter: Iterator<Item = (I, &'a Self::Output)>
        + DoubleEndedIterator
        + ExactSizeIterator
        + FusedIterator;
    fn entries(&'a self) -> Self::Iter;
    fn keys(&'a self) -> RangeIter<I>;
}

pub trait EntriesMut<'a, I>: Entries<'a, I> + IndexMut<I>
where
    I: IndexTy,
    Self::Output: 'a,
{
    type IterMut: Iterator<Item = (I, &'a mut Self::Output)>
        + DoubleEndedIterator
        + ExactSizeIterator
        + FusedIterator;
    fn entries_mut(&'a mut self) -> Self::IterMut;
}

pub trait TryIndex<I>: for<'a> Entries<'a, I>
where
    I: IndexTy,
{
    type Error;
    fn try_index(&self, idx: I) -> Result<&Self::Output, Self::Error>;
}

pub trait TryIndexMut<I>: TryIndex<I> + for<'a> EntriesMut<'a, I>
where
    I: IndexTy,
{
    fn try_index_mut(&mut self, idx: I) -> Result<&mut Self::Output, Self::Error>;
}

macro_rules! impl_index {
    (
        #[$(error = $Error:ident, )?iter = $Iter:ident, iter_mut = $IterMut:ident]
        impl Index<$I:ty> for Vec<$T:ty> {}
    ) => {
        #[derive(Clone, Debug)]
        pub struct $Iter<'a> {
            iter: std::iter::Enumerate<std::slice::Iter<'a, $T>>,
        }

        impl<'a> Iterator for $Iter<'a> {
            type Item = ($I, &'a $T);

            fn next(&mut self) -> Option<Self::Item> {
                self.iter.next().map(|(i, v)| (<$I>::new(i), v))
            }

            fn size_hint(&self) -> (usize, Option<usize>) {
                self.iter.size_hint()
            }

            fn fold<B, F>(self, init: B, mut f: F) -> B
            where
                F: FnMut(B, Self::Item) -> B,
            {
                self.iter
                    .fold(init, move |a, (i, v)| f(a, (<$I>::new(i), v)))
            }
        }

        impl DoubleEndedIterator for $Iter<'_> {
            fn next_back(&mut self) -> Option<Self::Item> {
                self.iter.next_back().map(|(i, v)| (<$I>::new(i), v))
            }

            fn rfold<B, F>(self, init: B, mut f: F) -> B
            where
                F: FnMut(B, Self::Item) -> B,
            {
                self.iter
                    .rfold(init, move |a, (i, v)| f(a, (<$I>::new(i), v)))
            }
        }

        impl ExactSizeIterator for $Iter<'_> {
            fn len(&self) -> usize {
                self.iter.len()
            }
        }

        impl FusedIterator for $Iter<'_> {}

        #[derive(Debug)]
        pub struct $IterMut<'a> {
            iter: std::iter::Enumerate<std::slice::IterMut<'a, $T>>,
        }

        impl<'a> Iterator for $IterMut<'a> {
            type Item = ($I, &'a mut $T);

            fn next(&mut self) -> Option<Self::Item> {
                self.iter.next().map(|(i, v)| (<$I>::new(i), v))
            }

            fn size_hint(&self) -> (usize, Option<usize>) {
                self.iter.size_hint()
            }

            fn fold<B, F>(self, init: B, mut f: F) -> B
            where
                F: FnMut(B, Self::Item) -> B,
            {
                self.iter
                    .fold(init, move |a, (i, v)| f(a, (<$I>::new(i), v)))
            }
        }

        impl DoubleEndedIterator for $IterMut<'_> {
            fn next_back(&mut self) -> Option<Self::Item> {
                self.iter.next_back().map(|(i, v)| (<$I>::new(i), v))
            }

            fn rfold<B, F>(self, init: B, mut f: F) -> B
            where
                F: FnMut(B, Self::Item) -> B,
            {
                self.iter
                    .rfold(init, move |a, (i, v)| f(a, (<$I>::new(i), v)))
            }
        }

        impl ExactSizeIterator for $IterMut<'_> {
            fn len(&self) -> usize {
                self.iter.len()
            }
        }

        impl FusedIterator for $IterMut<'_> {}

        impl Index<$I> for Vec<$T> {
            type Output = $T;

            fn index(&self, index: $I) -> &Self::Output {
                &self[index.get()]
            }
        }

        impl IndexMut<$I> for Vec<$T> {
            fn index_mut(&mut self, index: $I) -> &mut Self::Output {
                &mut self[index.get()]
            }
        }

        impl<'a> Entries<'a, $I> for Vec<$T> {
            type Iter = $Iter<'a>;
            fn entries(&'a self) -> Self::Iter {
                $Iter {
                    iter: (**self).iter().enumerate(),
                }
            }
            fn keys(&'a self) -> RangeIter<$I> {
                RangeIter::from_usize_range(0..self.len())
            }
        }

        impl<'a> EntriesMut<'a, $I> for Vec<$T> {
            type IterMut = $IterMut<'a>;
            fn entries_mut(&'a mut self) -> Self::IterMut {
                $IterMut {
                    iter: (**self).iter_mut().enumerate(),
                }
            }
        }

        $(
            impl TryIndex<$I> for Vec<$T> {
                type Error = $Error;

                fn try_index(&self, idx: $I) -> Result<&Self::Output, Self::Error> {
                    self.get(idx.get()).ok_or($Error { idx })
                }
            }

            impl TryIndexMut<$I> for Vec<$T> {
                fn try_index_mut(&mut self, idx: $I) -> Result<&mut Self::Output, Self::Error> {
                    self.get_mut(idx.get()).ok_or($Error { idx })
                }
            }
        )?

        impl Index<$I> for [$T] {
            type Output = $T;

            fn index(&self, index: $I) -> &Self::Output {
                &self[index.get()]
            }
        }

        impl IndexMut<$I> for [$T] {
            fn index_mut(&mut self, index: $I) -> &mut Self::Output {
                &mut self[index.get()]
            }
        }

        impl<'a> Entries<'a, $I> for [$T] {
            type Iter = $Iter<'a>;
            fn entries(&'a self) -> Self::Iter {
                $Iter {
                    iter: self.iter().enumerate(),
                }
            }
            fn keys(&'a self) -> RangeIter<$I> {
                RangeIter::from_usize_range(0..self.len())
            }
        }

        impl<'a> EntriesMut<'a, $I> for [$T] {
            type IterMut = $IterMut<'a>;
            fn entries_mut(&'a mut self) -> Self::IterMut {
                $IterMut {
                    iter: self.iter_mut().enumerate(),
                }
            }
        }

        $(
            impl TryIndex<$I> for [$T] {
                type Error = $Error;

                fn try_index(&self, idx: $I) -> Result<&Self::Output, Self::Error> {
                    self.get(idx.get()).ok_or($Error { idx })
                }
            }

            impl TryIndexMut<$I> for [$T] {
                fn try_index_mut(&mut self, idx: $I) -> Result<&mut Self::Output, Self::Error> {
                    self.get_mut(idx.get()).ok_or($Error { idx })
                }
            }
        )?
    };
}

impl_index! {
    #[error = SSAValIdxOutOfRange, iter = SSAValEntriesIter, iter_mut = SSAValEntriesIterMut]
    impl Index<SSAValIdx> for Vec<SSAVal> {}
}

impl_index! {
    #[error = BlockParamIdxOutOfRange, iter = BlockParamEntriesIter, iter_mut = BlockParamEntriesIterMut]
    impl Index<BlockParamIdx> for Vec<SSAValIdx> {}
}

impl_index! {
    #[error = OperandIdxOutOfRange, iter = OperandEntriesIter, iter_mut = OperandEntriesIterMut]
    impl Index<OperandIdx> for Vec<Operand> {}
}

impl_index! {
    #[error = InstIdxOutOfRange, iter = InstEntriesIter, iter_mut = InstEntriesIterMut]
    impl Index<InstIdx> for Vec<Inst> {}
}

impl_index! {
    #[error = BlockIdxOutOfRange, iter = BlockEntriesIter, iter_mut = BlockEntriesIterMut]
    impl Index<BlockIdx> for Vec<Block> {}
}

impl_index! {
    #[iter = LiveRangeEntriesIter, iter_mut = LiveRangeEntriesIterMut]
    impl Index<LiveRangeIdx> for Vec<LiveRange> {}
}

impl fmt::Display for SSAValIdx {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "v{}", self.get())
    }
}

impl fmt::Display for InstIdx {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "inst{}", self.get())
    }
}

impl fmt::Display for BlockIdx {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "blk{}", self.get())
    }
}

impl fmt::Display for BlockParamIdx {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "bparam{}", self.get())
    }
}

impl fmt::Display for OperandIdx {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "operand{}", self.get())
    }
}

impl fmt::Display for LiveRangeIdx {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "live_range{}", self.get())
    }
}

impl BlockIdx {
    pub const ENTRY_BLOCK: BlockIdx = BlockIdx::new(0);
}

/// range of instruction indexes from `start` inclusive to `end` exclusive.
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct InstRange {
    pub start: InstIdx,
    pub end: InstIdx,
}

impl fmt::Display for InstRange {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "inst{}..{}", self.start.get(), self.end.get())
    }
}

impl IntoIterator for InstRange {
    type Item = InstIdx;
    type IntoIter = InstRangeIter;
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl InstRange {
    pub const fn iter(self) -> InstRangeIter {
        InstRangeIter(self.start.get()..self.end.get())
    }
    pub const fn is_empty(self) -> bool {
        self.start.get() >= self.end.get()
    }
    pub const fn first(self) -> Option<InstIdx> {
        Some(const_try_opt!(self.split_first()).0)
    }
    pub const fn last(self) -> Option<InstIdx> {
        Some(const_try_opt!(self.split_last()).0)
    }
    pub const fn split_first(self) -> Option<(InstIdx, InstRange)> {
        if self.is_empty() {
            None
        } else {
            Some((
                self.start,
                Self {
                    start: self.start.next(),
                    end: self.end,
                },
            ))
        }
    }
    pub const fn split_last(self) -> Option<(InstIdx, InstRange)> {
        if self.is_empty() {
            None
        } else {
            Some((
                self.end.prev(),
                Self {
                    start: self.start,
                    end: self.end.prev(),
                },
            ))
        }
    }
    pub const fn len(self) -> usize {
        if self.is_empty() {
            0
        } else {
            self.end.get() - self.start.get()
        }
    }
}

#[derive(Clone, Debug)]
pub struct InstRangeIter(Range<usize>);

impl InstRangeIter {
    pub const fn range(self) -> InstRange {
        InstRange {
            start: InstIdx::new(self.0.start),
            end: InstIdx::new(self.0.end),
        }
    }
}

impl Iterator for InstRangeIter {
    type Item = InstIdx;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next().map(InstIdx::new)
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let v = self.0.len();
        (v, Some(v))
    }

    fn count(self) -> usize
    where
        Self: Sized,
    {
        self.len()
    }

    fn last(self) -> Option<Self::Item>
    where
        Self: Sized,
    {
        self.0.last().map(InstIdx::new)
    }

    fn nth(&mut self, n: usize) -> Option<Self::Item> {
        self.0.nth(n).map(InstIdx::new)
    }
}

impl FusedIterator for InstRangeIter {}

impl DoubleEndedIterator for InstRangeIter {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.0.next_back().map(InstIdx::new)
    }

    fn nth_back(&mut self, n: usize) -> Option<Self::Item> {
        self.0.nth_back(n).map(InstIdx::new)
    }
}

impl ExactSizeIterator for InstRangeIter {
    fn len(&self) -> usize {
        self.0.len()
    }
}
