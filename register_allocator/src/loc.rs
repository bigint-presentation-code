use crate::error::{Error, Result};
use arbitrary::{size_hint, Arbitrary};
use enum_map::Enum;
use serde::{Deserialize, Serialize};
use std::{iter::FusedIterator, num::NonZeroU32};

#[derive(
    Serialize,
    Deserialize,
    Copy,
    Clone,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Debug,
    Hash,
    Enum,
    Arbitrary,
)]
#[repr(u8)]
pub enum LocKind {
    Gpr,
    StackBits64,
    Ca,
    VlMaxvl,
}

impl LocKind {
    /// since `==` doesn't work with enums in const context
    pub const fn const_eq(self, other: Self) -> bool {
        self as u8 == other as u8
    }
    pub const fn base_ty(self) -> BaseTy {
        match self {
            Self::Gpr | Self::StackBits64 => BaseTy::Bits64,
            Self::Ca => BaseTy::Ca,
            Self::VlMaxvl => BaseTy::VlMaxvl,
        }
    }

    pub const fn loc_count(self) -> NonZeroU32 {
        match self {
            Self::StackBits64 => nzu32_lit!(512),
            Self::Gpr | Self::Ca | Self::VlMaxvl => self.base_ty().max_reg_len(),
        }
    }
}

#[derive(
    Serialize,
    Deserialize,
    Copy,
    Clone,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Debug,
    Hash,
    Enum,
    Arbitrary,
)]
#[repr(u8)]
pub enum BaseTy {
    Bits64,
    Ca,
    VlMaxvl,
}

impl BaseTy {
    /// since `==` doesn't work with enums in const context
    pub const fn const_eq(self, other: Self) -> bool {
        self as u8 == other as u8
    }

    pub const fn only_scalar(self) -> bool {
        self.max_reg_len().get() == 1
    }

    pub const fn max_reg_len(self) -> NonZeroU32 {
        match self {
            Self::Bits64 => nzu32_lit!(128),
            Self::Ca | Self::VlMaxvl => nzu32_lit!(1),
        }
    }

    pub const fn loc_kinds(self) -> &'static [LocKind] {
        match self {
            BaseTy::Bits64 => &[LocKind::Gpr, LocKind::StackBits64],
            BaseTy::Ca => &[LocKind::Ca],
            BaseTy::VlMaxvl => &[LocKind::VlMaxvl],
        }
    }

    pub fn arbitrary_reg_len(
        self,
        u: &mut arbitrary::Unstructured<'_>,
    ) -> arbitrary::Result<NonZeroU32> {
        Ok(NonZeroU32::new(u.int_in_range(1..=self.max_reg_len().get())?).unwrap())
    }

    pub fn arbitrary_reg_len_size_hint(depth: usize) -> (usize, Option<usize>) {
        (0, NonZeroU32::size_hint(depth).1)
    }
}

validated_fields! {
    #[fields_ty = TyFields]
    #[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
    pub struct Ty {
        pub base_ty: BaseTy,
        pub reg_len: NonZeroU32,
    }
}

impl<'a> Arbitrary<'a> for TyFields {
    fn arbitrary(u: &mut arbitrary::Unstructured<'a>) -> arbitrary::Result<Self> {
        let base_ty: BaseTy = u.arbitrary()?;
        let reg_len = base_ty.arbitrary_reg_len(u)?;
        Ok(Self { base_ty, reg_len })
    }
    fn size_hint(depth: usize) -> (usize, Option<usize>) {
        let base_ty = BaseTy::size_hint(depth);
        let reg_len = BaseTy::arbitrary_reg_len_size_hint(depth);
        size_hint::and(base_ty, reg_len)
    }
}

impl<'a> Arbitrary<'a> for Ty {
    fn arbitrary(u: &mut arbitrary::Unstructured<'a>) -> arbitrary::Result<Self> {
        Ok(Ty::new(u.arbitrary()?)?)
    }
    fn size_hint(depth: usize) -> (usize, Option<usize>) {
        TyFields::size_hint(depth)
    }
}

impl Ty {
    pub const fn new(fields: TyFields) -> Result<Ty> {
        let TyFields { base_ty, reg_len } = fields;
        if base_ty.only_scalar() && reg_len.get() != 1 {
            Err(Error::TriedToCreateVectorOfOnlyScalarType { base_ty })
        } else if reg_len.get() > base_ty.max_reg_len().get() {
            Err(Error::RegLenOutOfRange)
        } else {
            Ok(Self(fields))
        }
    }
    /// returns the `Ty` for `fields` or if there was an error, returns the corresponding scalar type (where `reg_len` is `1`)
    pub const fn new_or_scalar(fields: TyFields) -> Self {
        match Self::new(fields) {
            Ok(v) => v,
            Err(_) => Self::scalar(fields.base_ty),
        }
    }
    pub const fn scalar(base_ty: BaseTy) -> Self {
        Self(TyFields {
            base_ty,
            reg_len: nzu32_lit!(1),
        })
    }
    pub const fn bits64(reg_len: NonZeroU32) -> Self {
        Self(TyFields {
            base_ty: BaseTy::Bits64,
            reg_len,
        })
    }
    pub const fn try_concat(self, rhs: Self) -> Result<Ty> {
        if !self.get().base_ty.const_eq(rhs.get().base_ty) {
            Err(Error::BaseTyMismatch)
        } else {
            let Some(reg_len) = self.get().reg_len.checked_add(rhs.get().reg_len.get()) else {
                return Err(Error::RegLenOutOfRange);
            };
            Ty::new(TyFields {
                base_ty: self.get().base_ty,
                reg_len,
            })
        }
    }
}

validated_fields! {
    #[fields_ty = SubLocFields]
    #[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
    pub struct SubLoc {
        pub kind: LocKind,
        pub start: u32,
    }
}

impl SubLoc {
    pub const fn new(fields: SubLocFields) -> Result<SubLoc> {
        const_try!(Loc::new(LocFields {
            reg_len: nzu32_lit!(1),
            kind: fields.kind,
            start: fields.start
        }));
        Ok(SubLoc(fields))
    }
}

validated_fields! {
    #[fields_ty = LocFields]
    #[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
    pub struct Loc {
        pub reg_len: NonZeroU32,
        pub kind: LocKind,
        pub start: u32,
    }
}

impl<'a> Arbitrary<'a> for LocFields {
    fn arbitrary(u: &mut arbitrary::Unstructured<'a>) -> arbitrary::Result<Self> {
        let kind: LocKind = u.arbitrary()?;
        let reg_len = kind.base_ty().arbitrary_reg_len(u)?;
        let start = Loc::arbitrary_start(kind, reg_len, u)?;
        Ok(Self {
            kind,
            start,
            reg_len,
        })
    }

    fn size_hint(depth: usize) -> (usize, Option<usize>) {
        let kind = LocKind::size_hint(depth);
        let reg_len = BaseTy::arbitrary_reg_len_size_hint(depth);
        let start = Loc::arbitrary_start_size_hint(depth);
        size_hint::and(size_hint::and(kind, reg_len), start)
    }
}

impl<'a> Arbitrary<'a> for Loc {
    fn arbitrary(u: &mut arbitrary::Unstructured<'a>) -> arbitrary::Result<Self> {
        Ok(Loc::new(u.arbitrary()?)?)
    }
    fn size_hint(depth: usize) -> (usize, Option<usize>) {
        LocFields::size_hint(depth)
    }
}

impl LocFields {
    pub const fn ty(self) -> Result<Ty> {
        Ty::new(TyFields {
            base_ty: self.kind.base_ty(),
            reg_len: self.reg_len,
        })
    }
    pub const fn stop(self) -> NonZeroU32 {
        const_unwrap_opt!(self.reg_len.checked_add(self.start), "overflow")
    }
    pub const fn first_subloc(self) -> SubLocFields {
        SubLocFields {
            kind: self.kind,
            start: self.start,
        }
    }
}

impl Loc {
    pub const fn first_subloc(self) -> SubLoc {
        SubLoc(self.get().first_subloc())
    }
    pub fn arbitrary_with_ty(
        ty: Ty,
        u: &mut arbitrary::Unstructured<'_>,
    ) -> arbitrary::Result<Self> {
        let kind = *u.choose(ty.base_ty.loc_kinds())?;
        let start = Self::arbitrary_start(kind, ty.reg_len, u)?;
        Ok(Self::new(LocFields {
            kind,
            start,
            reg_len: ty.reg_len,
        })?)
    }
    pub fn arbitrary_start(
        kind: LocKind,
        reg_len: NonZeroU32,
        u: &mut arbitrary::Unstructured<'_>,
    ) -> arbitrary::Result<u32> {
        u.int_in_range(0..=Loc::max_start(kind, reg_len)?)
    }
    pub fn arbitrary_start_size_hint(depth: usize) -> (usize, Option<usize>) {
        (0, u32::size_hint(depth).1)
    }
    pub const fn ty(self) -> Ty {
        const_unwrap_res!(self.0.ty(), "Loc can only be constructed with valid fields")
    }
    /// does all `Loc` validation except checking `start`, returns the maximum
    /// value `start` can have, so a `Loc` is valid if
    /// `start < Loc::max_start(kind, reg_len)?`
    pub const fn max_start(kind: LocKind, reg_len: NonZeroU32) -> Result<u32, Error> {
        // validate Ty
        const_try!(Ty::new(TyFields {
            base_ty: kind.base_ty(),
            reg_len
        }));
        let loc_count: u32 = kind.loc_count().get();
        let Some(max_start) = loc_count.checked_sub(reg_len.get()) else {
            return Err(Error::InvalidRegLen)
        };
        Ok(max_start)
    }
    pub const fn new(fields: LocFields) -> Result<Loc> {
        let LocFields {
            kind,
            start,
            reg_len,
        } = fields;

        if start > const_try!(Self::max_start(kind, reg_len)) {
            Err(Error::StartNotInValidRange)
        } else {
            Ok(Self(fields))
        }
    }
    pub const fn conflicts(self, other: Loc) -> bool {
        self.0.kind.const_eq(other.0.kind)
            && self.0.start < other.0.stop().get()
            && other.0.start < self.0.stop().get()
    }
    pub const fn get_sub_loc_at_offset(self, sub_loc_ty: Ty, offset: u32) -> Result<Self> {
        if !sub_loc_ty.get().base_ty.const_eq(self.get().kind.base_ty()) {
            return Err(Error::BaseTyMismatch);
        }
        let Some(stop) = sub_loc_ty.get().reg_len.checked_add(offset) else {
            return Err(Error::InvalidSubLocOutOfRange)
        };
        if stop.get() > self.get().reg_len.get() {
            Err(Error::InvalidSubLocOutOfRange)
        } else {
            Self::new(LocFields {
                kind: self.get().kind,
                start: self.get().start + offset,
                reg_len: sub_loc_ty.get().reg_len,
            })
        }
    }
    /// get the Loc containing `self` such that:
    /// `retval.get_sub_loc_at_offset(self.ty(), offset) == self`
    /// and `retval.ty() == super_loc_ty`
    pub const fn get_super_loc_with_self_at_offset(
        self,
        super_loc_ty: Ty,
        offset: u32,
    ) -> Result<Self> {
        if !super_loc_ty
            .get()
            .base_ty
            .const_eq(self.get().kind.base_ty())
        {
            return Err(Error::BaseTyMismatch);
        }
        let Some(stop) = self.get().reg_len.checked_add(offset) else {
            return Err(Error::InvalidSubLocOutOfRange)
        };
        if stop.get() > super_loc_ty.get().reg_len.get() {
            Err(Error::InvalidSubLocOutOfRange)
        } else {
            Self::new(LocFields {
                kind: self.get().kind,
                start: self.get().start - offset,
                reg_len: super_loc_ty.get().reg_len,
            })
        }
    }
    pub const SPECIAL_GPRS: &[Loc] = &[
        Loc(LocFields {
            kind: LocKind::Gpr,
            start: 0,
            reg_len: nzu32_lit!(1),
        }),
        Loc(LocFields {
            kind: LocKind::Gpr,
            start: 1,
            reg_len: nzu32_lit!(1),
        }),
        Loc(LocFields {
            kind: LocKind::Gpr,
            start: 2,
            reg_len: nzu32_lit!(1),
        }),
        Loc(LocFields {
            kind: LocKind::Gpr,
            start: 13,
            reg_len: nzu32_lit!(1),
        }),
    ];
    pub fn sub_locs(
        self,
    ) -> impl Iterator<Item = SubLoc> + FusedIterator + ExactSizeIterator + DoubleEndedIterator
    {
        let LocFields {
            reg_len: _,
            kind,
            start,
        } = *self;
        (start..self.stop().get()).map(move |start| SubLoc(SubLocFields { kind, start }))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_base_ty_loc_kinds() {
        for loc_kind in 0..LocKind::LENGTH {
            let loc_kind = LocKind::from_usize(loc_kind);
            let base_ty = loc_kind.base_ty();
            let loc_kinds = base_ty.loc_kinds();
            assert!(
                loc_kinds.contains(&loc_kind),
                "loc_kind:{loc_kind:?} base_ty:{base_ty:?} loc_kinds:{loc_kinds:?}"
            );
        }
        for base_ty in 0..BaseTy::LENGTH {
            let base_ty = BaseTy::from_usize(base_ty);
            let loc_kinds = base_ty.loc_kinds();
            for &loc_kind in loc_kinds {
                assert_eq!(
                    loc_kind.base_ty(),
                    base_ty,
                    "loc_kind:{loc_kind:?} base_ty:{base_ty:?} loc_kinds:{loc_kinds:?}"
                );
            }
        }
    }
}
