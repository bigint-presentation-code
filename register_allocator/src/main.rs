use arbitrary::{Arbitrary, Unstructured};
use bigint_presentation_code_register_allocator::{
    function::{FnFields, Function},
    interned::GlobalState,
};
use clap::Parser;
use eyre::Result;
use serde::Serialize;
use serde_json::{Deserializer, Value};
use std::{
    io::{BufWriter, Read, Write},
    process::ExitCode,
};

#[derive(Parser, Debug)]
#[command(version, about, long_about)]
struct Args {
    /// dump the input function before attempting register allocation
    #[arg(long)]
    dump_input: bool,
    /// write JSON outputs in pretty format
    #[arg(long)]
    pretty: bool,
    /// generate an input function from the random bytes read from stdin using
    /// the same functionality used for fuzzing
    #[arg(long)]
    arbitrary: bool,
}

#[derive(Serialize, Debug)]
pub enum Info<'a> {
    DumpInput(&'a Function),
}

#[derive(Serialize, Debug)]
pub enum Output<'a> {
    Error(String),
    Info(Info<'a>),
    NeedDifferentInput,
    Success(Value),
}

impl<'a> Output<'a> {
    fn write_to_stdout(&self, pretty: bool) -> Result<()> {
        let mut stdout = BufWriter::new(std::io::stdout().lock());
        if pretty {
            serde_json::to_writer_pretty(&mut stdout, self)?;
        } else {
            serde_json::to_writer(&mut stdout, self)?;
        }
        writeln!(stdout)?;
        stdout.flush()?;
        Ok(())
    }
    fn from_err(e: impl Into<eyre::Report>) -> Self {
        Self::Error(format!("{:?}", e.into()))
    }
}

fn process_input(args: &Args, input: Value, exit_code: &mut ExitCode) -> Result<()> {
    GlobalState::scope(|| -> Result<()> {
        let function = match serde_json::from_value::<Function>(input) {
            Ok(v) => v,
            Err(e) => {
                Output::from_err(e).write_to_stdout(args.pretty)?;
                *exit_code = ExitCode::FAILURE;
                return Ok(());
            }
        };
        if args.dump_input {
            Output::Info(Info::DumpInput(&function)).write_to_stdout(args.pretty)?;
        }
        todo!()
    })
}

fn arbitrary_input(input_bytes: &[u8]) -> arbitrary::Result<Value> {
    GlobalState::scope(|| -> arbitrary::Result<Value> {
        Ok(
            serde_json::to_value(FnFields::arbitrary_take_rest(Unstructured::new(
                &input_bytes,
            ))?)
            .expect("serialization shouldn't ever fail"),
        )
    })
}

fn main() -> Result<ExitCode> {
    let args = Args::parse();
    let Args {
        dump_input: _,
        pretty,
        arbitrary,
    } = args;
    let stdin = std::io::stdin().lock();
    let mut exit_code = ExitCode::SUCCESS;
    if arbitrary {
        let mut input_bytes = vec![];
        stdin.take(0x10000).read_to_end(&mut input_bytes)?;
        if let Ok(input) = arbitrary_input(&input_bytes) {
            process_input(&args, input, &mut exit_code)?;
        } else {
            Output::NeedDifferentInput.write_to_stdout(pretty)?;
            exit_code = ExitCode::FAILURE;
        };
    } else {
        for input in Deserializer::from_reader(stdin).into_iter::<Value>() {
            process_input(&args, input?, &mut exit_code)?;
        }
    }
    Ok(exit_code)
}
