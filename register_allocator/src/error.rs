use crate::{
    index::{BlockIdx, BlockParamIdx, DisplayOptionIdx, IndexTy, InstIdx, OperandIdx, SSAValIdx},
    loc::{BaseTy, Ty},
};
use std::fmt;
use thiserror::Error;

#[derive(Debug, Error)]
#[error("SSA value index {idx} out of range")]
pub struct SSAValIdxOutOfRange {
    pub idx: SSAValIdx,
}

impl SSAValIdxOutOfRange {
    pub fn with_inst_and_operand(
        self,
        inst: InstIdx,
        operand: OperandIdx,
    ) -> WithContext<
        Option<BlockIdx>,
        InstIdx,
        AlwaysNone,
        OperandIdx,
        AlwaysNone,
        SSAValIdxOutOfRange,
    > {
        WithContext {
            block: None,
            inst,
            succ: AlwaysNone,
            operand,
            param: AlwaysNone,
            out_of_range_error: self,
        }
    }
    pub fn with_block_inst_and_operand(
        self,
        block: BlockIdx,
        inst: InstIdx,
        operand: OperandIdx,
    ) -> WithContext<
        Option<BlockIdx>,
        InstIdx,
        AlwaysNone,
        OperandIdx,
        AlwaysNone,
        SSAValIdxOutOfRange,
    > {
        WithContext {
            block: Some(block),
            inst,
            succ: AlwaysNone,
            operand,
            param: AlwaysNone,
            out_of_range_error: self,
        }
    }
    pub fn with_inst_succ_and_param(
        self,
        inst: InstIdx,
        succ: BlockIdx,
        param: BlockParamIdx,
    ) -> WithContext<
        Option<BlockIdx>,
        InstIdx,
        BlockIdx,
        AlwaysNone,
        BlockParamIdx,
        SSAValIdxOutOfRange,
    > {
        WithContext {
            block: None,
            inst,
            succ,
            operand: AlwaysNone,
            param,
            out_of_range_error: self,
        }
    }
    pub fn with_block_inst_succ_and_param(
        self,
        block: BlockIdx,
        inst: InstIdx,
        succ: BlockIdx,
        param: BlockParamIdx,
    ) -> WithContext<
        Option<BlockIdx>,
        InstIdx,
        BlockIdx,
        AlwaysNone,
        BlockParamIdx,
        SSAValIdxOutOfRange,
    > {
        WithContext {
            block: Some(block),
            inst,
            succ,
            operand: AlwaysNone,
            param,
            out_of_range_error: self,
        }
    }
    pub fn with_block_and_param(
        self,
        block: BlockIdx,
        param: BlockParamIdx,
    ) -> WithContext<BlockIdx, AlwaysNone, AlwaysNone, AlwaysNone, BlockParamIdx, SSAValIdxOutOfRange>
    {
        WithContext {
            block,
            inst: AlwaysNone,
            succ: AlwaysNone,
            operand: AlwaysNone,
            param,
            out_of_range_error: self,
        }
    }
}

#[derive(Debug, Error)]
#[error("instruction index {idx} out of range")]
pub struct InstIdxOutOfRange {
    pub idx: InstIdx,
}

impl InstIdxOutOfRange {
    pub fn with_block(
        self,
        block: BlockIdx,
    ) -> WithContext<BlockIdx, AlwaysNone, AlwaysNone, AlwaysNone, AlwaysNone, InstIdxOutOfRange>
    {
        WithContext {
            block,
            inst: AlwaysNone,
            succ: AlwaysNone,
            operand: AlwaysNone,
            param: AlwaysNone,
            out_of_range_error: self,
        }
    }
}

#[derive(Debug, Error)]
#[error("block index {idx} out of range")]
pub struct BlockIdxOutOfRange {
    pub idx: BlockIdx,
}

#[derive(Debug, Error)]
#[error("block parameter index {idx} is out of range")]
pub struct BlockParamIdxOutOfRange {
    pub idx: BlockParamIdx,
}

impl BlockParamIdxOutOfRange {
    pub fn with_block(
        self,
        block: BlockIdx,
    ) -> WithContext<
        BlockIdx,
        AlwaysNone,
        AlwaysNone,
        AlwaysNone,
        AlwaysNone,
        BlockParamIdxOutOfRange,
    > {
        WithContext {
            block,
            inst: AlwaysNone,
            succ: AlwaysNone,
            operand: AlwaysNone,
            param: AlwaysNone,
            out_of_range_error: self,
        }
    }
    pub fn with_block_inst_and_succ(
        self,
        block: BlockIdx,
        inst: InstIdx,
        succ: BlockIdx,
    ) -> WithContext<
        Option<BlockIdx>,
        InstIdx,
        BlockIdx,
        AlwaysNone,
        AlwaysNone,
        BlockParamIdxOutOfRange,
    > {
        WithContext {
            block: Some(block),
            inst,
            succ,
            operand: AlwaysNone,
            param: AlwaysNone,
            out_of_range_error: self,
        }
    }
    pub fn with_inst_and_succ(
        self,
        inst: InstIdx,
        succ: BlockIdx,
    ) -> WithContext<
        Option<BlockIdx>,
        InstIdx,
        BlockIdx,
        AlwaysNone,
        AlwaysNone,
        BlockParamIdxOutOfRange,
    > {
        WithContext {
            block: None,
            inst,
            succ,
            operand: AlwaysNone,
            param: AlwaysNone,
            out_of_range_error: self,
        }
    }
}

#[derive(Debug, Error)]
#[error("operand index {idx} is out of range")]
pub struct OperandIdxOutOfRange {
    pub idx: OperandIdx,
}

impl OperandIdxOutOfRange {
    pub fn with_block_and_inst(
        self,
        block: BlockIdx,
        inst: InstIdx,
    ) -> WithContext<
        Option<BlockIdx>,
        InstIdx,
        AlwaysNone,
        AlwaysNone,
        AlwaysNone,
        OperandIdxOutOfRange,
    > {
        WithContext {
            block: Some(block),
            inst,
            succ: AlwaysNone,
            operand: AlwaysNone,
            param: AlwaysNone,
            out_of_range_error: self,
        }
    }
    pub fn with_inst(
        self,
        inst: InstIdx,
    ) -> WithContext<
        Option<BlockIdx>,
        InstIdx,
        AlwaysNone,
        AlwaysNone,
        AlwaysNone,
        OperandIdxOutOfRange,
    > {
        WithContext {
            block: None,
            inst,
            succ: AlwaysNone,
            operand: AlwaysNone,
            param: AlwaysNone,
            out_of_range_error: self,
        }
    }
}

#[derive(Default, Clone, Copy, Debug)]
pub struct AlwaysNone;

pub trait ToContextValue<T: IndexTy>: fmt::Debug + Copy {
    fn to_context_value(self) -> Option<T>;
}

impl<T: IndexTy> ToContextValue<T> for AlwaysNone {
    fn to_context_value(self) -> Option<T> {
        None
    }
}

impl<T: IndexTy> ToContextValue<T> for T {
    fn to_context_value(self) -> Option<T> {
        Some(self)
    }
}

impl<T: IndexTy> ToContextValue<T> for Option<T> {
    fn to_context_value(self) -> Option<T> {
        self
    }
}

#[derive(Debug, Error)]
pub struct WithContext<Block, Inst, Succ, Operand, BlockParam, E>
where
    Block: ToContextValue<BlockIdx>,
    Inst: ToContextValue<InstIdx>,
    Succ: ToContextValue<BlockIdx>,
    Operand: ToContextValue<OperandIdx>,
    BlockParam: ToContextValue<BlockParamIdx>,
{
    pub block: Block,
    pub inst: Inst,
    pub succ: Succ,
    pub operand: Operand,
    pub param: BlockParam,
    #[source]
    pub out_of_range_error: E,
}

impl<Block, Inst, Succ, Operand, BlockParam, E> fmt::Display
    for WithContext<Block, Inst, Succ, Operand, BlockParam, E>
where
    Block: ToContextValue<BlockIdx>,
    Inst: ToContextValue<InstIdx>,
    Succ: ToContextValue<BlockIdx>,
    Operand: ToContextValue<OperandIdx>,
    BlockParam: ToContextValue<BlockParamIdx>,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let Self {
            block,
            inst,
            succ,
            operand,
            param,
            out_of_range_error: _,
        } = self;
        let block = block.to_context_value();
        let inst = inst.to_context_value();
        let succ = succ.to_context_value();
        let operand = operand.to_context_value();
        let param = param.to_context_value();
        macro_rules! write_if {
            ($field:ident, $f:ident, $($rest:tt)+) => {
                if let Some($field) = $field {
                    write!($f, $($rest)+)?;
                }
            };
        }
        if block.is_some() || inst.is_some() {
            write!(f, " in ")?;
        }
        write_if!(block, f, "{block}");
        if block.is_some() && inst.is_some() {
            write!(f, ":")?;
        }
        write_if!(inst, f, "{inst}");
        write_if!(succ, f, " in successor {succ}");
        write_if!(operand, f, " in {operand}");
        write_if!(param, f, " in block parameter {param}");
        Ok(())
    }
}

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    SSAValIdxOutOfRangeWithBlockInstAndOperand(
        #[from]
        WithContext<
            Option<BlockIdx>,
            InstIdx,
            AlwaysNone,
            OperandIdx,
            AlwaysNone,
            SSAValIdxOutOfRange,
        >,
    ),
    #[error(transparent)]
    SSAValIdxOutOfRangeWithBlockInstSuccAndParam(
        #[from]
        WithContext<
            Option<BlockIdx>,
            InstIdx,
            BlockIdx,
            AlwaysNone,
            BlockParamIdx,
            SSAValIdxOutOfRange,
        >,
    ),
    #[error(transparent)]
    SSAValIdxOutOfRangeWithBlockAndParam(
        #[from]
        WithContext<
            BlockIdx,
            AlwaysNone,
            AlwaysNone,
            AlwaysNone,
            BlockParamIdx,
            SSAValIdxOutOfRange,
        >,
    ),
    #[error(transparent)]
    InstIdxOutOfRangeWithBlock(
        #[from]
        WithContext<BlockIdx, AlwaysNone, AlwaysNone, AlwaysNone, AlwaysNone, InstIdxOutOfRange>,
    ),
    #[error(transparent)]
    InstIdxOutOfRange(#[from] InstIdxOutOfRange),
    #[error(transparent)]
    BlockIdxOutOfRange(#[from] BlockIdxOutOfRange),
    #[error(transparent)]
    BlockParamIdxOutOfRangeWithBlock(
        #[from]
        WithContext<
            BlockIdx,
            AlwaysNone,
            AlwaysNone,
            AlwaysNone,
            AlwaysNone,
            BlockParamIdxOutOfRange,
        >,
    ),
    #[error(transparent)]
    BlockParamIdxOutOfRangeWithBlockInstAndSucc(
        #[from]
        WithContext<
            Option<BlockIdx>,
            InstIdx,
            BlockIdx,
            AlwaysNone,
            AlwaysNone,
            BlockParamIdxOutOfRange,
        >,
    ),
    #[error(transparent)]
    OperandIdxOutOfRangeWithBlockAndInst(
        #[from]
        WithContext<
            Option<BlockIdx>,
            InstIdx,
            AlwaysNone,
            AlwaysNone,
            AlwaysNone,
            OperandIdxOutOfRange,
        >,
    ),
    #[error("can't create a vector of an only-scalar type: {base_ty:?}")]
    TriedToCreateVectorOfOnlyScalarType { base_ty: BaseTy },
    #[error("reg_len out of range")]
    RegLenOutOfRange,
    #[error("invalid reg_len")]
    InvalidRegLen,
    #[error("start not in valid range")]
    StartNotInValidRange,
    #[error("BaseTy mismatch")]
    BaseTyMismatch,
    #[error("invalid sub-Loc: offset and/or reg_len out of range")]
    InvalidSubLocOutOfRange,
    #[error("Ty mismatch: expected {expected_ty:?} got {ty:?}")]
    TyMismatch {
        ty: Option<Ty>,
        expected_ty: Option<Ty>,
    },
    #[error("function doesn't have entry block")]
    MissingEntryBlock,
    #[error("instruction index is too big")]
    InstIdxTooBig,
    #[error("block has invalid start {start}, expected {expected_start}")]
    BlockHasInvalidStart {
        start: InstIdx,
        expected_start: InstIdx,
    },
    #[error("block {block} doesn't contain any instructions")]
    BlockIsEmpty { block: BlockIdx },
    #[error("entry block must not have any block parameters")]
    EntryBlockCantHaveParams,
    #[error("entry block must not have any predecessors")]
    EntryBlockCantHavePreds,
    #[error("block end is out of range: {end}")]
    BlockEndOutOfRange { end: InstIdx },
    #[error("block's last instruction must be a block terminator: {term_idx}")]
    BlocksLastInstMustBeTerm { term_idx: InstIdx },
    #[error(
        "block terminator instructions are only allowed as a block's last \
        instruction: {inst_idx}"
    )]
    TermInstOnlyAllowedAtBlockEnd { inst_idx: InstIdx },
    #[error("instruction not in a block: {inst}")]
    InstHasNoBlock { inst: InstIdx },
    #[error("duplicate copy destination operand: operand index {operand_idx} for {inst}")]
    DupCopyDestOperand {
        inst: InstIdx,
        operand_idx: OperandIdx,
    },
    #[error("copy instruction's source type doesn't match source operands")]
    CopySrcTyMismatch { inst: InstIdx },
    #[error("copy instruction's destination type doesn't match destination operands")]
    CopyDestTyMismatch { inst: InstIdx },
    #[error(
        "operand index {operand_idx} for {inst} is missing from SSA value \
        {ssa_val_idx}'s uses"
    )]
    MissingOperandUse {
        ssa_val_idx: SSAValIdx,
        inst: InstIdx,
        operand_idx: OperandIdx,
    },
    #[error(
        "operand index {operand_idx} for {inst} has kind `Def` but isn't \
        SSA value {ssa_val_idx}'s definition"
    )]
    OperandDefIsNotSSAValDef {
        ssa_val_idx: SSAValIdx,
        inst: InstIdx,
        operand_idx: OperandIdx,
    },
    #[error(
        "SSA value {ssa_val_idx}'s definition isn't the corresponding \
        operand's SSA Value: operand index {operand_idx} for {inst}"
    )]
    SSAValDefIsNotOperandsSSAVal {
        ssa_val_idx: SSAValIdx,
        inst: InstIdx,
        operand_idx: OperandIdx,
    },
    #[error(
        "SSA value {ssa_val_idx}'s type can't be used with the constraint on \
        operand index {operand_idx} for {inst}"
    )]
    ConstraintTyMismatch {
        ssa_val_idx: SSAValIdx,
        inst: InstIdx,
        operand_idx: OperandIdx,
    },
    #[error(
        "fixed location constraint on operand index {operand_idx} for \
        {inst} conflicts with clobbers"
    )]
    FixedLocConflictsWithClobbers {
        inst: InstIdx,
        operand_idx: OperandIdx,
    },
    #[error("operand kind must be def")]
    OperandKindMustBeDef,
    #[error(
        "reuse target operand (index {reuse_target_operand_idx}) for \
        {inst} must have kind `Use`"
    )]
    ReuseTargetOperandMustBeUse {
        inst: InstIdx,
        reuse_target_operand_idx: OperandIdx,
    },
    #[error(
        "source block {src_block} missing from branch {branch_inst}'s \
        target block {tgt_block}'s predecessors"
    )]
    SrcBlockMissingFromBranchTgtBlocksPreds {
        src_block: BlockIdx,
        branch_inst: InstIdx,
        tgt_block: BlockIdx,
    },
    #[error(
        "branch {inst}'s parameter (index {param_idx}) for successor {succ} \
        is missing from SSA value {ssa_val_idx}'s uses"
    )]
    MissingBranchSuccParamUse {
        ssa_val_idx: SSAValIdx,
        inst: InstIdx,
        succ: BlockIdx,
        param_idx: BlockParamIdx,
    },
    #[error(
        "the number of parameters ({branch_param_count}) for branch {inst}'s \
        successor {succ} doesn't match the number of parameters \
        ({block_param_count}) declared in that block"
    )]
    BranchSuccParamCountMismatch {
        inst: InstIdx,
        succ: BlockIdx,
        block_param_count: usize,
        branch_param_count: usize,
    },
    #[error(
        "the type {branch_param_ty:?} of parameter {param_idx} for branch \
        {inst}'s successor {succ} doesn't match the type {block_param_ty:?} \
        declared in that block"
    )]
    BranchSuccParamTyMismatch {
        inst: InstIdx,
        succ: BlockIdx,
        param_idx: BlockParamIdx,
        block_param_ty: Ty,
        branch_param_ty: Ty,
    },
    #[error(
        "block {block}'s parameter {param_idx} doesn't match SSA value \
        {ssa_val_idx}'s definition"
    )]
    MismatchedBlockParamDef {
        ssa_val_idx: SSAValIdx,
        block: BlockIdx,
        param_idx: BlockParamIdx,
    },
    #[error(
        "predecessor {src_block} of target block {tgt_block} is missing from \
        that predecessor block's terminating branch {branch_inst}'s targets"
    )]
    PredMissingFromPredsTermBranchsTargets {
        src_block: BlockIdx,
        branch_inst: InstIdx,
        tgt_block: BlockIdx,
    },
    #[error(
        "SSA value {ssa_val_idx}'s use isn't the corresponding \
        operand's SSA Value: operand index {operand_idx} for {inst}"
    )]
    SSAValUseIsNotOperandsSSAVal {
        ssa_val_idx: SSAValIdx,
        inst: InstIdx,
        operand_idx: OperandIdx,
    },
    #[error(
        "SSA value {ssa_val_idx} is use as a branch instruction {inst}'s \
        block parameter, but that instruction isn't a branch instruction"
    )]
    SSAValUseIsNotBranch {
        ssa_val_idx: SSAValIdx,
        inst: InstIdx,
    },
    #[error("expected instruction {inst} to be a `BlockTerm` instruction")]
    InstIsNotBlockTerm { inst: InstIdx },
    #[error("target block {tgt_block} not found in branch instruction {branch_inst}")]
    BranchTargetNotFound {
        branch_inst: InstIdx,
        tgt_block: BlockIdx,
    },
    #[error(
        "SSA value {ssa_val_idx}'s use isn't the corresponding \
        branch {branch_inst}'s target block parameter's SSA Value for \
        target block {tgt_block}'s parameter index {param_idx}"
    )]
    MismatchedBranchTargetBlockParamUse {
        ssa_val_idx: SSAValIdx,
        branch_inst: InstIdx,
        tgt_block: BlockIdx,
        param_idx: BlockParamIdx,
    },
    #[error(
        "block {block_idx} has incorrect immediate dominator: expected \
        {} found {}",
        .expected.display_option_idx(),
        .found.display_option_idx(),
    )]
    IncorrectImmediateDominator {
        block_idx: BlockIdx,
        found: Option<BlockIdx>,
        expected: Option<BlockIdx>,
    },
    #[error(
        "critical edges are not allowed: source block {src_block} has \
        multiple successors and target block {tgt_block} has multiple \
        predecessors: source block's terminating branch {branch_inst}"
    )]
    CriticalEdgeNotAllowed {
        src_block: BlockIdx,
        branch_inst: InstIdx,
        tgt_block: BlockIdx,
    },
    #[error(
        "operand can't reuse operand of differing type: in {inst}: reuse \
        source operand: index {src_operand_idx} type {src_ty:?}) reuse \
        target operand: index {tgt_operand_idx} type {tgt_ty:?})"
    )]
    ReuseOperandTyMismatch {
        inst: InstIdx,
        tgt_operand_idx: OperandIdx,
        src_operand_idx: OperandIdx,
        src_ty: Ty,
        tgt_ty: Ty,
    },
}

pub type Result<T, E = Error> = std::result::Result<T, E>;

impl From<Error> for arbitrary::Error {
    fn from(_value: Error) -> Self {
        Self::IncorrectFormat
    }
}
