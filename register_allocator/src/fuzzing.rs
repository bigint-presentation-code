use crate::{
    function::{
        Block, BlockTermInstKind, Constraint, CopyInstKind, FnFields, Inst, InstKind, InstStage,
        KindAndConstraint, Operand, OperandKind, OperandKindDefOnly, SSAVal, SSAValDef,
    },
    index::{BlockIdx, Entries, EntriesMut, InstIdx, InstRange, OperandIdx, RangeIter, SSAValIdx},
    interned::{GlobalState, Intern},
    loc::Ty,
    loc_set::LocSet,
};
use arbitrary::{Arbitrary, Error, Unstructured};
use enum_map::EnumMap;
use hashbrown::HashMap;
use petgraph::algo::dominators;
use std::{borrow::Borrow, collections::BTreeMap, ops::Deref};

#[derive(Clone, Debug, Default)]
struct SSAValIdxs {
    per_ty: HashMap<Ty, Vec<SSAValIdx>>,
    all: Vec<SSAValIdx>,
}

impl SSAValIdxs {
    fn per_ty(&self, ty: Ty) -> &[SSAValIdx] {
        self.per_ty.get(&ty).map(Deref::deref).unwrap_or_default()
    }
    fn extend<T: Borrow<SSAValIdx>>(
        &mut self,
        ssa_vals: &Vec<SSAVal>,
        ssa_val_idxs: impl IntoIterator<Item = T>,
    ) {
        for ssa_val_idx in ssa_val_idxs {
            let ssa_val_idx: SSAValIdx = *ssa_val_idx.borrow();
            self.per_ty
                .entry(ssa_vals[ssa_val_idx].ty)
                .or_default()
                .push(ssa_val_idx);
            self.all.push(ssa_val_idx);
        }
    }
}

struct FnBuilder<'a, 'b, 'g> {
    global_state: &'g GlobalState,
    u: &'a mut Unstructured<'b>,
    func: FnFields,
    available_ssa_vals_at_end: HashMap<BlockIdx, SSAValIdxs>,
}

impl FnBuilder<'_, '_, '_> {
    fn available_ssa_vals_at_end(&mut self, block_idx: Option<BlockIdx>) -> SSAValIdxs {
        let Some(block_idx) = block_idx else {
            return Default::default();
        };
        if let Some(retval) = self.available_ssa_vals_at_end.get(&block_idx) {
            return retval.clone();
        }
        let mut available_ssa_vals = self.available_ssa_vals_before_start(block_idx);
        let block = &self.func.blocks[block_idx];
        available_ssa_vals.extend(&self.func.ssa_vals, &block.params);
        for inst_idx in block.insts {
            let inst = &self.func.insts[inst_idx];
            available_ssa_vals.extend(
                &self.func.ssa_vals,
                inst.operands
                    .iter()
                    .filter(|o| o.kind_and_constraint.kind() == OperandKind::Def)
                    .map(|o| o.ssa_val),
            );
        }
        self.available_ssa_vals_at_end
            .insert(block_idx, available_ssa_vals.clone());
        available_ssa_vals
    }
    fn available_ssa_vals_before_start(&mut self, block_idx: BlockIdx) -> SSAValIdxs {
        let idom = self.func.blocks[block_idx].immediate_dominator;
        self.available_ssa_vals_at_end(idom)
    }
    fn new_ssa_val(ssa_vals: &mut Vec<SSAVal>, ty: Ty) -> SSAValIdx {
        let retval = SSAValIdx::new(ssa_vals.len());
        ssa_vals.push(SSAVal {
            ty,
            def: SSAValDef::invalid(),
            operand_uses: Default::default(),
            branch_succ_param_uses: Default::default(),
        });
        retval
    }
    fn new_inst_in_last_block(
        &mut self,
        kind: InstKind,
        operands: Vec<Operand>,
        clobbers: LocSet,
    ) -> InstIdx {
        let block = self.func.blocks.last_mut().expect("no block");
        let inst_idx = block.insts.end;
        assert_eq!(inst_idx.get(), self.func.insts.len());
        block.insts.end = block.insts.end.next();
        self.func.insts.push(Inst {
            kind,
            operands,
            clobbers: clobbers.into_interned(self.global_state),
        });
        inst_idx
    }
    fn make_copy_inst_kind(
        u: &mut Unstructured<'_>,
        // takes ref to Vec since Index<SSAValIdx> is only implemented for Vec
        ssa_vals: &Vec<SSAVal>,
        _inst_idx: InstIdx, // for debugging
        operands: &[Operand],
    ) -> Result<Option<CopyInstKind>, Error> {
        if operands.is_empty() {
            return Ok(None);
        }
        let mut possible_src_operand_idxs = vec![];
        let mut possible_dest_operand_idxs = vec![];
        for (operand_idx, operand) in operands.entries() {
            match operand.kind_and_constraint.kind() {
                OperandKind::Use => possible_src_operand_idxs.push(operand_idx),
                OperandKind::Def => possible_dest_operand_idxs.push(operand_idx),
            }
        }
        if possible_src_operand_idxs.is_empty() || possible_dest_operand_idxs.is_empty() {
            return Ok(None);
        }
        // src can have duplicates, so pick randomly
        let possible_src_operand_idxs = (0..u.int_in_range(1..=3)?)
            .map(|_| u.choose(&possible_src_operand_idxs).copied())
            .collect::<Result<Vec<OperandIdx>, Error>>()?;
        // dest can't have duplicates, so shuffle
        let len = possible_dest_operand_idxs.len();
        for i in 0..len {
            possible_dest_operand_idxs.swap(i, u.choose_index(len)?);
        }
        possible_dest_operand_idxs.truncate(u.int_in_range(1..=3)?);
        let mut copy_ty: Option<Ty> = None;
        let mut src_operand_idxs = vec![];
        let mut dest_operand_idxs = vec![];
        for src_operand_idx in possible_src_operand_idxs {
            let operand_type = ssa_vals[operands[src_operand_idx].ssa_val].ty;
            let proposed_copy_ty = match copy_ty.map(|ty| ty.try_concat(operand_type)) {
                None => operand_type,
                Some(Ok(ty)) => ty,
                Some(Err(_)) => continue,
            };
            let mut proposed_dest_operand_idxs = vec![];
            let mut dest_copy_ty: Option<Ty> = None;
            for &dest_operand_idx in &possible_dest_operand_idxs {
                let dest_operand_ty = ssa_vals[operands[dest_operand_idx].ssa_val].ty;
                if dest_operand_ty.base_ty != proposed_copy_ty.base_ty
                    || dest_operand_ty.reg_len > proposed_copy_ty.reg_len
                {
                    continue;
                }
                let next_dest_copy_ty = if let Some(dest_copy_ty) = dest_copy_ty {
                    let Ok(ty) = dest_copy_ty.try_concat(dest_operand_ty) else {
                        continue;
                    };
                    ty
                } else {
                    dest_operand_ty
                };
                if next_dest_copy_ty.reg_len > proposed_copy_ty.reg_len {
                    continue;
                }
                dest_copy_ty = Some(next_dest_copy_ty);
                proposed_dest_operand_idxs.push(dest_operand_idx);
            }
            if dest_copy_ty != Some(proposed_copy_ty) || proposed_dest_operand_idxs.is_empty() {
                continue;
            }
            copy_ty = Some(proposed_copy_ty);
            src_operand_idxs.push(src_operand_idx);
            dest_operand_idxs = proposed_dest_operand_idxs;
        }
        Ok(copy_ty.map(|copy_ty| CopyInstKind {
            src_operand_idxs,
            dest_operand_idxs,
            copy_ty,
        }))
    }
    fn run(&mut self) -> Result<(), Error> {
        let block_count = self.u.int_in_range(1..=10u16)?;
        for block_idx in RangeIter::<BlockIdx>::from_usize_range(0..block_count as usize) {
            let mut params = Vec::new();
            if block_idx != BlockIdx::ENTRY_BLOCK {
                for _param_idx in 0..self.u.int_in_range(0..=10)? {
                    let ty = self.u.arbitrary()?;
                    params.push(Self::new_ssa_val(&mut self.func.ssa_vals, ty));
                }
            }
            let end = InstIdx::new(self.func.insts.len());
            self.func.blocks.push(Block {
                params,
                insts: InstRange { start: end, end },
                preds: Default::default(),
                immediate_dominator: Default::default(),
            });
            for is_term in (0..self.u.int_in_range(0..=10)?)
                .map(|_| false)
                .chain([true])
            {
                let mut operands = vec![];
                for _ in 0..self.u.int_in_range(0..=5)? {
                    let kind = self.u.arbitrary()?;
                    let ssa_val = if let OperandKind::Def = kind {
                        let ty = self.u.arbitrary()?;
                        Self::new_ssa_val(&mut self.func.ssa_vals, ty)
                    } else {
                        SSAValIdx::new(!0)
                    };
                    operands.push(Operand {
                        ssa_val,
                        kind_and_constraint: KindAndConstraint::Constraint {
                            kind,
                            constraint: Constraint::Any,
                        },
                        stage: self.u.arbitrary()?,
                    });
                }
                let inst_kind = if is_term {
                    let mut succs_and_params = BTreeMap::default();
                    let succ_range = (BlockIdx::ENTRY_BLOCK.get() as u16 + 1)..=(block_count - 1);
                    if !succ_range.is_empty() {
                        for i in 0..self.u.int_in_range(0..=3usize)? {
                            if i > succ_range.len() {
                                break;
                            }
                            let succ =
                                BlockIdx::new(self.u.int_in_range(succ_range.clone())?.into());
                            succs_and_params.insert(succ, vec![]);
                        }
                    }
                    InstKind::BlockTerm(BlockTermInstKind { succs_and_params })
                } else {
                    InstKind::Normal
                };
                let clobbers = self.u.arbitrary()?;
                self.new_inst_in_last_block(inst_kind, operands, clobbers);
            }
        }
        for block_idx in self.func.blocks.keys() {
            let term_idx = self
                .func
                .try_get_block_term_inst_idx(block_idx)
                .expect("known to have block term inst");
            let successors = self.func.insts[term_idx]
                .kind
                .block_term()
                .expect("known to have block term inst")
                .succs_and_params
                .keys()
                .copied();
            for succ in successors {
                self.func.blocks[succ].preds.insert(block_idx);
            }
        }
        for block_idx in self.func.blocks.keys() {
            let term_idx = self
                .func
                .try_get_block_term_inst_idx(block_idx)
                .expect("known to have block term inst");
            let succs_and_params = &mut self.func.insts[term_idx]
                .kind
                .block_term_mut()
                .expect("known to have block term inst")
                .succs_and_params;
            if succs_and_params.len() <= 1 {
                continue;
            }
            if succs_and_params
                .keys()
                .all(|&succ_idx| self.func.blocks[succ_idx].preds.len() <= 1)
            {
                continue;
            }
            // has critical edges -- delete all but first succ to fix that
            let mut first = true;
            succs_and_params.retain(|&succ_idx, _| {
                if first {
                    first = false;
                    return true;
                }
                let succ = &mut self.func.blocks[succ_idx];
                succ.preds.remove(&block_idx);
                false
            });
        }
        let dominators = dominators::simple_fast(&self.func, BlockIdx::ENTRY_BLOCK);
        for block_idx in self.func.blocks.keys() {
            self.func.blocks[block_idx].immediate_dominator =
                dominators.immediate_dominator(block_idx);
        }
        // must have filled dominators first since available_ssa_vals_before_start() needs them
        for block_idx in self.func.blocks.keys() {
            let mut available_ssa_vals = self.available_ssa_vals_before_start(block_idx);
            available_ssa_vals.extend(&self.func.ssa_vals, &self.func.blocks[block_idx].params);
            for inst_idx in self.func.blocks[block_idx].insts {
                let inst = &mut self.func.insts[inst_idx];
                for (_operand_idx, operand) in inst.operands.entries_mut() {
                    if operand.kind_and_constraint.kind() != OperandKind::Use {
                        continue;
                    }
                    if available_ssa_vals.all.is_empty() {
                        // no ssa vals available, change to def
                        *operand = Operand {
                            kind_and_constraint: KindAndConstraint::Constraint {
                                kind: OperandKind::Def,
                                constraint: Constraint::Any,
                            },
                            ssa_val: Self::new_ssa_val(
                                &mut self.func.ssa_vals,
                                self.u.arbitrary()?,
                            ),
                            stage: self.u.arbitrary()?,
                        };
                    } else {
                        operand.ssa_val = *self.u.choose(&available_ssa_vals.all)?;
                    }
                }
                available_ssa_vals.extend(
                    &self.func.ssa_vals,
                    inst.operands
                        .iter()
                        .filter(|o| o.kind_and_constraint.kind() == OperandKind::Def)
                        .map(|o| o.ssa_val),
                );
                match &mut inst.kind {
                    InstKind::Normal => {}
                    InstKind::Copy(_) => unreachable!(),
                    InstKind::BlockTerm(block_term_inst_kind) => {
                        for (&succ_idx, params) in &mut block_term_inst_kind.succs_and_params {
                            let succ = &self.func.blocks[succ_idx];
                            for (_param_idx, &succ_param) in succ.params.entries() {
                                let succ_param_ty = self.func.ssa_vals[succ_param].ty;
                                let choices = available_ssa_vals.per_ty(succ_param_ty);
                                let ssa_val_idx;
                                if choices.is_empty() {
                                    // no available ssa vals with correct type, fix that by appending def operand with that type
                                    ssa_val_idx =
                                        Self::new_ssa_val(&mut self.func.ssa_vals, succ_param_ty);
                                    available_ssa_vals.extend(&self.func.ssa_vals, [ssa_val_idx]);
                                    inst.operands.push(Operand {
                                        kind_and_constraint: KindAndConstraint::Constraint {
                                            kind: OperandKind::Def,
                                            constraint: Constraint::Any,
                                        },
                                        ssa_val: ssa_val_idx,
                                        stage: self.u.arbitrary()?,
                                    });
                                } else {
                                    ssa_val_idx = *self.u.choose(choices)?
                                };
                                params.push(ssa_val_idx);
                            }
                        }
                    }
                }
            }
        }
        for (inst_idx, inst) in self.func.insts.entries_mut() {
            let mut reusable_operand_idxs: HashMap<Ty, Vec<OperandIdx>> = HashMap::new();
            for (operand_idx, operand) in inst.operands.entries() {
                if operand.kind_and_constraint.kind() == OperandKind::Use {
                    reusable_operand_idxs
                        .entry(self.func.ssa_vals[operand.ssa_val].ty)
                        .or_default()
                        .push(operand_idx);
                }
            }
            let mut used_locs = EnumMap::<InstStage, LocSet>::default();
            for operand_idx in inst.operands.keys() {
                let operand = &inst.operands[operand_idx];
                let operand_ty = self.func.ssa_vals[operand.ssa_val].ty;
                if operand.kind_and_constraint.kind() == OperandKind::Def && self.u.arbitrary()? {
                    let reusable_operand_idxs = reusable_operand_idxs
                        .get(&operand_ty)
                        .map(Deref::deref)
                        .unwrap_or_default();
                    if !reusable_operand_idxs.is_empty() {
                        let reuse_operand_idx = *self.u.choose(reusable_operand_idxs)?;
                        if reuse_operand_idx != operand_idx {
                            inst.operands[operand_idx].kind_and_constraint =
                                KindAndConstraint::Reuse {
                                    kind: OperandKindDefOnly,
                                    reuse_operand_idx,
                                };
                            continue;
                        }
                    }
                }
                let operand = &mut inst.operands[operand_idx];
                let KindAndConstraint::Constraint {
                    kind,
                    ref mut constraint,..
                } = operand.kind_and_constraint else {
                    unreachable!();
                };
                *constraint = Constraint::arbitrary_with_ty(operand_ty, self.u)?;
                if let Some(fixed_loc) = constraint.fixed_loc() {
                    // is the operand probably live for both stages
                    let probably_live_for_both_stages = match (kind, operand.stage) {
                        (OperandKind::Use, InstStage::Late) => true, // also probably live for early stage
                        (OperandKind::Def, InstStage::Early) => true, // also probably live for late stage
                        _ => false,
                    };
                    let mut conflicts = inst
                        .clobbers
                        .clone()
                        .conflicts_with(fixed_loc, self.global_state);
                    if probably_live_for_both_stages {
                        for used_locs in used_locs.values() {
                            conflicts = conflicts
                                || used_locs
                                    .to_interned(self.global_state)
                                    .conflicts_with(fixed_loc, self.global_state);
                        }
                    } else {
                        conflicts = conflicts
                            || used_locs[operand.stage]
                                .to_interned(self.global_state)
                                .conflicts_with(fixed_loc, self.global_state);
                    }
                    if conflicts {
                        // conflicting constraint, so switch to a safe default instead
                        *constraint = Constraint::Any;
                    } else if probably_live_for_both_stages {
                        for used_locs in used_locs.values_mut() {
                            used_locs.insert(fixed_loc);
                        }
                    } else {
                        used_locs[operand.stage].insert(fixed_loc);
                    }
                }
            }
            match inst.kind {
                InstKind::Normal => {
                    if self.u.arbitrary()? {
                        if let Some(copy_inst_kind) = Self::make_copy_inst_kind(
                            self.u,
                            &self.func.ssa_vals,
                            inst_idx,
                            &inst.operands,
                        )? {
                            inst.kind = InstKind::Copy(copy_inst_kind);
                        }
                    }
                }
                InstKind::Copy(_) => unreachable!(),
                InstKind::BlockTerm(_) => {}
            }
        }
        Ok(())
    }
}

impl<'a> Arbitrary<'a> for FnFields {
    fn arbitrary(u: &mut Unstructured<'a>) -> Result<Self, Error> {
        GlobalState::get(|global_state| {
            let mut builder = FnBuilder {
                global_state,
                u,
                func: Self {
                    ssa_vals: Vec::new(),
                    insts: Vec::new(),
                    blocks: Vec::new(),
                    start_inst_to_block_map: Default::default(),
                },
                available_ssa_vals_at_end: Default::default(),
            };
            builder.run()?;
            Ok(builder.func)
        })
    }
}
