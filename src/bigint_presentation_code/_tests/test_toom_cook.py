from contextlib import contextmanager
import sys
import unittest
from typing import Any, Callable, ContextManager, Iterator, Tuple, Iterable

from bigint_presentation_code.compiler_ir import (GPR_SIZE_IN_BITS,
                                                  GPR_SIZE_IN_BYTES,
                                                  GPR_VALUE_MASK, BaseSimState,
                                                  Fn, GenAsmState, OpKind,
                                                  PostRASimState,
                                                  PreRASimState, SSAVal)
from bigint_presentation_code.register_allocator import allocate_registers
from bigint_presentation_code.register_allocator_test_util import GraphDumper
from bigint_presentation_code.toom_cook import (ToomCookInstance, ToomCookMul,
                                                simple_mul)
from bigint_presentation_code.util import OSet

_StateFactory = Callable[[], ContextManager[BaseSimState]]


def simple_umul(fn, lhs, rhs):
    # type: (Fn, SSAVal, SSAVal) -> tuple[SSAVal, None]
    return simple_mul(fn=fn, lhs=lhs, lhs_signed=False, rhs=rhs,
                      rhs_signed=False, name="mul"), None


def get_pre_ra_state_factory(code):
    # type: (Mul) -> _StateFactory
    @contextmanager
    def state_factory():
        state = PreRASimState(ssa_vals={}, memory={})
        with state.set_as_current_debugging_state():
            yield state
    return state_factory


class Mul:
    _MulFn = Callable[[Fn, SSAVal, SSAVal], Tuple[SSAVal, Any]]

    def __init__(self, mul, lhs_size_in_words, rhs_size_in_words):
        # type: (_MulFn, int, int) -> None
        super().__init__()
        self.fn = fn = Fn()
        self.dest_offset = 0
        self.dest_size_in_words = lhs_size_in_words + rhs_size_in_words
        self.dest_size_in_bytes = self.dest_size_in_words * GPR_SIZE_IN_BYTES
        self.lhs_size_in_words = lhs_size_in_words
        self.lhs_size_in_bytes = self.lhs_size_in_words * GPR_SIZE_IN_BYTES
        self.rhs_size_in_words = rhs_size_in_words
        self.rhs_size_in_bytes = self.rhs_size_in_words * GPR_SIZE_IN_BYTES
        self.lhs_offset = self.dest_size_in_bytes + self.dest_offset
        self.rhs_offset = self.lhs_size_in_bytes + self.lhs_offset
        self.ptr_in = fn.append_new_op(kind=OpKind.FuncArgR3,
                                       name="ptr_in").outputs[0]
        self.lhs_setvl = fn.append_new_op(
            kind=OpKind.SetVLI, immediates=[lhs_size_in_words],
            maxvl=lhs_size_in_words, name="lhs_setvl")
        self.load_lhs = fn.append_new_op(
            kind=OpKind.SvLd, immediates=[self.lhs_offset],
            input_vals=[self.ptr_in, self.lhs_setvl.outputs[0]],
            name="load_lhs", maxvl=lhs_size_in_words)
        self.rhs_setvl = fn.append_new_op(
            kind=OpKind.SetVLI, immediates=[rhs_size_in_words],
            maxvl=rhs_size_in_words, name="rhs_setvl")
        self.load_rhs = fn.append_new_op(
            kind=OpKind.SvLd, immediates=[self.rhs_offset],
            input_vals=[self.ptr_in, self.rhs_setvl.outputs[0]],
            name="load_rhs", maxvl=rhs_size_in_words)
        self.retval = mul(
            fn, self.load_lhs.outputs[0], self.load_rhs.outputs[0])
        self.dest_setvl = fn.append_new_op(
            kind=OpKind.SetVLI, immediates=[self.dest_size_in_words],
            maxvl=self.dest_size_in_words, name="dest_setvl")
        self.store = fn.append_new_op(
            kind=OpKind.SvStd,
            input_vals=[self.retval[0], self.ptr_in,
                        self.dest_setvl.outputs[0]],
            immediates=[self.dest_offset], maxvl=self.dest_size_in_words,
            name="store_dest")


class TestToomCook(unittest.TestCase):
    maxDiff = None

    def get_post_ra_state_factory(self, code):
        # type: (Mul) -> _StateFactory
        ssa_val_to_loc_map = allocate_registers(
            code.fn, debug_out=sys.stdout, dump_graph=GraphDumper(self))

        @contextmanager
        def state_factory():
            yield PostRASimState(
                ssa_val_to_loc_map=ssa_val_to_loc_map,
                memory={}, loc_values={})
        return state_factory

    def test_toom_2_repr(self):
        TOOM_2 = ToomCookInstance.make_toom_2()
        # print(repr(repr(TOOM_2)))
        self.assertEqual(
            repr(TOOM_2),
            "ToomCookInstance(lhs_part_count=2, rhs_part_count=2, "
            "eval_points=(0, 1, POINT_AT_INFINITY), "
            "lhs_eval_ops=("
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "EvalOpAdd(lhs="
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "rhs="
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)})), "
            "poly=EvalOpPoly({0: Fraction(1, 1), 1: Fraction(1, 1)})), "
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)}))),"
            " rhs_eval_ops=("
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "EvalOpAdd(lhs="
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "rhs="
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)})), "
            "poly=EvalOpPoly({0: Fraction(1, 1), 1: Fraction(1, 1)})), "
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)}))),"
            " prod_eval_ops=("
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "EvalOpSub(lhs="
            "EvalOpSub(lhs="
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)})), "
            "rhs="
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "poly=EvalOpPoly({0: Fraction(-1, 1), 1: Fraction(1, 1)})), "
            "rhs="
            "EvalOpInput(lhs=2, rhs=0, poly=EvalOpPoly({2: Fraction(1, 1)})), "
            "poly=EvalOpPoly({"
            "0: Fraction(-1, 1), 1: Fraction(1, 1), 2: Fraction(-1, 1)})), "
            "EvalOpInput(lhs=2, rhs=0, poly=EvalOpPoly({2: Fraction(1, 1)}))))"
        )

    def test_toom_2_5_repr(self):
        TOOM_2_5 = ToomCookInstance.make_toom_2_5()
        # print(repr(repr(TOOM_2_5)))
        self.assertEqual(
            repr(TOOM_2_5),
            "ToomCookInstance(lhs_part_count=3, rhs_part_count=2, "
            "eval_points=(0, 1, -1, POINT_AT_INFINITY), lhs_eval_ops=("
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "EvalOpAdd(lhs="
            "EvalOpAdd(lhs="
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "rhs=EvalOpInput(lhs=2, rhs=0, "
            "poly=EvalOpPoly({2: Fraction(1, 1)})), "
            "poly=EvalOpPoly({0: Fraction(1, 1), 2: Fraction(1, 1)})), "
            "rhs=EvalOpInput(lhs=1, rhs=0, "
            "poly=EvalOpPoly({1: Fraction(1, 1)})), "
            "poly=EvalOpPoly({"
            "0: Fraction(1, 1), 1: Fraction(1, 1), 2: Fraction(1, 1)})), "
            "EvalOpSub(lhs="
            "EvalOpAdd(lhs="
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "rhs=EvalOpInput(lhs=2, rhs=0, "
            "poly=EvalOpPoly({2: Fraction(1, 1)})), "
            "poly=EvalOpPoly({0: Fraction(1, 1), 2: Fraction(1, 1)})), "
            "rhs=EvalOpInput(lhs=1, rhs=0, "
            "poly=EvalOpPoly({1: Fraction(1, 1)})), poly=EvalOpPoly("
            "{0: Fraction(1, 1), 1: Fraction(-1, 1), 2: Fraction(1, 1)})), "
            "EvalOpInput(lhs=2, rhs=0, "
            "poly=EvalOpPoly({2: Fraction(1, 1)}))), rhs_eval_ops=("
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "EvalOpAdd(lhs=EvalOpInput(lhs=0, rhs=0, "
            "poly=EvalOpPoly({0: Fraction(1, 1)})), rhs="
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)})), "
            "poly=EvalOpPoly({0: Fraction(1, 1), 1: Fraction(1, 1)})), "
            "EvalOpSub(lhs="
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "rhs=EvalOpInput(lhs=1, rhs=0, "
            "poly=EvalOpPoly({1: Fraction(1, 1)})), "
            "poly=EvalOpPoly({0: Fraction(1, 1), 1: Fraction(-1, 1)})), "
            "EvalOpInput(lhs=1, rhs=0, "
            "poly=EvalOpPoly({1: Fraction(1, 1)}))), "
            "prod_eval_ops=("
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "EvalOpSub(lhs=EvalOpExactDiv(lhs=EvalOpSub(lhs="
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)})), "
            "rhs=EvalOpInput(lhs=2, rhs=0, "
            "poly=EvalOpPoly({2: Fraction(1, 1)})), "
            "poly=EvalOpPoly({1: Fraction(1, 1), 2: Fraction(-1, 1)})), "
            "rhs=2, "
            "poly=EvalOpPoly({1: Fraction(1, 2), 2: Fraction(-1, 2)})), rhs="
            "EvalOpInput(lhs=3, rhs=0, poly=EvalOpPoly({3: Fraction(1, 1)})), "
            "poly=EvalOpPoly("
            "{1: Fraction(1, 2), 2: Fraction(-1, 2), 3: Fraction(-1, 1)})), "
            "EvalOpSub(lhs=EvalOpExactDiv(lhs=EvalOpAdd(lhs="
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)})), "
            "rhs="
            "EvalOpInput(lhs=2, rhs=0, poly=EvalOpPoly({2: Fraction(1, 1)})), "
            "poly=EvalOpPoly({1: Fraction(1, 1), 2: Fraction(1, 1)})), rhs=2, "
            "poly=EvalOpPoly({1: Fraction(1, 2), 2: Fraction(1, 2)})), rhs="
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "poly=EvalOpPoly("
            "{0: Fraction(-1, 1), 1: Fraction(1, 2), 2: Fraction(1, 2)})), "
            "EvalOpInput(lhs=3, rhs=0, poly=EvalOpPoly({3: Fraction(1, 1)}))))"
        )

    def test_reversed_toom_2_5_repr(self):
        TOOM_2_5 = ToomCookInstance.make_toom_2_5().reversed()
        # print(repr(repr(TOOM_2_5)))
        self.assertEqual(
            repr(TOOM_2_5),
            "ToomCookInstance(lhs_part_count=2, rhs_part_count=3, "
            "eval_points=(0, 1, -1, POINT_AT_INFINITY), lhs_eval_ops=("
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "EvalOpAdd(lhs="
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "rhs="
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)})), "
            "poly=EvalOpPoly({0: Fraction(1, 1), 1: Fraction(1, 1)})), "
            "EvalOpSub(lhs="
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "rhs="
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)})), "
            "poly=EvalOpPoly({0: Fraction(1, 1), 1: Fraction(-1, 1)})), "
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)}))),"
            " rhs_eval_ops=("
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "EvalOpAdd(lhs=EvalOpAdd(lhs="
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "rhs="
            "EvalOpInput(lhs=2, rhs=0, poly=EvalOpPoly({2: Fraction(1, 1)})), "
            "poly=EvalOpPoly({0: Fraction(1, 1), 2: Fraction(1, 1)})), rhs="
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)})), "
            "poly=EvalOpPoly("
            "{0: Fraction(1, 1), 1: Fraction(1, 1), 2: Fraction(1, 1)})), "
            "EvalOpSub(lhs=EvalOpAdd(lhs="
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "rhs="
            "EvalOpInput(lhs=2, rhs=0, poly=EvalOpPoly({2: Fraction(1, 1)})), "
            "poly=EvalOpPoly({0: Fraction(1, 1), 2: Fraction(1, 1)})), rhs="
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)})), "
            "poly=EvalOpPoly("
            "{0: Fraction(1, 1), 1: Fraction(-1, 1), 2: Fraction(1, 1)})), "
            "EvalOpInput(lhs=2, rhs=0, poly=EvalOpPoly({2: Fraction(1, 1)}))),"
            " prod_eval_ops=("
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "EvalOpSub(lhs=EvalOpExactDiv(lhs=EvalOpSub(lhs="
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)})), "
            "rhs="
            "EvalOpInput(lhs=2, rhs=0, poly=EvalOpPoly({2: Fraction(1, 1)})), "
            "poly=EvalOpPoly({1: Fraction(1, 1), 2: Fraction(-1, 1)})), "
            "rhs=2, "
            "poly=EvalOpPoly({1: Fraction(1, 2), 2: Fraction(-1, 2)})), rhs="
            "EvalOpInput(lhs=3, rhs=0, poly=EvalOpPoly({3: Fraction(1, 1)})), "
            "poly=EvalOpPoly("
            "{1: Fraction(1, 2), 2: Fraction(-1, 2), 3: Fraction(-1, 1)})), "
            "EvalOpSub(lhs=EvalOpExactDiv(lhs=EvalOpAdd(lhs="
            "EvalOpInput(lhs=1, rhs=0, poly=EvalOpPoly({1: Fraction(1, 1)})), "
            "rhs="
            "EvalOpInput(lhs=2, rhs=0, poly=EvalOpPoly({2: Fraction(1, 1)})), "
            "poly=EvalOpPoly({1: Fraction(1, 1), 2: Fraction(1, 1)})), rhs=2, "
            "poly=EvalOpPoly({1: Fraction(1, 2), 2: Fraction(1, 2)})), rhs="
            "EvalOpInput(lhs=0, rhs=0, poly=EvalOpPoly({0: Fraction(1, 1)})), "
            "poly=EvalOpPoly("
            "{0: Fraction(-1, 1), 1: Fraction(1, 2), 2: Fraction(1, 2)})), "
            "EvalOpInput(lhs=3, rhs=0, poly=EvalOpPoly({3: Fraction(1, 1)}))))"
        )

    def test_simple_mul_192x192_pre_ra_sim(self):
        for lhs_signed in False, True:
            for rhs_signed in False, True:
                self.tst_simple_mul_192x192_sim(
                    lhs_signed=lhs_signed, rhs_signed=rhs_signed,
                    get_state_factory=get_pre_ra_state_factory)

    def test_simple_mul_192x192_post_ra_sim(self):
        for lhs_signed in False, True:
            for rhs_signed in False, True:
                self.tst_simple_mul_192x192_sim(
                    lhs_signed=lhs_signed, rhs_signed=rhs_signed,
                    get_state_factory=self.get_post_ra_state_factory)

    def tst_simple_mul_192x192_sim(
        self, lhs_signed,  # type: bool
        rhs_signed,  # type: bool
        get_state_factory,  # type: Callable[[Mul], _StateFactory]
    ):
        # test multiplying:
        #   0x000191acb262e15b_4c6b5f2b19e1a53e_821a2342132c5b57
        # * 0x4a37c0567bcbab53_cf1f597598194ae6_208a49071aeec507
        # ==
        # int("0x00074736574206e_6f69746163696c70"
        #     "_69746c756d207469_622d3438333e2d32"
        #     "_3931783239312079_7261727469627261", base=0)
        # == int.from_bytes(b"arbitrary 192x192->384-bit multiplication test",
        #                   'little')
        lhs_value = 0x000191acb262e15b_4c6b5f2b19e1a53e_821a2342132c5b57
        rhs_value = 0x4a37c0567bcbab53_cf1f597598194ae6_208a49071aeec507
        prod_value = int.from_bytes(
            b"arbitrary 192x192->384-bit multiplication test", 'little')
        self.assertEqual(lhs_value * rhs_value, prod_value)
        code = Mul(
            mul=lambda fn, lhs, rhs: (simple_mul(
                fn=fn, lhs=lhs, lhs_signed=lhs_signed,
                rhs=rhs, rhs_signed=rhs_signed, name="mul"), None),
            lhs_size_in_words=3, rhs_size_in_words=3)
        state_factory = get_state_factory(code)
        ptr_in = 0x100
        dest_ptr = ptr_in + code.dest_offset
        lhs_ptr = ptr_in + code.lhs_offset
        rhs_ptr = ptr_in + code.rhs_offset
        for lhs_neg in False, True:
            for rhs_neg in False, True:
                if lhs_neg and not lhs_signed:
                    continue
                if rhs_neg and not rhs_signed:
                    continue
                with self.subTest(lhs_signed=lhs_signed,
                                  rhs_signed=rhs_signed,
                                  lhs_neg=lhs_neg, rhs_neg=rhs_neg):
                    with state_factory() as state:
                        state[code.ptr_in] = ptr_in,
                        lhs = lhs_value
                        if lhs_neg:
                            lhs = 2 ** 192 - lhs
                        rhs = rhs_value
                        if rhs_neg:
                            rhs = 2 ** 192 - rhs
                        for i in range(3):
                            v = (lhs >> GPR_SIZE_IN_BITS * i) & GPR_VALUE_MASK
                            state.store(lhs_ptr + i * GPR_SIZE_IN_BYTES, v)
                        for i in range(3):
                            v = (rhs >> GPR_SIZE_IN_BITS * i) & GPR_VALUE_MASK
                            state.store(rhs_ptr + i * GPR_SIZE_IN_BYTES, v)
                        code.fn.sim(state)
                        expected = prod_value
                        if lhs_neg != rhs_neg:
                            expected = 2 ** 384 - expected
                        prod = 0
                        for i in range(6):
                            v = state.load(dest_ptr + GPR_SIZE_IN_BYTES * i)
                            prod += v << (GPR_SIZE_IN_BITS * i)
                        self.assertEqual(hex(prod), hex(expected))

    def test_simple_mul_192x192_ops(self):
        code = Mul(mul=simple_umul, lhs_size_in_words=3, rhs_size_in_words=3)
        fn = code.fn
        self.assertEqual(
            fn.ops_to_str(),
            "ptr_in:\n"
            "    (<...outputs[0]: <I64>>) <= FuncArgR3\n"
            "lhs_setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x3)\n"
            "load_lhs:\n"
            "    (<...outputs[0]: <I64*3>>) <= SvLd(\n"
            "    <ptr_in.outputs[0]: <I64>>,\n"
            "    <lhs_setvl.outputs[0]: <VL_MAXVL>>, 0x30)\n"
            "rhs_setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x3)\n"
            "load_rhs:\n"
            "    (<...outputs[0]: <I64*3>>) <= SvLd(\n"
            "    <ptr_in.outputs[0]: <I64>>,\n"
            "    <rhs_setvl.outputs[0]: <VL_MAXVL>>, 0x48)\n"
            "mul_rhs_setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x3)\n"
            "mul_rhs_spread:\n"
            "    (<...outputs[0]: <I64>>, <...outputs[1]: <I64>>,\n"
            "    <...outputs[2]: <I64>>) <= Spread(\n"
            "    <load_rhs.outputs[0]: <I64*3>>,\n"
            "    <mul_rhs_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_zero:\n"
            "    (<...outputs[0]: <I64>>) <= LI(0x0)\n"
            "mul_lhs_setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x3)\n"
            "mul_zero2:\n"
            "    (<...outputs[0]: <I64>>) <= LI(0x0)\n"
            "mul_0_mul:\n"
            "    (<...outputs[0]: <I64*3>>, <...outputs[1]: <I64>>\n"
            "    ) <= SvMAddEDU(<load_lhs.outputs[0]: <I64*3>>,\n"
            "    <mul_rhs_spread.outputs[0]: <I64>>,\n"
            "    <mul_zero.outputs[0]: <I64>>,\n"
            "    <mul_lhs_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_0_mul_rt_spread:\n"
            "    (<...outputs[0]: <I64>>, <...outputs[1]: <I64>>,\n"
            "    <...outputs[2]: <I64>>) <= Spread(\n"
            "    <mul_0_mul.outputs[0]: <I64*3>>,\n"
            "    <mul_lhs_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_1_mul:\n"
            "    (<...outputs[0]: <I64*3>>, <...outputs[1]: <I64>>\n"
            "    ) <= SvMAddEDU(<load_lhs.outputs[0]: <I64*3>>,\n"
            "    <mul_rhs_spread.outputs[1]: <I64>>,\n"
            "    <mul_zero.outputs[0]: <I64>>,\n"
            "    <mul_lhs_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_1_mul_rt_spread:\n"
            "    (<...outputs[0]: <I64>>, <...outputs[1]: <I64>>,\n"
            "    <...outputs[2]: <I64>>) <= Spread(\n"
            "    <mul_1_mul.outputs[0]: <I64*3>>,\n"
            "    <mul_lhs_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_1_cast_retval_zero:\n"
            "    (<...outputs[0]: <I64>>) <= LI(0x0)\n"
            "mul_1_cast_pp_zero:\n"
            "    (<...outputs[0]: <I64>>) <= LI(0x0)\n"
            "mul_1_setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x5)\n"
            "mul_1_retval_concat:\n"
            "    (<...outputs[0]: <I64*5>>) <= Concat(\n"
            "    <mul_0_mul_rt_spread.outputs[1]: <I64>>,\n"
            "    <mul_0_mul_rt_spread.outputs[2]: <I64>>,\n"
            "    <mul_0_mul.outputs[1]: <I64>>,\n"
            "    <mul_1_cast_retval_zero.outputs[0]: <I64>>,\n"
            "    <mul_1_cast_retval_zero.outputs[0]: <I64>>,\n"
            "    <mul_1_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_1_pp_concat:\n"
            "    (<...outputs[0]: <I64*5>>) <= Concat(\n"
            "    <mul_1_mul_rt_spread.outputs[0]: <I64>>,\n"
            "    <mul_1_mul_rt_spread.outputs[1]: <I64>>,\n"
            "    <mul_1_mul_rt_spread.outputs[2]: <I64>>,\n"
            "    <mul_1_mul.outputs[1]: <I64>>,\n"
            "    <mul_1_cast_pp_zero.outputs[0]: <I64>>,\n"
            "    <mul_1_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_1_clear_ca:\n"
            "    (<...outputs[0]: <CA>>) <= ClearCA\n"
            "mul_1_add:\n"
            "    (<...outputs[0]: <I64*5>>, <...outputs[1]: <CA>>\n"
            "    ) <= SvAddE(<mul_1_retval_concat.outputs[0]: <I64*5>>,\n"
            "    <mul_1_pp_concat.outputs[0]: <I64*5>>,\n"
            "    <mul_1_clear_ca.outputs[0]: <CA>>,\n"
            "    <mul_1_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_1_sum_spread:\n"
            "    (<...outputs[0]: <I64>>, <...outputs[1]: <I64>>,\n"
            "    <...outputs[2]: <I64>>, <...outputs[3]: <I64>>,\n"
            "    <...outputs[4]: <I64>>) <= Spread(\n"
            "    <mul_1_add.outputs[0]: <I64*5>>,\n"
            "    <mul_1_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_2_mul:\n"
            "    (<...outputs[0]: <I64*3>>, <...outputs[1]: <I64>>\n"
            "    ) <= SvMAddEDU(<load_lhs.outputs[0]: <I64*3>>,\n"
            "    <mul_rhs_spread.outputs[2]: <I64>>,\n"
            "    <mul_zero.outputs[0]: <I64>>,\n"
            "    <mul_lhs_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_2_mul_rt_spread:\n"
            "    (<...outputs[0]: <I64>>, <...outputs[1]: <I64>>,\n"
            "    <...outputs[2]: <I64>>) <= Spread(\n"
            "    <mul_2_mul.outputs[0]: <I64*3>>,\n"
            "    <mul_lhs_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_2_setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x4)\n"
            "mul_2_retval_concat:\n"
            "    (<...outputs[0]: <I64*4>>) <= Concat(\n"
            "    <mul_1_sum_spread.outputs[1]: <I64>>,\n"
            "    <mul_1_sum_spread.outputs[2]: <I64>>,\n"
            "    <mul_1_sum_spread.outputs[3]: <I64>>,\n"
            "    <mul_1_sum_spread.outputs[4]: <I64>>,\n"
            "    <mul_2_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_2_pp_concat:\n"
            "    (<...outputs[0]: <I64*4>>) <= Concat(\n"
            "    <mul_2_mul_rt_spread.outputs[0]: <I64>>,\n"
            "    <mul_2_mul_rt_spread.outputs[1]: <I64>>,\n"
            "    <mul_2_mul_rt_spread.outputs[2]: <I64>>,\n"
            "    <mul_2_mul.outputs[1]: <I64>>,\n"
            "    <mul_2_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_2_clear_ca:\n"
            "    (<...outputs[0]: <CA>>) <= ClearCA\n"
            "mul_2_add:\n"
            "    (<...outputs[0]: <I64*4>>, <...outputs[1]: <CA>>\n"
            "    ) <= SvAddE(<mul_2_retval_concat.outputs[0]: <I64*4>>,\n"
            "    <mul_2_pp_concat.outputs[0]: <I64*4>>,\n"
            "    <mul_2_clear_ca.outputs[0]: <CA>>,\n"
            "    <mul_2_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_2_sum_spread:\n"
            "    (<...outputs[0]: <I64>>, <...outputs[1]: <I64>>,\n"
            "    <...outputs[2]: <I64>>, <...outputs[3]: <I64>>) <= Spread(\n"
            "    <mul_2_add.outputs[0]: <I64*4>>,\n"
            "    <mul_2_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "mul_setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x6)\n"
            "mul_concat:\n"
            "    (<...outputs[0]: <I64*6>>) <= Concat(\n"
            "    <mul_0_mul_rt_spread.outputs[0]: <I64>>,\n"
            "    <mul_1_sum_spread.outputs[0]: <I64>>,\n"
            "    <mul_2_sum_spread.outputs[0]: <I64>>,\n"
            "    <mul_2_sum_spread.outputs[1]: <I64>>,\n"
            "    <mul_2_sum_spread.outputs[2]: <I64>>,\n"
            "    <mul_2_sum_spread.outputs[3]: <I64>>,\n"
            "    <mul_setvl.outputs[0]: <VL_MAXVL>>)\n"
            "dest_setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x6)\n"
            "store_dest:\n"
            "    SvStd(<mul_concat.outputs[0]: <I64*6>>,\n"
            "    <ptr_in.outputs[0]: <I64>>,\n"
            "    <dest_setvl.outputs[0]: <VL_MAXVL>>, 0x0)"
        )

    def test_simple_mul_192x192_reg_alloc(self):
        code = Mul(mul=simple_umul, lhs_size_in_words=3, rhs_size_in_words=3)
        fn = code.fn
        assigned_registers = allocate_registers(
            fn, debug_out=sys.stdout, dump_graph=GraphDumper(self))
        print(repr(assigned_registers))
        self.assertEqual(
            repr(assigned_registers), "{"
            "<mul_1_cast_pp_zero.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=7, reg_len=1), "
            "<mul_1_mul_rt_spread.out2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=5, reg_len=1), "
            "<mul_1_pp_concat.out0.copy.outputs[0]: <I64*5>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=5), "
            "<mul_1_cast_pp_zero.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=7, reg_len=1), "
            "<mul_1_mul_rt_spread.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<mul_1_pp_concat.inp0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<mul_1_pp_concat.inp1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=4, reg_len=1), "
            "<mul_1_pp_concat.inp2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=5, reg_len=1), "
            "<mul_1_pp_concat.inp3.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=6, reg_len=1), "
            "<mul_1_pp_concat.inp4.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=7, reg_len=1), "
            "<mul_1_add.inp1.copy.outputs[0]: <I64*5>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=5), "
            "<mul_1_mul.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=3), "
            "<mul_1_mul.out0.copy.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=3), "
            "<mul_1_mul_rt_spread.inp0.copy.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=3), "
            "<mul_1_mul_rt_spread.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<mul_1_mul_rt_spread.outputs[1]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=4, reg_len=1), "
            "<mul_1_mul_rt_spread.outputs[2]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=5, reg_len=1), "
            "<mul_1_mul_rt_spread.out1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=4, reg_len=1), "
            "<mul_1_pp_concat.outputs[0]: <I64*5>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=5), "
            "<mul_2_mul_rt_spread.out1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=4, reg_len=1), "
            "<mul_2_pp_concat.outputs[0]: <I64*4>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=4), "
            "<mul_2_mul_rt_spread.out2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=5, reg_len=1), "
            "<mul_2_pp_concat.out0.copy.outputs[0]: <I64*4>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=4), "
            "<mul_2_add.inp1.copy.outputs[0]: <I64*4>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=4), "
            "<mul_2_mul.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=3), "
            "<mul_2_mul.out0.copy.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=3), "
            "<mul_2_mul_rt_spread.inp0.copy.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=3), "
            "<mul_2_mul_rt_spread.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<mul_2_mul_rt_spread.outputs[1]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=4, reg_len=1), "
            "<mul_2_mul_rt_spread.outputs[2]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=5, reg_len=1), "
            "<mul_2_mul_rt_spread.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<mul_2_pp_concat.inp0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<mul_2_pp_concat.inp1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=4, reg_len=1), "
            "<mul_2_pp_concat.inp2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=5, reg_len=1), "
            "<mul_2_pp_concat.inp3.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=6, reg_len=1), "
            "<mul_zero.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=6, reg_len=1), "
            "<mul_0_mul.inp2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=6, reg_len=1), "
            "<mul_0_mul.outputs[1]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=6, reg_len=1), "
            "<mul_1_mul.inp2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=6, reg_len=1), "
            "<mul_1_mul.outputs[1]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=6, reg_len=1), "
            "<mul_1_mul.out1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=6, reg_len=1), "
            "<mul_2_mul.inp2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=6, reg_len=1), "
            "<mul_2_mul.outputs[1]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=6, reg_len=1), "
            "<mul_2_mul.out1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=6, reg_len=1), "
            "<mul_0_mul_rt_spread.inp0.copy.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=3), "
            "<mul_0_mul_rt_spread.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=1), "
            "<mul_0_mul_rt_spread.outputs[1]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=15, reg_len=1), "
            "<mul_0_mul_rt_spread.outputs[2]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=1), "
            "<mul_0_mul_rt_spread.out1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=15, reg_len=1), "
            "<mul_0_mul.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=3), "
            "<mul_0_mul.out0.copy.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=3), "
            "<mul_1_retval_concat.outputs[0]: <I64*5>>: "
            "Loc(kind=LocKind.GPR, start=15, reg_len=5), "
            "<mul_1_add.inp0.copy.outputs[0]: <I64*5>>: "
            "Loc(kind=LocKind.GPR, start=15, reg_len=5), "
            "<mul_0_mul_rt_spread.out2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=1), "
            "<mul_0_mul.out1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=17, reg_len=1), "
            "<mul_1_retval_concat.out0.copy.outputs[0]: <I64*5>>: "
            "Loc(kind=LocKind.GPR, start=15, reg_len=5), "
            "<mul_1_cast_retval_zero.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=18, reg_len=1), "
            "<mul_1_retval_concat.inp0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=15, reg_len=1), "
            "<mul_1_retval_concat.inp1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=1), "
            "<mul_1_retval_concat.inp2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=17, reg_len=1), "
            "<mul_1_retval_concat.inp3.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=18, reg_len=1), "
            "<mul_1_retval_concat.inp4.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=19, reg_len=1), "
            "<mul_2_sum_spread.out3.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=19, reg_len=1), "
            "<mul_1_sum_spread.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=15, reg_len=1), "
            "<mul_0_mul_rt_spread.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=1), "
            "<mul_concat.out0.copy.outputs[0]: <I64*6>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=6), "
            "<mul_2_add.inp0.copy.outputs[0]: <I64*4>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=4), "
            "<mul_1_add.outputs[0]: <I64*5>>: "
            "Loc(kind=LocKind.GPR, start=15, reg_len=5), "
            "<mul_1_add.out0.copy.outputs[0]: <I64*5>>: "
            "Loc(kind=LocKind.GPR, start=15, reg_len=5), "
            "<mul_2_sum_spread.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=1), "
            "<mul_concat.inp0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=1), "
            "<mul_concat.inp1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=15, reg_len=1), "
            "<mul_concat.inp2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=1), "
            "<mul_concat.inp3.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=17, reg_len=1), "
            "<mul_concat.inp4.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=18, reg_len=1), "
            "<mul_concat.inp5.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=19, reg_len=1), "
            "<mul_1_sum_spread.inp0.copy.outputs[0]: <I64*5>>: "
            "Loc(kind=LocKind.GPR, start=15, reg_len=5), "
            "<mul_1_sum_spread.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=15, reg_len=1), "
            "<mul_1_sum_spread.outputs[1]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=1), "
            "<mul_1_sum_spread.outputs[2]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=17, reg_len=1), "
            "<mul_1_sum_spread.outputs[3]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=18, reg_len=1), "
            "<mul_1_sum_spread.outputs[4]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=19, reg_len=1), "
            "<mul_1_sum_spread.out2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=17, reg_len=1), "
            "<mul_2_retval_concat.outputs[0]: <I64*4>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=4), "
            "<mul_1_sum_spread.out3.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=18, reg_len=1), "
            "<mul_2_retval_concat.out0.copy.outputs[0]: <I64*4>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=4), "
            "<mul_1_sum_spread.out4.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=19, reg_len=1), "
            "<mul_1_sum_spread.out1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=1), "
            "<mul_2_retval_concat.inp0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=1), "
            "<mul_2_retval_concat.inp1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=17, reg_len=1), "
            "<mul_2_retval_concat.inp2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=18, reg_len=1), "
            "<mul_2_retval_concat.inp3.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=19, reg_len=1), "
            "<mul_2_add.outputs[0]: <I64*4>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=4), "
            "<mul_2_add.out0.copy.outputs[0]: <I64*4>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=4), "
            "<mul_2_sum_spread.inp0.copy.outputs[0]: <I64*4>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=4), "
            "<mul_2_sum_spread.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=1), "
            "<mul_2_sum_spread.outputs[1]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=17, reg_len=1), "
            "<mul_2_sum_spread.outputs[2]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=18, reg_len=1), "
            "<mul_2_sum_spread.outputs[3]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=19, reg_len=1), "
            "<mul_2_sum_spread.out1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=17, reg_len=1), "
            "<mul_concat.outputs[0]: <I64*6>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=6), "
            "<mul_2_sum_spread.out2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=18, reg_len=1), "
            "<store_dest.inp0.copy.outputs[0]: <I64*6>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=6), "
            "<mul_1_cast_retval_zero.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=8, reg_len=1), "
            "<mul_zero.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=9, reg_len=1), "
            "<ptr_in.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=10, reg_len=1), "
            "<store_dest.inp1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=10, reg_len=1), "
            "<load_lhs.inp0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=10, reg_len=1), "
            "<load_rhs.inp0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=10, reg_len=1), "
            "<ptr_in.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<mul_2_mul.inp1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=22, reg_len=1), "
            "<load_rhs.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=20, reg_len=3), "
            "<load_rhs.out0.copy.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=20, reg_len=3), "
            "<mul_rhs_spread.out2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=22, reg_len=1), "
            "<mul_rhs_spread.out1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=21, reg_len=1), "
            "<mul_1_mul.inp1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=21, reg_len=1), "
            "<mul_rhs_spread.inp0.copy.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=20, reg_len=3), "
            "<mul_rhs_spread.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=20, reg_len=1), "
            "<mul_rhs_spread.outputs[1]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=21, reg_len=1), "
            "<mul_rhs_spread.outputs[2]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=22, reg_len=1), "
            "<mul_rhs_spread.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=20, reg_len=1), "
            "<mul_0_mul.inp1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=20, reg_len=1), "
            "<load_lhs.out0.copy.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=24, reg_len=3), "
            "<load_lhs.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=24, reg_len=3), "
            "<mul_0_mul.inp0.copy.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=24, reg_len=3), "
            "<mul_1_mul.inp0.copy.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=24, reg_len=3), "
            "<mul_2_mul.inp0.copy.outputs[0]: <I64*3>>: "
            "Loc(kind=LocKind.GPR, start=24, reg_len=3), "
            "<mul_zero2.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<mul_zero2.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<store_dest.inp2.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<store_dest.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<dest_setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_concat.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_concat.inp6.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_sum_spread.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_sum_spread.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_add.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_clear_ca.outputs[0]: <CA>>: "
            "Loc(kind=LocKind.CA, start=0, reg_len=1), "
            "<mul_2_add.outputs[1]: <CA>>: "
            "Loc(kind=LocKind.CA, start=0, reg_len=1), "
            "<mul_2_add.inp3.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_add.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_add.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_pp_concat.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_pp_concat.inp4.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_retval_concat.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_retval_concat.inp4.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_mul_rt_spread.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_mul_rt_spread.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_mul.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_mul.inp3.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_2_mul.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_sum_spread.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_sum_spread.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_add.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_clear_ca.outputs[0]: <CA>>: "
            "Loc(kind=LocKind.CA, start=0, reg_len=1), "
            "<mul_1_add.outputs[1]: <CA>>: "
            "Loc(kind=LocKind.CA, start=0, reg_len=1), "
            "<mul_1_add.inp3.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_add.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_add.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_pp_concat.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_pp_concat.inp5.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_retval_concat.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_retval_concat.inp5.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_mul_rt_spread.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_mul_rt_spread.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_mul.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_mul.inp3.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_1_mul.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_0_mul_rt_spread.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_0_mul_rt_spread.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_0_mul.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_0_mul.inp3.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_0_mul.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_lhs_setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_rhs_spread.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_rhs_spread.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<mul_rhs_setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<load_rhs.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<load_rhs.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<rhs_setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<load_lhs.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<load_lhs.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<lhs_setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1)"
            "}")

    def test_simple_mul_192x192_asm(self):
        code = Mul(mul=simple_umul, lhs_size_in_words=3, rhs_size_in_words=3)
        fn = code.fn
        assigned_registers = allocate_registers(
            fn, debug_out=sys.stdout, dump_graph=GraphDumper(self))
        gen_asm_state = GenAsmState(assigned_registers)
        fn.gen_asm(gen_asm_state)
        print(gen_asm_state.output)
        self.assertEqual(gen_asm_state.output, [
            'or 10, 3, 3',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'sv.ld *24, 48(10)',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'sv.ld *20, 72(10)',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'addi 6, 0, 0',
            'or 9, 6, 6',
            'setvl 0, 0, 3, 0, 1, 1',
            'addi 3, 0, 0',
            'setvl 0, 0, 3, 0, 1, 1',
            'or 6, 9, 9',
            'setvl 0, 0, 3, 0, 1, 1',
            'sv.maddedu *14, *24, 20, 6',
            'setvl 0, 0, 3, 0, 1, 1',
            'or 17, 6, 6',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'or 6, 9, 9',
            'setvl 0, 0, 3, 0, 1, 1',
            'sv.maddedu *3, *24, 21, 6',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'addi 8, 0, 0',
            'or 18, 8, 8',
            'addi 7, 0, 0',
            'setvl 0, 0, 5, 0, 1, 1',
            'or 19, 18, 18',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'addic 0, 0, 0',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'sv.adde *15, *15, *3',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'or 6, 9, 9',
            'setvl 0, 0, 3, 0, 1, 1',
            'sv.maddedu *3, *24, 22, 6',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'addic 0, 0, 0',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.adde *16, *16, *3',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'sv.std *14, 0(10)'
        ])

    def toom_2_mul_256x256(self, lhs_signed, rhs_signed):
        # type: (bool, bool) -> Mul
        TOOM_2 = ToomCookInstance.make_toom_2()
        instances = TOOM_2,

        def mul(fn, lhs, rhs):
            # type: (Fn, SSAVal, SSAVal) -> tuple[SSAVal, ToomCookMul]
            v = ToomCookMul(fn=fn, lhs=lhs, lhs_signed=lhs_signed, rhs=rhs,
                            rhs_signed=rhs_signed, instances=instances)
            return v.retval, v
        return Mul(mul=mul, lhs_size_in_words=4, rhs_size_in_words=4)

    def make_256x256_mul_test_cases(self, lhs_signed, rhs_signed):
        # type: (bool, bool) -> Iterator[tuple[int, int, int]]
        # test multiplying `+-1 << n` and:
        # 0xc162321a5eaad80b_4b86bb0efdfb93c0_a789ff04cc11b157_eaa08e29fb197621
        # *
        # 0x3138710167583371_998af336a8fac64d_e6da3737090787fe_85ba09ea701f4af2
        # ==
        # int("0x"
        #     "252e6e6f69746163_696c7069746c754d_"
        #     "2061627573746172_614b202d20322d4d_"
        #     "4f4f5420676e6973_75206c756d20746e_"
        #     "6967696220746962_2d36353278363532", base=0)
        # == int.from_bytes(b'256x256-bit bigint mul using TOOM-2 '
        #                   b'- Karatsuba Multiplication.%', 'little')
        lhs_value_in = (0xc162321a5eaad80b_4b86bb0efdfb93c0 << 128) \
            | 0xa789ff04cc11b157_eaa08e29fb197621
        rhs_value_in = (0x3138710167583371_998af336a8fac64d << 128) \
            | 0xe6da3737090787fe_85ba09ea701f4af2
        prod_value_in = int.from_bytes(
            b'256x256-bit bigint mul using TOOM-2 '
            b'- Karatsuba Multiplication.%', 'little')
        self.assertEqual(lhs_value_in * rhs_value_in, prod_value_in)
        shifts = [*range(0, 256, 16), *range(15, 256, 16)]
        lhs_values = [1 << i for i in shifts] + [0, lhs_value_in]
        rhs_values = [1 << i for i in shifts] + [0, rhs_value_in]
        if lhs_signed:
            lhs_values.extend([-i for i in lhs_values])
        if rhs_signed:
            rhs_values.extend([-i for i in rhs_values])

        def key(v):
            # type: (int) -> tuple[bool, int]
            return abs(v) in (lhs_value_in, rhs_value_in), v % (1 << 256)

        lhs_values.sort(key=key)
        rhs_values.sort(key=key)
        for lhs_value in lhs_values:
            for rhs_value in rhs_values:
                lhs_value %= 1 << 256
                rhs_value %= 1 << 256
                if lhs_value >> 255 != 0 and lhs_signed:
                    lhs_value -= 1 << 256
                if rhs_value >> 255 != 0 and rhs_signed:
                    rhs_value -= 1 << 256
                prod_value = lhs_value * rhs_value
                lhs_value %= 1 << 256
                rhs_value %= 1 << 256
                prod_value %= 1 << 512
                yield lhs_value, rhs_value, prod_value

    def tst_toom_2_mul_256x256_sim(
        self, lhs_signed,  # type: bool
        rhs_signed,  # type: bool
        get_state_factory,  # type: Callable[[Mul], _StateFactory]
    ):
        code = self.toom_2_mul_256x256(
            lhs_signed=lhs_signed, rhs_signed=rhs_signed)
        print(code.retval[1])
        print(code.fn.ops_to_str())
        state_factory = get_state_factory(code)
        ptr_in = 0x100
        dest_ptr = ptr_in + code.dest_offset
        lhs_ptr = ptr_in + code.lhs_offset
        rhs_ptr = ptr_in + code.rhs_offset
        values = self.make_256x256_mul_test_cases(
            lhs_signed=lhs_signed, rhs_signed=rhs_signed)
        for lhs_value, rhs_value, prod_value in values:
            with self.subTest(lhs_signed=lhs_signed, rhs_signed=rhs_signed,
                              lhs_value=hex(lhs_value),
                              rhs_value=hex(rhs_value),
                              prod_value=hex(prod_value)):
                with state_factory() as state:
                    state[code.ptr_in] = ptr_in,
                    for i in range(4):
                        v = lhs_value >> GPR_SIZE_IN_BITS * i
                        v &= GPR_VALUE_MASK
                        state.store(lhs_ptr + i * GPR_SIZE_IN_BYTES, v)
                    for i in range(4):
                        v = rhs_value >> GPR_SIZE_IN_BITS * i
                        v &= GPR_VALUE_MASK
                        state.store(rhs_ptr + i * GPR_SIZE_IN_BYTES, v)
                    code.fn.sim(state)
                    prod = 0
                    for i in range(8):
                        v = state.load(dest_ptr + GPR_SIZE_IN_BYTES * i)
                        prod += v << (GPR_SIZE_IN_BITS * i)
                    self.assertEqual(hex(prod), hex(prod_value),
                                     f"failed: state={state}")

    def test_toom_2_mul_256x256_pre_ra_sim(self):
        for lhs_signed in False, True:
            for rhs_signed in False, True:
                self.tst_toom_2_mul_256x256_sim(
                    lhs_signed=lhs_signed, rhs_signed=rhs_signed,
                    get_state_factory=get_pre_ra_state_factory)

    def test_toom_2_mul_256x256_uu_post_ra_sim(self):
        self.tst_toom_2_mul_256x256_sim(
            lhs_signed=False, rhs_signed=False,
            get_state_factory=self.get_post_ra_state_factory)

    def test_toom_2_mul_256x256_su_post_ra_sim(self):
        self.tst_toom_2_mul_256x256_sim(
            lhs_signed=True, rhs_signed=False,
            get_state_factory=self.get_post_ra_state_factory)

    def test_toom_2_mul_256x256_us_post_ra_sim(self):
        self.tst_toom_2_mul_256x256_sim(
            lhs_signed=False, rhs_signed=True,
            get_state_factory=self.get_post_ra_state_factory)

    def test_toom_2_mul_256x256_ss_post_ra_sim(self):
        self.tst_toom_2_mul_256x256_sim(
            lhs_signed=True, rhs_signed=True,
            get_state_factory=self.get_post_ra_state_factory)

    def test_toom_2_mul_256x256_asm(self):
        code = self.toom_2_mul_256x256(lhs_signed=False, rhs_signed=False)
        fn = code.fn
        assigned_registers = allocate_registers(
            fn, debug_out=sys.stdout, dump_graph=GraphDumper(self))
        gen_asm_state = GenAsmState(assigned_registers)
        fn.gen_asm(gen_asm_state)
        print(gen_asm_state.output)
        self.assertEqual(gen_asm_state.output, [
            'or 49, 3, 3',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 3, 49, 49',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.ld *39, 64(3)',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 3, 49, 49',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.ld *14, 96(3)',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.or *3, *14, *14',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.or *14, *39, *39',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 32, 16, 16',
            'setvl 0, 0, 3, 0, 1, 1',
            'or 16, 32, 32',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'sv.or *30, *14, *14',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'or 48, 17, 17',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'sv.or *14, *30, *30',
            'setvl 0, 0, 3, 0, 1, 1',
            'or 41, 16, 16',
            'addi 7, 0, 0',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 16, 41, 41',
            'or 17, 7, 7',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.or *30, *14, *14',
            'setvl 0, 0, 1, 0, 1, 1',
            'or 17, 48, 48',
            'setvl 0, 0, 1, 0, 1, 1',
            'sv.or *7, *17, *17',
            'addi 8, 0, 0',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 9, 8, 8',
            'or 10, 8, 8',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.or *17, *7, *7',
            'setvl 0, 0, 4, 0, 1, 1',
            'addic 0, 0, 0',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.or *39, *30, *30',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.adde *34, *39, *17',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.or *44, *34, *34',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.or *14, *3, *3',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 3, 14, 14',
            'or 10, 17, 17',
            'setvl 0, 0, 3, 0, 1, 1',
            'or 14, 3, 3',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'sv.or *17, *10, *10',
            'or 43, 17, 17',
            'setvl 0, 0, 3, 0, 1, 1',
            'setvl 0, 0, 3, 0, 1, 1',
            'sv.or *7, *14, *14',
            'setvl 0, 0, 3, 0, 1, 1',
            'or 3, 7, 7',
            'or 4, 8, 8',
            'or 5, 9, 9',
            'addi 7, 0, 0',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 6, 7, 7',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 1, 0, 1, 1',
            'or 17, 43, 43',
            'setvl 0, 0, 1, 0, 1, 1',
            'addi 7, 0, 0',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 18, 7, 7',
            'or 19, 7, 7',
            'or 20, 7, 7',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'addic 0, 0, 0',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.adde *7, *3, *17',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.or *38, *7, *7',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'addi 7, 0, 0',
            'or 35, 7, 7',
            'setvl 0, 0, 4, 0, 1, 1',
            'addi 7, 0, 0',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.or *14, *30, *30',
            'or 7, 3, 3',
            'or 21, 35, 35',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.maddedu *22, *14, 7, 21',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 26, 21, 21',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.or *14, *30, *30',
            'or 34, 4, 4',
            'or 21, 35, 35',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.maddedu *7, *14, 34, 21',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 11, 21, 21',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'addi 14, 0, 0',
            'or 27, 14, 14',
            'addi 14, 0, 0',
            'or 12, 14, 14',
            'setvl 0, 0, 6, 0, 1, 1',
            'or 28, 27, 27',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'addic 0, 0, 0',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'sv.adde *23, *23, *7',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.or *14, *30, *30',
            'or 34, 5, 5',
            'or 21, 35, 35',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.maddedu *7, *14, 34, 21',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 11, 21, 21',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'addi 14, 0, 0',
            'or 29, 14, 14',
            'addi 14, 0, 0',
            'or 12, 14, 14',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'addic 0, 0, 0',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'sv.adde *24, *24, *7',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.or *14, *30, *30',
            'or 8, 6, 6',
            'or 21, 35, 35',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.maddedu *3, *14, 8, 21',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 7, 21, 21',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'addic 0, 0, 0',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'sv.adde *25, *25, *3',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 8, 0, 1, 1',
            'setvl 0, 0, 8, 0, 1, 1',
            'setvl 0, 0, 8, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.or *7, *38, *38',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 38, 7, 7',
            'or 39, 8, 8',
            'or 40, 9, 9',
            'or 41, 10, 10',
            'addi 3, 0, 0',
            'or 10, 3, 3',
            'setvl 0, 0, 4, 0, 1, 1',
            'addi 3, 0, 0',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.or *34, *44, *44',
            'or 9, 38, 38',
            'or 7, 10, 10',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.maddedu *14, *34, 9, 7',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 18, 7, 7',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 30, 14, 14',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 9, 39, 39',
            'or 7, 10, 10',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.maddedu *3, *44, 9, 7',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'addi 9, 0, 0',
            'or 19, 9, 9',
            'addi 9, 0, 0',
            'or 8, 9, 9',
            'setvl 0, 0, 6, 0, 1, 1',
            'or 20, 19, 19',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'addic 0, 0, 0',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'sv.adde *15, *15, *3',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'or 31, 15, 15',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 9, 40, 40',
            'or 7, 10, 10',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.maddedu *3, *44, 9, 7',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'addi 9, 0, 0',
            'or 21, 9, 9',
            'addi 8, 0, 0',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'addic 0, 0, 0',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'sv.adde *16, *16, *3',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'setvl 0, 0, 6, 0, 1, 1',
            'or 32, 16, 16',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 9, 41, 41',
            'or 7, 10, 10',
            'setvl 0, 0, 4, 0, 1, 1',
            'sv.maddedu *38, *44, 9, 7',
            'setvl 0, 0, 4, 0, 1, 1',
            'or 42, 7, 7',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 4, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'addic 0, 0, 0',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'sv.adde *17, *17, *38',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'or 35, 19, 19',
            'or 36, 20, 20',
            'or 37, 21, 21',
            'setvl 0, 0, 8, 0, 1, 1',
            'or 14, 30, 30',
            'or 15, 31, 31',
            'or 16, 32, 32',
            'or 19, 35, 35',
            'or 20, 36, 36',
            'or 21, 37, 37',
            'setvl 0, 0, 8, 0, 1, 1',
            'setvl 0, 0, 8, 0, 1, 1',
            'sv.or *30, *14, *14',
            'setvl 0, 0, 1, 0, 1, 1',
            'or 10, 43, 43',
            'setvl 0, 0, 1, 0, 1, 1',
            'or 43, 10, 10',
            'addi 3, 0, 0',
            'setvl 0, 0, 1, 0, 1, 1',
            'addi 4, 0, 0',
            'or 42, 48, 48',
            'or 10, 43, 43',
            'or 4, 3, 3',
            'setvl 0, 0, 1, 0, 1, 1',
            'sv.maddedu *3, *42, 10, 4',
            'setvl 0, 0, 1, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 8, 0, 1, 1',
            'setvl 0, 0, 8, 0, 1, 1',
            'setvl 0, 0, 8, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 8, 0, 1, 1',
            'setvl 0, 0, 8, 0, 1, 1',
            'sv.or *14, *30, *30',
            'setvl 0, 0, 8, 0, 1, 1',
            'or 30, 14, 14',
            'or 31, 15, 15',
            'or 32, 16, 16',
            'or 35, 19, 19',
            'or 36, 20, 20',
            'or 37, 21, 21',
            'setvl 0, 0, 7, 0, 1, 1',
            'or 14, 30, 30',
            'or 15, 31, 31',
            'or 16, 32, 32',
            'or 19, 35, 35',
            'or 20, 36, 36',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'sv.or *30, *14, *14',
            'setvl 0, 0, 7, 0, 1, 1',
            'subfc 0, 0, 0',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'sv.or *14, *30, *30',
            'setvl 0, 0, 7, 0, 1, 1',
            'sv.subfe *30, *22, *14',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'addi 10, 0, 0',
            'setvl 0, 0, 7, 0, 1, 1',
            'or 5, 10, 10',
            'or 6, 10, 10',
            'or 7, 10, 10',
            'or 8, 10, 10',
            'or 9, 10, 10',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'subfc 0, 0, 0',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'sv.subfe *14, *3, *30',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 7, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'addi 10, 0, 0',
            'addi 10, 0, 0',
            'or 29, 10, 10',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'addic 0, 0, 0',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'sv.adde *25, *25, *14',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 5, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'addic 0, 0, 0',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'sv.adde *28, *28, *3',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 2, 0, 1, 1',
            'setvl 0, 0, 8, 0, 1, 1',
            'setvl 0, 0, 8, 0, 1, 1',
            'setvl 0, 0, 8, 0, 1, 1',
            'setvl 0, 0, 8, 0, 1, 1',
            'setvl 0, 0, 8, 0, 1, 1',
            'or 3, 49, 49',
            'setvl 0, 0, 8, 0, 1, 1',
            'sv.std *22, 0(3)',
        ])

    def tst_toom_mul_sim(
        self, code,  # type: Mul
        lhs_signed,  # type: bool
        rhs_signed,  # type: bool
        get_state_factory,  # type: Callable[[Mul], _StateFactory]
        test_cases,  # type: Iterable[tuple[int, int]]
    ):
        print(code.retval[1])
        print(code.fn.ops_to_str())
        state_factory = get_state_factory(code)
        ptr_in = 0x100
        dest_ptr = ptr_in + code.dest_offset
        lhs_ptr = ptr_in + code.lhs_offset
        rhs_ptr = ptr_in + code.rhs_offset
        lhs_size_in_bits = code.lhs_size_in_words * GPR_SIZE_IN_BITS
        rhs_size_in_bits = code.rhs_size_in_words * GPR_SIZE_IN_BITS
        for lhs_value, rhs_value in test_cases:
            lhs_value %= 1 << lhs_size_in_bits
            rhs_value %= 1 << rhs_size_in_bits
            if lhs_signed and lhs_value >> (lhs_size_in_bits - 1):
                lhs_value -= 1 << lhs_size_in_bits
            if rhs_signed and rhs_value >> (rhs_size_in_bits - 1):
                rhs_value -= 1 << rhs_size_in_bits
            prod_value = lhs_value * rhs_value
            lhs_value %= 1 << lhs_size_in_bits
            rhs_value %= 1 << rhs_size_in_bits
            prod_value %= 1 << (lhs_size_in_bits + rhs_size_in_bits)
            with self.subTest(lhs_signed=lhs_signed, rhs_signed=rhs_signed,
                              lhs_value=hex(lhs_value),
                              rhs_value=hex(rhs_value),
                              prod_value=hex(prod_value)):
                with state_factory() as state:
                    state[code.ptr_in] = ptr_in,
                    for i in range(code.lhs_size_in_words):
                        v = lhs_value >> GPR_SIZE_IN_BITS * i
                        v &= GPR_VALUE_MASK
                        state.store(lhs_ptr + i * GPR_SIZE_IN_BYTES, v)
                    for i in range(code.rhs_size_in_words):
                        v = rhs_value >> GPR_SIZE_IN_BITS * i
                        v &= GPR_VALUE_MASK
                        state.store(rhs_ptr + i * GPR_SIZE_IN_BYTES, v)
                    code.fn.sim(state)
                    prod = 0
                    for i in range(code.dest_size_in_words):
                        v = state.load(dest_ptr + GPR_SIZE_IN_BYTES * i)
                        prod += v << (GPR_SIZE_IN_BITS * i)
                    self.assertEqual(hex(prod), hex(prod_value),
                                     f"failed: state={state}")

    def tst_toom_mul_all_sizes_pre_ra_sim(self, instances, lhs_signed, rhs_signed):
        # type: (tuple[ToomCookInstance, ...], bool, bool) -> None
        def mul(fn, lhs, rhs):
            # type: (Fn, SSAVal, SSAVal) -> tuple[SSAVal, ToomCookMul]
            v = ToomCookMul(
                fn=fn, lhs=lhs, lhs_signed=lhs_signed, rhs=rhs,
                rhs_signed=rhs_signed, instances=instances)
            return v.retval, v
        sizes_in_words = OSet()  # type: OSet[int]
        for i in range(6):
            sizes_in_words.add(1 << i)
            sizes_in_words.add(3 << i)
        sizes_in_words = OSet(
            i for i in sorted(sizes_in_words) if 1 <= i <= 16)
        for lhs_size_in_words in sizes_in_words:
            for rhs_size_in_words in sizes_in_words:
                lhs_size_in_bits = GPR_SIZE_IN_BITS * lhs_size_in_words
                rhs_size_in_bits = GPR_SIZE_IN_BITS * rhs_size_in_words
                with self.subTest(lhs_size_in_words=lhs_size_in_words,
                                  rhs_size_in_words=rhs_size_in_words,
                                  lhs_signed=lhs_signed,
                                  rhs_signed=rhs_signed):
                    test_cases = []  # type: list[tuple[int, int]]
                    test_cases.append((-1, -1))
                    test_cases.append(((0x80 << 2048) // 0xFF,
                                       (0x80 << 2048) // 0xFF))
                    test_cases.append(((0x40 << 2048) // 0xFF,
                                       (0x80 << 2048) // 0xFF))
                    test_cases.append(((0x80 << 2048) // 0xFF,
                                       (0x40 << 2048) // 0xFF))
                    test_cases.append(((0x40 << 2048) // 0xFF,
                                       (0x40 << 2048) // 0xFF))
                    test_cases.append((1 << (lhs_size_in_bits - 1),
                                       1 << (rhs_size_in_bits - 1)))
                    test_cases.append((1, 1 << (rhs_size_in_bits - 1)))
                    test_cases.append((1 << (lhs_size_in_bits - 1), 1))
                    test_cases.append((1, 1))
                    self.tst_toom_mul_sim(
                        code=Mul(mul=mul,
                                 lhs_size_in_words=lhs_size_in_words,
                                 rhs_size_in_words=rhs_size_in_words),
                        lhs_signed=lhs_signed, rhs_signed=rhs_signed,
                        get_state_factory=get_pre_ra_state_factory,
                        test_cases=test_cases)

    def test_toom_2_once_mul_uu_all_sizes_pre_ra_sim(self):
        TOOM_2 = ToomCookInstance.make_toom_2()
        self.tst_toom_mul_all_sizes_pre_ra_sim(
            (TOOM_2,), lhs_signed=False, rhs_signed=False)

    def test_toom_2_once_mul_us_all_sizes_pre_ra_sim(self):
        TOOM_2 = ToomCookInstance.make_toom_2()
        self.tst_toom_mul_all_sizes_pre_ra_sim(
            (TOOM_2,), lhs_signed=False, rhs_signed=True)

    def test_toom_2_once_mul_su_all_sizes_pre_ra_sim(self):
        TOOM_2 = ToomCookInstance.make_toom_2()
        self.tst_toom_mul_all_sizes_pre_ra_sim(
            (TOOM_2,), lhs_signed=True, rhs_signed=False)

    def test_toom_2_once_mul_ss_all_sizes_pre_ra_sim(self):
        TOOM_2 = ToomCookInstance.make_toom_2()
        self.tst_toom_mul_all_sizes_pre_ra_sim(
            (TOOM_2,), lhs_signed=True, rhs_signed=True)

    def test_toom_2_mul_uu_all_sizes_pre_ra_sim(self):
        TOOM_2 = ToomCookInstance.make_toom_2()
        instances = TOOM_2, TOOM_2, TOOM_2, TOOM_2
        self.tst_toom_mul_all_sizes_pre_ra_sim(
            instances, lhs_signed=False, rhs_signed=False)

    def test_toom_2_mul_us_all_sizes_pre_ra_sim(self):
        TOOM_2 = ToomCookInstance.make_toom_2()
        instances = TOOM_2, TOOM_2, TOOM_2, TOOM_2
        self.tst_toom_mul_all_sizes_pre_ra_sim(
            instances, lhs_signed=False, rhs_signed=True)

    def test_toom_2_mul_su_all_sizes_pre_ra_sim(self):
        TOOM_2 = ToomCookInstance.make_toom_2()
        instances = TOOM_2, TOOM_2, TOOM_2, TOOM_2
        self.tst_toom_mul_all_sizes_pre_ra_sim(
            instances, lhs_signed=True, rhs_signed=False)

    def test_toom_2_mul_ss_all_sizes_pre_ra_sim(self):
        TOOM_2 = ToomCookInstance.make_toom_2()
        instances = TOOM_2, TOOM_2, TOOM_2, TOOM_2
        self.tst_toom_mul_all_sizes_pre_ra_sim(
            instances, lhs_signed=True, rhs_signed=True)


if __name__ == "__main__":
    unittest.main()
