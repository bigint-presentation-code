import unittest

from bigint_presentation_code.compiler_ir import (GPR_SIZE_IN_BYTES, BaseTy,
                                                  Fn, FnAnalysis, GenAsmState,
                                                  Loc, LocKind, LocSet, OpKind,
                                                  OpStage, PreRASimState,
                                                  ProgramPoint, SSAVal, Ty)
from bigint_presentation_code.util import OFSet


class TestCompilerIR(unittest.TestCase):
    maxDiff = None

    def test_program_point(self):
        # type: () -> None
        expected = []  # type: list[ProgramPoint]
        for op_index in range(5):
            for stage in OpStage:
                expected.append(ProgramPoint(op_index=op_index, stage=stage))

        for idx, pp in enumerate(expected):
            if idx + 1 < len(expected):
                self.assertEqual(pp.next(), expected[idx + 1])

        self.assertEqual(sorted(expected), expected)

    def test_loc_set_hash_intern(self):
        # type: () -> None
        # hashes should match all other collections.abc.Set types, which are
        # supposed to match frozenset but don't until Python 3.11 because of a
        # bug fixed in:
        # https://github.com/python/cpython/commit/c878f5d81772dc6f718d6608c78baa4be9a4f176
        a = LocSet([])
        self.assertEqual(hash(a), hash(OFSet()))
        starts = 0, 1, 0, 1, 2
        GPR = LocKind.GPR
        expected = OFSet(Loc(kind=GPR, start=i, reg_len=1) for i in starts)
        b = LocSet(expected)
        c = LocSet(Loc(kind=GPR, start=i, reg_len=1) for i in starts)
        d = LocSet(Loc(kind=GPR, start=i, reg_len=1) for i in starts)
        # hashes should be equal to OFSet's hash
        self.assertEqual(hash(b), hash(expected))
        self.assertEqual(hash(c), hash(expected))
        self.assertEqual(hash(d), hash(expected))
        # they should intern to the same object
        self.assertIs(b, d)
        self.assertIs(c, d)

    def make_add_fn(self):
        # type: () -> tuple[Fn, SSAVal]
        fn = Fn()
        op0 = fn.append_new_op(OpKind.FuncArgR3, name="arg")
        arg = op0.outputs[0]
        MAXVL = 32
        op1 = fn.append_new_op(OpKind.SetVLI, immediates=[MAXVL], name="vl")
        vl = op1.outputs[0]
        op2 = fn.append_new_op(
            OpKind.SvLd, input_vals=[arg, vl], immediates=[0], maxvl=MAXVL,
            name="ld")
        a = op2.outputs[0]
        op3 = fn.append_new_op(OpKind.SvLI, input_vals=[vl], immediates=[0],
                               maxvl=MAXVL, name="li")
        b = op3.outputs[0]
        op4 = fn.append_new_op(OpKind.SetCA, name="ca")
        ca = op4.outputs[0]
        op5 = fn.append_new_op(
            OpKind.SvAddE, input_vals=[a, b, ca, vl], maxvl=MAXVL, name="add")
        s = op5.outputs[0]
        _ = fn.append_new_op(OpKind.SvStd, input_vals=[s, arg, vl],
                             immediates=[0], maxvl=MAXVL, name="st")
        return fn, arg

    def test_fn_analysis(self):
        fn, _arg = self.make_add_fn()
        fn_analysis = FnAnalysis(fn)
        self.assertEqual(
            repr(fn_analysis.uses),
            "FMap({"
            "<arg.outputs[0]: <I64>>: OFSet(["
            "<ld.input_uses[0]: <I64>>, <st.input_uses[1]: <I64>>]), "
            "<vl.outputs[0]: <VL_MAXVL>>: OFSet(["
            "<ld.input_uses[1]: <VL_MAXVL>>, <li.input_uses[0]: <VL_MAXVL>>, "
            "<add.input_uses[3]: <VL_MAXVL>>, "
            "<st.input_uses[2]: <VL_MAXVL>>]), "
            "<ld.outputs[0]: <I64*32>>: OFSet(["
            "<add.input_uses[0]: <I64*32>>]), "
            "<li.outputs[0]: <I64*32>>: OFSet(["
            "<add.input_uses[1]: <I64*32>>]), "
            "<ca.outputs[0]: <CA>>: OFSet([<add.input_uses[2]: <CA>>]), "
            "<add.outputs[0]: <I64*32>>: OFSet(["
            "<st.input_uses[0]: <I64*32>>]), "
            "<add.outputs[1]: <CA>>: OFSet()})"
        )
        self.assertEqual(
            repr(fn_analysis.op_indexes),
            "FMap({"
            "arg:\n"
            "    (<...outputs[0]: <I64>>) <= FuncArgR3: 0, "
            "vl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x20): 1, "
            "ld:\n"
            "    (<...outputs[0]: <I64*32>>) <= SvLd(\n"
            "    <arg.outputs[0]: <I64>>, <vl.outputs[0]: <VL_MAXVL>>, 0x0)"
            ": 2, "
            "li:\n"
            "    (<...outputs[0]: <I64*32>>) <= SvLI(\n"
            "    <vl.outputs[0]: <VL_MAXVL>>, 0x0): 3, "
            "ca:\n"
            "    (<...outputs[0]: <CA>>) <= SetCA: 4, "
            "add:\n"
            "    (<...outputs[0]: <I64*32>>, <...outputs[1]: <CA>>\n"
            "    ) <= SvAddE(<ld.outputs[0]: <I64*32>>,\n"
            "    <li.outputs[0]: <I64*32>>, <ca.outputs[0]: <CA>>,\n"
            "    <vl.outputs[0]: <VL_MAXVL>>): 5, "
            "st:\n"
            "    SvStd(<add.outputs[0]: <I64*32>>, <arg.outputs[0]: <I64>>,\n"
            "    <vl.outputs[0]: <VL_MAXVL>>, 0x0): 6"
            "})"
        )
        self.assertEqual(
            repr(fn_analysis.live_ranges),
            "FMap({"
            "<arg.outputs[0]: <I64>>: <range:ops[0]:Early..ops[6]:Late>, "
            "<vl.outputs[0]: <VL_MAXVL>>: <range:ops[1]:Late..ops[6]:Late>, "
            "<ld.outputs[0]: <I64*32>>: <range:ops[2]:Early..ops[5]:Late>, "
            "<li.outputs[0]: <I64*32>>: <range:ops[3]:Early..ops[5]:Late>, "
            "<ca.outputs[0]: <CA>>: <range:ops[4]:Late..ops[5]:Late>, "
            "<add.outputs[0]: <I64*32>>: <range:ops[5]:Early..ops[6]:Late>, "
            "<add.outputs[1]: <CA>>: <range:ops[5]:Early..ops[6]:Early>})"
        )
        self.assertEqual(
            repr(fn_analysis.live_at),
            "FMap({"
            "<ops[0]:Early>: OFSet([<arg.outputs[0]: <I64>>]), "
            "<ops[0]:Late>: OFSet([<arg.outputs[0]: <I64>>]), "
            "<ops[1]:Early>: OFSet([<arg.outputs[0]: <I64>>]), "
            "<ops[1]:Late>: OFSet(["
            "<arg.outputs[0]: <I64>>, <vl.outputs[0]: <VL_MAXVL>>]), "
            "<ops[2]:Early>: OFSet(["
            "<arg.outputs[0]: <I64>>, <vl.outputs[0]: <VL_MAXVL>>, "
            "<ld.outputs[0]: <I64*32>>]), "
            "<ops[2]:Late>: OFSet(["
            "<arg.outputs[0]: <I64>>, <vl.outputs[0]: <VL_MAXVL>>, "
            "<ld.outputs[0]: <I64*32>>]), "
            "<ops[3]:Early>: OFSet(["
            "<arg.outputs[0]: <I64>>, <vl.outputs[0]: <VL_MAXVL>>, "
            "<ld.outputs[0]: <I64*32>>, <li.outputs[0]: <I64*32>>]), "
            "<ops[3]:Late>: OFSet(["
            "<arg.outputs[0]: <I64>>, <vl.outputs[0]: <VL_MAXVL>>, "
            "<ld.outputs[0]: <I64*32>>, <li.outputs[0]: <I64*32>>]), "
            "<ops[4]:Early>: OFSet(["
            "<arg.outputs[0]: <I64>>, <vl.outputs[0]: <VL_MAXVL>>, "
            "<ld.outputs[0]: <I64*32>>, <li.outputs[0]: <I64*32>>]), "
            "<ops[4]:Late>: OFSet(["
            "<arg.outputs[0]: <I64>>, <vl.outputs[0]: <VL_MAXVL>>, "
            "<ld.outputs[0]: <I64*32>>, <li.outputs[0]: <I64*32>>, "
            "<ca.outputs[0]: <CA>>]), "
            "<ops[5]:Early>: OFSet(["
            "<arg.outputs[0]: <I64>>, <vl.outputs[0]: <VL_MAXVL>>, "
            "<ld.outputs[0]: <I64*32>>, <li.outputs[0]: <I64*32>>, "
            "<ca.outputs[0]: <CA>>, <add.outputs[0]: <I64*32>>, "
            "<add.outputs[1]: <CA>>]), "
            "<ops[5]:Late>: OFSet(["
            "<arg.outputs[0]: <I64>>, <vl.outputs[0]: <VL_MAXVL>>, "
            "<add.outputs[0]: <I64*32>>, <add.outputs[1]: <CA>>]), "
            "<ops[6]:Early>: OFSet(["
            "<arg.outputs[0]: <I64>>, <vl.outputs[0]: <VL_MAXVL>>, "
            "<add.outputs[0]: <I64*32>>]), "
            "<ops[6]:Late>: OFSet()})"
        )
        self.assertEqual(
            repr(fn_analysis.def_program_ranges),
            "FMap({"
            "<arg.outputs[0]: <I64>>: <range:ops[0]:Early..ops[1]:Early>, "
            "<vl.outputs[0]: <VL_MAXVL>>: <range:ops[1]:Late..ops[2]:Early>, "
            "<ld.outputs[0]: <I64*32>>: <range:ops[2]:Early..ops[3]:Early>, "
            "<li.outputs[0]: <I64*32>>: <range:ops[3]:Early..ops[4]:Early>, "
            "<ca.outputs[0]: <CA>>: <range:ops[4]:Late..ops[5]:Early>, "
            "<add.outputs[0]: <I64*32>>: <range:ops[5]:Early..ops[6]:Early>, "
            "<add.outputs[1]: <CA>>: <range:ops[5]:Early..ops[6]:Early>})"
        )
        self.assertEqual(
            repr(fn_analysis.use_program_points),
            "FMap({"
            "<ld.input_uses[0]: <I64>>: <ops[2]:Early>, "
            "<ld.input_uses[1]: <VL_MAXVL>>: <ops[2]:Early>, "
            "<li.input_uses[0]: <VL_MAXVL>>: <ops[3]:Early>, "
            "<add.input_uses[0]: <I64*32>>: <ops[5]:Early>, "
            "<add.input_uses[1]: <I64*32>>: <ops[5]:Early>, "
            "<add.input_uses[2]: <CA>>: <ops[5]:Early>, "
            "<add.input_uses[3]: <VL_MAXVL>>: <ops[5]:Early>, "
            "<st.input_uses[0]: <I64*32>>: <ops[6]:Early>, "
            "<st.input_uses[1]: <I64>>: <ops[6]:Early>, "
            "<st.input_uses[2]: <VL_MAXVL>>: <ops[6]:Early>})"
        )
        self.assertEqual(
            repr(fn_analysis.all_program_points),
            "<range:ops[0]:Early..ops[7]:Early>")
        self.assertEqual(repr(fn_analysis.copies), "FMap({})")
        self.assertEqual(
            repr(fn_analysis.const_ssa_vals),
            "FMap({"
            "<vl.outputs[0]: <VL_MAXVL>>: (32,), "
            "<li.outputs[0]: <I64*32>>: ("
            "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "
            "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), "
            "<ca.outputs[0]: <CA>>: (1,)})"
        )
        self.assertEqual(
            repr(fn_analysis.const_ssa_val_sub_regs),
            "FMap({"
            "<vl.outputs[0]: <VL_MAXVL>>[0]: 32, "
            "<li.outputs[0]: <I64*32>>[0]: 0, "
            "<li.outputs[0]: <I64*32>>[1]: 0, "
            "<li.outputs[0]: <I64*32>>[2]: 0, "
            "<li.outputs[0]: <I64*32>>[3]: 0, "
            "<li.outputs[0]: <I64*32>>[4]: 0, "
            "<li.outputs[0]: <I64*32>>[5]: 0, "
            "<li.outputs[0]: <I64*32>>[6]: 0, "
            "<li.outputs[0]: <I64*32>>[7]: 0, "
            "<li.outputs[0]: <I64*32>>[8]: 0, "
            "<li.outputs[0]: <I64*32>>[9]: 0, "
            "<li.outputs[0]: <I64*32>>[10]: 0, "
            "<li.outputs[0]: <I64*32>>[11]: 0, "
            "<li.outputs[0]: <I64*32>>[12]: 0, "
            "<li.outputs[0]: <I64*32>>[13]: 0, "
            "<li.outputs[0]: <I64*32>>[14]: 0, "
            "<li.outputs[0]: <I64*32>>[15]: 0, "
            "<li.outputs[0]: <I64*32>>[16]: 0, "
            "<li.outputs[0]: <I64*32>>[17]: 0, "
            "<li.outputs[0]: <I64*32>>[18]: 0, "
            "<li.outputs[0]: <I64*32>>[19]: 0, "
            "<li.outputs[0]: <I64*32>>[20]: 0, "
            "<li.outputs[0]: <I64*32>>[21]: 0, "
            "<li.outputs[0]: <I64*32>>[22]: 0, "
            "<li.outputs[0]: <I64*32>>[23]: 0, "
            "<li.outputs[0]: <I64*32>>[24]: 0, "
            "<li.outputs[0]: <I64*32>>[25]: 0, "
            "<li.outputs[0]: <I64*32>>[26]: 0, "
            "<li.outputs[0]: <I64*32>>[27]: 0, "
            "<li.outputs[0]: <I64*32>>[28]: 0, "
            "<li.outputs[0]: <I64*32>>[29]: 0, "
            "<li.outputs[0]: <I64*32>>[30]: 0, "
            "<li.outputs[0]: <I64*32>>[31]: 0, "
            "<ca.outputs[0]: <CA>>[0]: 1})"
        )

    def test_repr(self):
        fn, _arg = self.make_add_fn()
        self.assertEqual(
            fn.ops_to_str(),
            "arg:\n"
            "    (<...outputs[0]: <I64>>) <= FuncArgR3\n"
            "vl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x20)\n"
            "ld:\n"
            "    (<...outputs[0]: <I64*32>>) <= SvLd(\n"
            "    <arg.outputs[0]: <I64>>, <vl.outputs[0]: <VL_MAXVL>>, 0x0)\n"
            "li:\n"
            "    (<...outputs[0]: <I64*32>>) <= SvLI(\n"
            "    <vl.outputs[0]: <VL_MAXVL>>, 0x0)\n"
            "ca:\n"
            "    (<...outputs[0]: <CA>>) <= SetCA\n"
            "add:\n"
            "    (<...outputs[0]: <I64*32>>, <...outputs[1]: <CA>>\n"
            "    ) <= SvAddE(<ld.outputs[0]: <I64*32>>,\n"
            "    <li.outputs[0]: <I64*32>>, <ca.outputs[0]: <CA>>,\n"
            "    <vl.outputs[0]: <VL_MAXVL>>)\n"
            "st:\n"
            "    SvStd(<add.outputs[0]: <I64*32>>, <arg.outputs[0]: <I64>>,\n"
            "    <vl.outputs[0]: <VL_MAXVL>>, 0x0)"
        )
        self.assertEqual(
            fn.ops_to_str(as_python_literal=True), r"""
            "arg:\n"
            "    (<...outputs[0]: <I64>>) <= FuncArgR3\n"
            "vl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x20)\n"
            "ld:\n"
            "    (<...outputs[0]: <I64*32>>) <= SvLd(\n"
            "    <arg.outputs[0]: <I64>>, <vl.outputs[0]: <VL_MAXVL>>, 0x0)\n"
            "li:\n"
            "    (<...outputs[0]: <I64*32>>) <= SvLI(\n"
            "    <vl.outputs[0]: <VL_MAXVL>>, 0x0)\n"
            "ca:\n"
            "    (<...outputs[0]: <CA>>) <= SetCA\n"
            "add:\n"
            "    (<...outputs[0]: <I64*32>>, <...outputs[1]: <CA>>\n"
            "    ) <= SvAddE(<ld.outputs[0]: <I64*32>>,\n"
            "    <li.outputs[0]: <I64*32>>, <ca.outputs[0]: <CA>>,\n"
            "    <vl.outputs[0]: <VL_MAXVL>>)\n"
            "st:\n"
            "    SvStd(<add.outputs[0]: <I64*32>>, <arg.outputs[0]: <I64>>,\n"
            "    <vl.outputs[0]: <VL_MAXVL>>, 0x0)"
"""[1:-1]
        )
        self.assertEqual([repr(op.properties) for op in fn.ops], [
            "OpProperties(kind=OpKind.FuncArgR3, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([3])}), ty=<I64>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SetVLI, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SvLd, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 13), *range(14, 128)])}), "
            "ty=<I64>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early)), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early),), maxvl=32, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SvLI, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early),), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early),), maxvl=32, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SetCA, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.CA: FBitSet([0])}), ty=<CA>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SvAddE, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.CA: FBitSet([0])}), ty=<CA>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early)), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.CA: FBitSet([0])}), ty=<CA>), "
            "tied_input_index=2, spread_index=None, "
            "write_stage=OpStage.Early)), maxvl=32, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SvStd, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 13), *range(14, 128)])}), "
            "ty=<I64>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early)), "
            "outputs=(), maxvl=32, copy_reg_len=0)",
        ])

    def test_pre_ra_insert_copies(self):
        fn, _arg = self.make_add_fn()
        fn.pre_ra_insert_copies()
        self.assertEqual(
            fn.ops_to_str(),
            "arg:\n"
            "    (<...outputs[0]: <I64>>) <= FuncArgR3\n"
            "arg.out0.copy:\n"
            "    (<...outputs[0]: <I64>>) <= CopyFromReg(\n"
            "    <arg.outputs[0]: <I64>>)\n"
            "vl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x20)\n"
            "ld.inp0.copy:\n"
            "    (<...outputs[0]: <I64>>) <= CopyToReg(\n"
            "    <arg.out0.copy.outputs[0]: <I64>>)\n"
            "ld.inp1.setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x20)\n"
            "ld:\n"
            "    (<...outputs[0]: <I64*32>>) <= SvLd(\n"
            "    <ld.inp0.copy.outputs[0]: <I64>>,\n"
            "    <ld.inp1.setvl.outputs[0]: <VL_MAXVL>>, 0x0)\n"
            "ld.out0.setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x20)\n"
            "ld.out0.copy:\n"
            "    (<...outputs[0]: <I64*32>>) <= VecCopyFromReg(\n"
            "    <ld.outputs[0]: <I64*32>>,\n"
            "    <ld.out0.setvl.outputs[0]: <VL_MAXVL>>)\n"
            "li.inp0.setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x20)\n"
            "li:\n"
            "    (<...outputs[0]: <I64*32>>) <= SvLI(\n"
            "    <li.inp0.setvl.outputs[0]: <VL_MAXVL>>, 0x0)\n"
            "li.out0.setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x20)\n"
            "li.out0.copy:\n"
            "    (<...outputs[0]: <I64*32>>) <= VecCopyFromReg(\n"
            "    <li.outputs[0]: <I64*32>>,\n"
            "    <li.out0.setvl.outputs[0]: <VL_MAXVL>>)\n"
            "ca:\n"
            "    (<...outputs[0]: <CA>>) <= SetCA\n"
            "add.inp0.setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x20)\n"
            "add.inp0.copy:\n"
            "    (<...outputs[0]: <I64*32>>) <= VecCopyToReg(\n"
            "    <ld.out0.copy.outputs[0]: <I64*32>>,\n"
            "    <add.inp0.setvl.outputs[0]: <VL_MAXVL>>)\n"
            "add.inp1.setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x20)\n"
            "add.inp1.copy:\n"
            "    (<...outputs[0]: <I64*32>>) <= VecCopyToReg(\n"
            "    <li.out0.copy.outputs[0]: <I64*32>>,\n"
            "    <add.inp1.setvl.outputs[0]: <VL_MAXVL>>)\n"
            "add.inp3.setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x20)\n"
            "add:\n"
            "    (<...outputs[0]: <I64*32>>, <...outputs[1]: <CA>>\n"
            "    ) <= SvAddE(<add.inp0.copy.outputs[0]: <I64*32>>,\n"
            "    <add.inp1.copy.outputs[0]: <I64*32>>,\n"
            "    <ca.outputs[0]: <CA>>,\n"
            "    <add.inp3.setvl.outputs[0]: <VL_MAXVL>>)\n"
            "add.out0.setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x20)\n"
            "add.out0.copy:\n"
            "    (<...outputs[0]: <I64*32>>) <= VecCopyFromReg(\n"
            "    <add.outputs[0]: <I64*32>>,\n"
            "    <add.out0.setvl.outputs[0]: <VL_MAXVL>>)\n"
            "st.inp0.setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x20)\n"
            "st.inp0.copy:\n"
            "    (<...outputs[0]: <I64*32>>) <= VecCopyToReg(\n"
            "    <add.out0.copy.outputs[0]: <I64*32>>,\n"
            "    <st.inp0.setvl.outputs[0]: <VL_MAXVL>>)\n"
            "st.inp1.copy:\n"
            "    (<...outputs[0]: <I64>>) <= CopyToReg(\n"
            "    <arg.out0.copy.outputs[0]: <I64>>)\n"
            "st.inp2.setvl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x20)\n"
            "st:\n"
            "    SvStd(<st.inp0.copy.outputs[0]: <I64*32>>,\n"
            "    <st.inp1.copy.outputs[0]: <I64>>,\n"
            "    <st.inp2.setvl.outputs[0]: <VL_MAXVL>>, 0x0)"
        )
        self.assertEqual([repr(op.properties) for op in fn.ops], [
            "OpProperties(kind=OpKind.FuncArgR3, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([3])}), ty=<I64>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.CopyFromReg, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 13), *range(14, 128)])}), "
            "ty=<I64>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early),), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 13), *range(14, 128)]), "
            "LocKind.StackI64: FBitSet(range(0, 512))}), ty=<I64>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=1)",
            "OpProperties(kind=OpKind.SetVLI, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.CopyToReg, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 13), *range(14, 128)]), "
            "LocKind.StackI64: FBitSet(range(0, 512))}), ty=<I64>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early),), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 13), *range(14, 128)])}), "
            "ty=<I64>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=1)",
            "OpProperties(kind=OpKind.SetVLI, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SvLd, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 13), *range(14, 128)])}), "
            "ty=<I64>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early)), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early),), maxvl=32, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SetVLI, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.VecCopyFromReg, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early)), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97)), "
            "LocKind.StackI64: FBitSet(range(0, 481))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=32, copy_reg_len=32)",
            "OpProperties(kind=OpKind.SetVLI, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SvLI, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early),), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early),), maxvl=32, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SetVLI, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.VecCopyFromReg, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early)), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97)), "
            "LocKind.StackI64: FBitSet(range(0, 481))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=32, copy_reg_len=32)",
            "OpProperties(kind=OpKind.SetCA, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.CA: FBitSet([0])}), ty=<CA>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SetVLI, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.VecCopyToReg, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97)), "
            "LocKind.StackI64: FBitSet(range(0, 481))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early)), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=32, copy_reg_len=32)",
            "OpProperties(kind=OpKind.SetVLI, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.VecCopyToReg, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97)), "
            "LocKind.StackI64: FBitSet(range(0, 481))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early)), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=32, copy_reg_len=32)",
            "OpProperties(kind=OpKind.SetVLI, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SvAddE, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.CA: FBitSet([0])}), ty=<CA>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early)), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.CA: FBitSet([0])}), ty=<CA>), "
            "tied_input_index=2, spread_index=None, "
            "write_stage=OpStage.Early)), maxvl=32, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SetVLI, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.VecCopyFromReg, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early)), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97)), "
            "LocKind.StackI64: FBitSet(range(0, 481))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=32, copy_reg_len=32)",
            "OpProperties(kind=OpKind.SetVLI, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.VecCopyToReg, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97)), "
            "LocKind.StackI64: FBitSet(range(0, 481))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early)), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=32, copy_reg_len=32)",
            "OpProperties(kind=OpKind.CopyToReg, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 13), *range(14, 128)]), "
            "LocKind.StackI64: FBitSet(range(0, 512))}), ty=<I64>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early),), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 13), *range(14, 128)])}), "
            "ty=<I64>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=1)",
            "OpProperties(kind=OpKind.SetVLI, "
            "inputs=(), "
            "outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),), maxvl=1, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SvStd, "
            "inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet(range(14, 97))}), ty=<I64*32>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 13), *range(14, 128)])}), "
            "ty=<I64>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), ty=<VL_MAXVL>), "
            "tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early)), "
            "outputs=(), maxvl=32, copy_reg_len=0)",
        ])

    def test_sim(self):
        fn, arg = self.make_add_fn()
        addr = 0x100
        state = PreRASimState(ssa_vals={arg: (addr,)}, memory={})
        state.store(addr=addr, value=0xffffffff_ffffffff,
                    size_in_bytes=GPR_SIZE_IN_BYTES)
        state.store(addr=addr + GPR_SIZE_IN_BYTES, value=0xabcdef01_23456789,
                    size_in_bytes=GPR_SIZE_IN_BYTES)
        self.assertEqual(
            repr(state),
            "PreRASimState(memory={\n"
            "0x00100: <0xffffffffffffffff>,\n"
            "0x00108: <0xabcdef0123456789>}, "
            "ssa_vals={<arg.outputs[0]: <I64>>: (0x100,)})")
        fn.sim(state)
        self.assertEqual(
            repr(state),
            "PreRASimState(memory={\n"
            "0x00100: <0x0000000000000000>,\n"
            "0x00108: <0xabcdef012345678a>,\n"
            "0x00110: <0x0000000000000000>,\n"
            "0x00118: <0x0000000000000000>,\n"
            "0x00120: <0x0000000000000000>,\n"
            "0x00128: <0x0000000000000000>,\n"
            "0x00130: <0x0000000000000000>,\n"
            "0x00138: <0x0000000000000000>,\n"
            "0x00140: <0x0000000000000000>,\n"
            "0x00148: <0x0000000000000000>,\n"
            "0x00150: <0x0000000000000000>,\n"
            "0x00158: <0x0000000000000000>,\n"
            "0x00160: <0x0000000000000000>,\n"
            "0x00168: <0x0000000000000000>,\n"
            "0x00170: <0x0000000000000000>,\n"
            "0x00178: <0x0000000000000000>,\n"
            "0x00180: <0x0000000000000000>,\n"
            "0x00188: <0x0000000000000000>,\n"
            "0x00190: <0x0000000000000000>,\n"
            "0x00198: <0x0000000000000000>,\n"
            "0x001a0: <0x0000000000000000>,\n"
            "0x001a8: <0x0000000000000000>,\n"
            "0x001b0: <0x0000000000000000>,\n"
            "0x001b8: <0x0000000000000000>,\n"
            "0x001c0: <0x0000000000000000>,\n"
            "0x001c8: <0x0000000000000000>,\n"
            "0x001d0: <0x0000000000000000>,\n"
            "0x001d8: <0x0000000000000000>,\n"
            "0x001e0: <0x0000000000000000>,\n"
            "0x001e8: <0x0000000000000000>,\n"
            "0x001f0: <0x0000000000000000>,\n"
            "0x001f8: <0x0000000000000000>}, ssa_vals={\n"
            "<arg.outputs[0]: <I64>>: (0x100,),\n"
            "<vl.outputs[0]: <VL_MAXVL>>: (0x20,),\n"
            "<ld.outputs[0]: <I64*32>>: (\n"
            "    0xffffffffffffffff, 0xabcdef0123456789, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0),\n"
            "<li.outputs[0]: <I64*32>>: (\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0),\n"
            "<ca.outputs[0]: <CA>>: (0x1,),\n"
            "<add.outputs[0]: <I64*32>>: (\n"
            "    0x0, 0xabcdef012345678a, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0,\n"
            "    0x0, 0x0, 0x0, 0x0),\n"
            "<add.outputs[1]: <CA>>: (0x0,),\n"
            "})")

    def test_gen_asm(self):
        fn, _arg = self.make_add_fn()
        fn.pre_ra_insert_copies()
        VL_LOC = Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1)
        CA_LOC = Loc(kind=LocKind.CA, start=0, reg_len=1)
        state = GenAsmState(allocated_locs={
            fn.ops[0].outputs[0]: Loc(kind=LocKind.GPR, start=3, reg_len=1),
            fn.ops[1].outputs[0]: Loc(kind=LocKind.GPR, start=3, reg_len=1),
            fn.ops[2].outputs[0]: VL_LOC,
            fn.ops[3].outputs[0]: Loc(kind=LocKind.GPR, start=3, reg_len=1),
            fn.ops[4].outputs[0]: VL_LOC,
            fn.ops[5].outputs[0]: Loc(kind=LocKind.GPR, start=32, reg_len=32),
            fn.ops[6].outputs[0]: VL_LOC,
            fn.ops[7].outputs[0]: Loc(kind=LocKind.GPR, start=32, reg_len=32),
            fn.ops[8].outputs[0]: VL_LOC,
            fn.ops[9].outputs[0]: Loc(kind=LocKind.GPR, start=64, reg_len=32),
            fn.ops[10].outputs[0]: VL_LOC,
            fn.ops[11].outputs[0]: Loc(kind=LocKind.GPR, start=64, reg_len=32),
            fn.ops[12].outputs[0]: CA_LOC,
            fn.ops[13].outputs[0]: VL_LOC,
            fn.ops[14].outputs[0]: Loc(kind=LocKind.GPR, start=32, reg_len=32),
            fn.ops[15].outputs[0]: VL_LOC,
            fn.ops[16].outputs[0]: Loc(kind=LocKind.GPR, start=64, reg_len=32),
            fn.ops[17].outputs[0]: VL_LOC,
            fn.ops[18].outputs[0]: Loc(kind=LocKind.GPR, start=32, reg_len=32),
            fn.ops[18].outputs[1]: CA_LOC,
            fn.ops[19].outputs[0]: VL_LOC,
            fn.ops[20].outputs[0]: Loc(kind=LocKind.GPR, start=32, reg_len=32),
            fn.ops[21].outputs[0]: VL_LOC,
            fn.ops[22].outputs[0]: Loc(kind=LocKind.GPR, start=32, reg_len=32),
            fn.ops[23].outputs[0]: Loc(kind=LocKind.GPR, start=3, reg_len=1),
            fn.ops[24].outputs[0]: VL_LOC,
        })
        fn.gen_asm(state)
        self.assertEqual(state.output, [
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1',
            'sv.ld *32, 0(3)',
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1',
            'sv.addi *64, 0, 0',
            'setvl 0, 0, 32, 0, 1, 1',
            'subfc 0, 0, 0',
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1',
            'sv.adde *32, *32, *64',
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1',
            'sv.std *32, 0(3)',
        ])

    def test_spread(self):
        fn = Fn()
        maxvl = 4
        vl = fn.append_new_op(OpKind.SetVLI, immediates=[maxvl],
                              name="vl", maxvl=maxvl).outputs[0]
        li = fn.append_new_op(OpKind.SvLI, input_vals=[vl], immediates=[0],
                              name="li", maxvl=maxvl).outputs[0]
        spread_op = fn.append_new_op(OpKind.Spread, input_vals=[li, vl],
                                     name="spread", maxvl=maxvl)
        self.assertEqual(spread_op.outputs[0].ty_before_spread,
                         Ty(base_ty=BaseTy.I64, reg_len=maxvl))
        _concat = fn.append_new_op(
            OpKind.Concat, input_vals=[*spread_op.outputs[::-1], vl],
            name="concat", maxvl=maxvl)
        self.assertEqual([repr(op.properties) for op in fn.ops], [
            "OpProperties(kind=OpKind.SetVLI, inputs=("
            "), outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), "
            "ty=<VL_MAXVL>), tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),"
            "), maxvl=4, copy_reg_len=0)",
            "OpProperties(kind=OpKind.SvLI, inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), "
            "ty=<VL_MAXVL>), tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early),"
            "), outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 10), *range(14, 125)])}), "
            "ty=<I64*4>), tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early),"
            "), maxvl=4, copy_reg_len=0)",
            "OpProperties(kind=OpKind.Spread, inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 10), *range(14, 125)])}), "
            "ty=<I64*4>), tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), "
            "ty=<VL_MAXVL>), tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early)"
            "), outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 10), *range(14, 125)])}), "
            "ty=<I64*4>), tied_input_index=None, spread_index=0, "
            "write_stage=OpStage.Late), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 10), *range(14, 125)])}), "
            "ty=<I64*4>), tied_input_index=None, spread_index=1, "
            "write_stage=OpStage.Late), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 10), *range(14, 125)])}), "
            "ty=<I64*4>), tied_input_index=None, spread_index=2, "
            "write_stage=OpStage.Late), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 10), *range(14, 125)])}), "
            "ty=<I64*4>), tied_input_index=None, spread_index=3, "
            "write_stage=OpStage.Late)"
            "), maxvl=4, copy_reg_len=4)",
            "OpProperties(kind=OpKind.Concat, inputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 10), *range(14, 125)])}), "
            "ty=<I64*4>), tied_input_index=None, spread_index=0, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 10), *range(14, 125)])}), "
            "ty=<I64*4>), tied_input_index=None, spread_index=1, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 10), *range(14, 125)])}), "
            "ty=<I64*4>), tied_input_index=None, spread_index=2, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 10), *range(14, 125)])}), "
            "ty=<I64*4>), tied_input_index=None, spread_index=3, "
            "write_stage=OpStage.Early), "
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.VL_MAXVL: FBitSet([0])}), "
            "ty=<VL_MAXVL>), tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Early)"
            "), outputs=("
            "OperandDesc(loc_set_before_spread=LocSet(starts=FMap({"
            "LocKind.GPR: FBitSet([*range(3, 10), *range(14, 125)])}), "
            "ty=<I64*4>), tied_input_index=None, spread_index=None, "
            "write_stage=OpStage.Late),"
            "), maxvl=4, copy_reg_len=4)",
        ])
        self.assertEqual(
            fn.ops_to_str(),
            "vl:\n"
            "    (<...outputs[0]: <VL_MAXVL>>) <= SetVLI(0x4)\n"
            "li:\n"
            "    (<...outputs[0]: <I64*4>>) <= SvLI(\n"
            "    <vl.outputs[0]: <VL_MAXVL>>, 0x0)\n"
            "spread:\n"
            "    (<...outputs[0]: <I64>>, <...outputs[1]: <I64>>,\n"
            "    <...outputs[2]: <I64>>, <...outputs[3]: <I64>>) <= Spread(\n"
            "    <li.outputs[0]: <I64*4>>, <vl.outputs[0]: <VL_MAXVL>>)\n"
            "concat:\n"
            "    (<...outputs[0]: <I64*4>>) <= Concat(\n"
            "    <spread.outputs[3]: <I64>>, <spread.outputs[2]: <I64>>,\n"
            "    <spread.outputs[1]: <I64>>, <spread.outputs[0]: <I64>>,\n"
            "    <vl.outputs[0]: <VL_MAXVL>>)"
        )
        fn_analysis = FnAnalysis(fn)
        self.assertEqual(
            repr(fn_analysis.copies),
            "FMap({"
            "<spread.outputs[0]: <I64>>[0]: <li.outputs[0]: <I64*4>>[0], "
            "<spread.outputs[1]: <I64>>[0]: <li.outputs[0]: <I64*4>>[1], "
            "<spread.outputs[2]: <I64>>[0]: <li.outputs[0]: <I64*4>>[2], "
            "<spread.outputs[3]: <I64>>[0]: <li.outputs[0]: <I64*4>>[3], "
            "<concat.outputs[0]: <I64*4>>[0]: <li.outputs[0]: <I64*4>>[3], "
            "<concat.outputs[0]: <I64*4>>[1]: <li.outputs[0]: <I64*4>>[2], "
            "<concat.outputs[0]: <I64*4>>[2]: <li.outputs[0]: <I64*4>>[1], "
            "<concat.outputs[0]: <I64*4>>[3]: <li.outputs[0]: <I64*4>>[0]})"
        )
        self.assertEqual(
            repr(fn_analysis.const_ssa_val_sub_regs),
            "FMap({"
            "<vl.outputs[0]: <VL_MAXVL>>[0]: 4, "
            "<li.outputs[0]: <I64*4>>[0]: 0, "
            "<li.outputs[0]: <I64*4>>[1]: 0, "
            "<li.outputs[0]: <I64*4>>[2]: 0, "
            "<li.outputs[0]: <I64*4>>[3]: 0, "
            "<spread.outputs[0]: <I64>>[0]: 0, "
            "<spread.outputs[1]: <I64>>[0]: 0, "
            "<spread.outputs[2]: <I64>>[0]: 0, "
            "<spread.outputs[3]: <I64>>[0]: 0, "
            "<concat.outputs[0]: <I64*4>>[0]: 0, "
            "<concat.outputs[0]: <I64*4>>[1]: 0, "
            "<concat.outputs[0]: <I64*4>>[2]: 0, "
            "<concat.outputs[0]: <I64*4>>[3]: 0})"
        )


if __name__ == "__main__":
    _ = unittest.main()
