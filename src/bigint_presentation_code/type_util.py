from typing import TYPE_CHECKING, Any, NoReturn, Union

if TYPE_CHECKING:
    from typing_extensions import Literal, Self, final
else:
    def final(v):
        return v

    class _Literal:
        def __getitem__(self, v):
            if isinstance(v, tuple):
                return Union[tuple(type(i) for i in v)]
            return type(v)

    Literal = _Literal()

    Self = Any


# pyright currently doesn't like typing_extensions' definition
# -- added to typing in python 3.11
def assert_never(arg):
    # type: (NoReturn) -> NoReturn
    raise AssertionError("got to code that's supposed to be unreachable")


__all__ = [
    "assert_never",
    "final",
    "Literal",
    "Self",
]
