from abc import ABCMeta, abstractmethod
from collections import defaultdict
from typing import (AbstractSet, Any, Callable, Generic, Iterable, Iterator,
                    Mapping, MutableSet, NewType, TypeVar, overload)

from bigint_presentation_code.type_util import Self, final
from nmutil.plain_data import plain_data

_T_co = TypeVar("_T_co", covariant=True)
_T = TypeVar("_T")
_T2 = TypeVar("_T2")

__all__ = [
    "BaseBitSet",
    "bit_count",
    "BitSet",
    "DisjointSets",
    "DisjointSetsItem",
    "FBitSet",
    "FMap",
    "Interned",
    "OFSet",
    "OSet",
    "top_set_bit_index",
    "trailing_zero_count",
]


class _InternedMeta(ABCMeta):
    def __call__(self, *args: Any, **kwds: Any) -> Any:
        return super().__call__(*args, **kwds)._Interned__intern()


class Interned(metaclass=_InternedMeta):
    def __init_intern(self):
        # type: (Self) -> Self
        cls = type(self)
        old_hash = cls.__hash__
        old_hash = getattr(old_hash, "_Interned__old_hash", old_hash)
        old_eq = cls.__eq__
        old_eq = getattr(old_eq, "_Interned__old_eq", old_eq)

        def __hash__(self):
            # type: (Self) -> int
            return self._Interned__hash  # type: ignore
        __hash__._Interned__old_hash = old_hash  # type: ignore
        cls.__hash__ = __hash__

        def __eq__(self,  # type: Self
                   __other,  # type: Any
                   *, __eq=old_eq,  # type: Callable[[Self, Any], bool]
                   ):
            # type: (...) -> bool
            if self.__class__ is __other.__class__:
                return self is __other
            return __eq(self, __other)
        __eq__._Interned__old_eq = old_eq  # type: ignore
        cls.__eq__ = __eq__

        table = defaultdict(list)  # type: dict[int, list[Self]]

        def __intern(self,  # type: Self
                     *, __hash=old_hash,  # type: Callable[[Self], int]
                     __eq=old_eq,  # type: Callable[[Self, Any], bool]
                     __table=table,  # type: dict[int, list[Self]]
                     __NotImplemented=NotImplemented,  # type: Any
                     ):
            # type: (...) -> Self
            h = __hash(self)
            bucket = __table[h]
            for i in bucket:
                v = __eq(self, i)
                if v is not __NotImplemented and v:
                    return i
            self.__dict__["_Interned__hash"] = h
            bucket.append(self)
            return self
        cls._Interned__intern = __intern
        return __intern(self)

    _Interned__intern = __init_intern


class OFSet(AbstractSet[_T_co], Interned):
    """ ordered frozen set """
    __slots__ = "__items", "__dict__", "__weakref__"

    def __init__(self, items=()):
        # type: (Iterable[_T_co]) -> None
        super().__init__()
        if isinstance(items, OFSet):
            self.__items = items.__items
        else:
            self.__items = {v: None for v in items}

    def __contains__(self, x):
        # type: (Any) -> bool
        return x in self.__items

    def __iter__(self):
        # type: () -> Iterator[_T_co]
        return iter(self.__items)

    def __len__(self):
        # type: () -> int
        return len(self.__items)

    def __hash__(self):
        # type: () -> int
        return self._hash()

    def __repr__(self):
        # type: () -> str
        if len(self) == 0:
            return "OFSet()"
        return f"OFSet({list(self)})"


class OSet(MutableSet[_T]):
    """ ordered mutable set """
    __slots__ = "__items", "__dict__"

    def __init__(self, items=()):
        # type: (Iterable[_T]) -> None
        super().__init__()
        self.__items = {v: None for v in items}

    def __contains__(self, x):
        # type: (Any) -> bool
        return x in self.__items

    def __iter__(self):
        # type: () -> Iterator[_T]
        return iter(self.__items)

    def __len__(self):
        # type: () -> int
        return len(self.__items)

    def add(self, value):
        # type: (_T) -> None
        self.__items[value] = None

    def discard(self, value):
        # type: (_T) -> None
        self.__items.pop(value, None)

    def remove(self, value):
        # type: (_T) -> None
        del self.__items[value]

    def pop(self):
        # type: () -> _T
        return self.__items.popitem()[0]

    def clear(self):
        # type: () -> None
        self.__items.clear()

    def __repr__(self):
        # type: () -> str
        if len(self) == 0:
            return "OSet()"
        return f"OSet({list(self)})"


class FMap(Mapping[_T, _T_co], Interned):
    """ordered frozen hashable mapping"""
    __slots__ = "__items", "__hash", "__dict__", "__weakref__"

    @overload
    def __init__(self, items):
        # type: (Mapping[_T, _T_co]) -> None
        ...

    @overload
    def __init__(self, items):
        # type: (Iterable[tuple[_T, _T_co]]) -> None
        ...

    @overload
    def __init__(self):
        # type: () -> None
        ...

    def __init__(self, items=()):
        # type: (Mapping[_T, _T_co] | Iterable[tuple[_T, _T_co]]) -> None
        super().__init__()
        self.__items = dict(items)  # type: dict[_T, _T_co]
        self.__hash = None  # type: None | int

    def __getitem__(self, item):
        # type: (_T) -> _T_co
        return self.__items[item]

    def __iter__(self):
        # type: () -> Iterator[_T]
        return iter(self.__items)

    def __len__(self):
        # type: () -> int
        return len(self.__items)

    def __eq__(self, other):
        # type: (FMap[Any, Any] | Any) -> bool
        if isinstance(other, FMap):
            return self.__items == other.__items
        return super().__eq__(other)

    def __hash__(self):
        # type: () -> int
        if self.__hash is None:
            self.__hash = hash(frozenset(self.items()))
        return self.__hash

    def __repr__(self):
        # type: () -> str
        return f"FMap({self.__items})"

    def get(self, key, default=None):
        # type: (_T, _T_co | _T2) -> _T_co | _T2
        return self.__items.get(key, default)

    def __contains__(self, key):
        # type: (_T | object) -> bool
        return key in self.__items


def trailing_zero_count(v, default=-1):
    # type: (int, int) -> int
    without_bit = v & (v - 1)  # clear lowest set bit
    bit = v & ~without_bit  # extract lowest set bit
    return top_set_bit_index(bit, default)


def top_set_bit_index(v, default=-1):
    # type: (int, int) -> int
    if v <= 0:
        return default
    return v.bit_length() - 1


try:
    # added in cpython 3.10
    bit_count = int.bit_count  # type: ignore
except AttributeError:
    def bit_count(v):
        # type: (int) -> int
        """returns the number of 1 bits in the absolute value of the input"""
        return bin(abs(v)).count('1')


class BaseBitSet(AbstractSet[int]):
    __slots__ = "__bits", "__dict__", "__weakref__"

    @classmethod
    @abstractmethod
    def _frozen(cls):
        # type: () -> bool
        return False

    @classmethod
    def _from_bits(cls, bits):
        # type: (int) -> Self
        return cls(bits=bits)

    def __init__(self, items=(), bits=0):
        # type: (Iterable[int], int) -> None
        super().__init__()
        if isinstance(items, BaseBitSet):
            bits |= items.bits
        else:
            for item in items:
                if item < 0:
                    raise ValueError("can't store negative integers")
                bits |= 1 << item
        if bits < 0:
            raise ValueError("can't store an infinite set")
        self.__bits = bits

    @property
    def bits(self):
        # type: () -> int
        return self.__bits

    @bits.setter
    def bits(self, bits):
        # type: (int) -> None
        if self._frozen():
            raise AttributeError("can't write to frozen bitset's bits")
        if bits < 0:
            raise ValueError("can't store an infinite set")
        self.__bits = bits

    def __contains__(self, x):
        # type: (Any) -> bool
        if isinstance(x, int) and x >= 0:
            return (1 << x) & self.bits != 0
        return False

    def __iter__(self):
        # type: () -> Iterator[int]
        bits = self.bits
        while bits != 0:
            index = trailing_zero_count(bits)
            yield index
            bits -= 1 << index

    def __reversed__(self):
        # type: () -> Iterator[int]
        bits = self.bits
        while bits != 0:
            index = top_set_bit_index(bits)
            yield index
            bits -= 1 << index

    def __len__(self):
        # type: () -> int
        return bit_count(self.bits)

    def __repr__(self):
        # type: () -> str
        if self.bits == 0:
            return f"{self.__class__.__name__}()"
        len_self = len(self)
        if len_self <= 3:
            v = list(self)
            return f"{self.__class__.__name__}({v})"
        ranges = []  # type: list[range]
        MAX_RANGES = 5
        for i in self:
            if len(ranges) != 0 and ranges[-1].stop == i:
                ranges[-1] = range(
                    ranges[-1].start, i + ranges[-1].step, ranges[-1].step)
            elif len(ranges) != 0 and len(ranges[-1]) == 1:
                start = ranges[-1][0]
                step = i - start
                stop = i + step
                ranges[-1] = range(start, stop, step)
            elif len(ranges) != 0 and len(ranges[-1]) == 2:
                single = ranges[-1][0]
                start = ranges[-1][1]
                ranges[-1] = range(single, single + 1)
                step = i - start
                stop = i + step
                ranges.append(range(start, stop, step))
            else:
                ranges.append(range(i, i + 1))
            if len(ranges) > MAX_RANGES:
                break
        if len(ranges) == 1:
            return f"{self.__class__.__name__}({ranges[0]})"
        if len(ranges) <= MAX_RANGES:
            range_strs = []  # type: list[str]
            for r in ranges:
                if len(r) == 1:
                    range_strs.append(str(r[0]))
                else:
                    range_strs.append(f"*{r}")
            ranges_str = ", ".join(range_strs)
            return f"{self.__class__.__name__}([{ranges_str}])"
        if self.bits > 0xFFFFFFFF and len_self < 10:
            v = list(self)
            return f"{self.__class__.__name__}({v})"
        return f"{self.__class__.__name__}(bits={hex(self.bits)})"

    def __eq__(self, other):
        # type: (Any) -> bool
        if not isinstance(other, BaseBitSet):
            return super().__eq__(other)
        return self.bits == other.bits

    def __and__(self, other):
        # type: (Iterable[Any]) -> Self
        if isinstance(other, BaseBitSet):
            return self._from_bits(self.bits & other.bits)
        bits = 0
        for item in other:
            if isinstance(item, int) and item >= 0:
                bits |= 1 << item
        return self._from_bits(self.bits & bits)

    __rand__ = __and__

    def __or__(self, other):
        # type: (Iterable[Any]) -> Self
        if isinstance(other, BaseBitSet):
            return self._from_bits(self.bits | other.bits)
        bits = self.bits
        for item in other:
            if isinstance(item, int) and item >= 0:
                bits |= 1 << item
        return self._from_bits(bits)

    __ror__ = __or__

    def __xor__(self, other):
        # type: (Iterable[Any]) -> Self
        if isinstance(other, BaseBitSet):
            return self._from_bits(self.bits ^ other.bits)
        bits = self.bits
        for item in other:
            if isinstance(item, int) and item >= 0:
                bits ^= 1 << item
        return self._from_bits(bits)

    __rxor__ = __xor__

    def __sub__(self, other):
        # type: (Iterable[Any]) -> Self
        if isinstance(other, BaseBitSet):
            return self._from_bits(self.bits & ~other.bits)
        bits = self.bits
        for item in other:
            if isinstance(item, int) and item >= 0:
                bits &= ~(1 << item)
        return self._from_bits(bits)

    def __rsub__(self, other):
        # type: (Iterable[Any]) -> Self
        if isinstance(other, BaseBitSet):
            return self._from_bits(~self.bits & other.bits)
        bits = 0
        for item in other:
            if isinstance(item, int) and item >= 0:
                bits |= 1 << item
        return self._from_bits(~self.bits & bits)

    def isdisjoint(self, other):
        # type: (Iterable[Any]) -> bool
        if isinstance(other, BaseBitSet):
            return self.bits & other.bits == 0
        return super().isdisjoint(other)


class BitSet(BaseBitSet, MutableSet[int]):
    """Mutable Bit Set"""

    @final
    @classmethod
    def _frozen(cls):
        # type: () -> bool
        return False

    def add(self, value):
        # type: (int) -> None
        if value < 0:
            raise ValueError("can't store negative integers")
        self.bits |= 1 << value

    def discard(self, value):
        # type: (int) -> None
        if value >= 0:
            self.bits &= ~(1 << value)

    def clear(self):
        # type: () -> None
        self.bits = 0

    def __ior__(self, it):
        # type: (AbstractSet[Any]) -> Self
        if isinstance(it, BaseBitSet):
            self.bits |= it.bits
            return self
        return super().__ior__(it)

    def __iand__(self, it):
        # type: (AbstractSet[Any]) -> Self
        if isinstance(it, BaseBitSet):
            self.bits &= it.bits
            return self
        return super().__iand__(it)

    def __ixor__(self, it):
        # type: (AbstractSet[Any]) -> Self
        if isinstance(it, BaseBitSet):
            self.bits ^= it.bits
            return self
        return super().__ixor__(it)

    def __isub__(self, it):
        # type: (AbstractSet[Any]) -> Self
        if isinstance(it, BaseBitSet):
            self.bits &= ~it.bits
            return self
        return super().__isub__(it)


class FBitSet(BaseBitSet, Interned):
    """Frozen Bit Set"""

    @final
    @classmethod
    def _frozen(cls):
        # type: () -> bool
        return True

    def __hash__(self):
        # type: () -> int
        return super()._hash()


DisjointSetsItem = NewType("DisjointSetsItem", int)


@plain_data()
@final
class _DisjointSetsEntry(Generic[_T_co]):
    __slots__ = "value", "parent", "rank"

    def __init__(self, value, parent, rank):
        # type: (_T_co, DisjointSetsItem, int) -> None
        self.value = value
        self.parent = parent
        self.rank = rank


@final
class DisjointSets(Generic[_T_co]):
    """ Disjoint-set data structure, aka. union-find or merge-find
    https://en.wikipedia.org/wiki/Disjoint-set_data_structure
    """

    def __init__(self):
        self.__values = []  # type: list[_DisjointSetsEntry[_T_co]]

    def __entry(self, __key):
        # type: (DisjointSetsItem) -> _DisjointSetsEntry[_T_co]
        if __key < 0 or __key >= len(self.__values):
            raise KeyError(__key)
        return self.__values[__key]

    def __getitem__(self, __key):
        # type: (DisjointSetsItem) -> _T_co
        return self.__entry(__key).value

    def __setitem__(self, __key, __value):
        # type: (DisjointSetsItem, _T_co) -> None
        self.__entry(__key).value = __value

    def __len__(self):
        # type: () -> int
        return len(self.__values)

    @property
    def representatives(self):
        # type: () -> Iterator[DisjointSetsItem]
        for i, entry in enumerate(self.__values):
            item = DisjointSetsItem(i)
            if entry.parent == item:
                yield item

    def __iter__(self):
        # type: () -> Iterator[DisjointSetsItem]
        return map(DisjointSetsItem, range(len(self.__values)))

    def add_new_set(self, value):
        # type: (_T_co) -> DisjointSetsItem
        item = DisjointSetsItem(len(self.__values))
        self.__values.append(_DisjointSetsEntry(
            value=value, parent=item, rank=0))
        return item

    def find_representative(self, item):
        # type: (DisjointSetsItem) -> DisjointSetsItem
        entry = self.__entry(item)
        while entry.parent != item:
            parent_entry = self.__values[entry.parent]
            item = entry.parent = parent_entry.parent
            entry = self.__values[item]
        return item

    def merge(self, __x, __y):
        # type: (DisjointSetsItem, DisjointSetsItem) -> DisjointSetsItem
        __x = self.find_representative(__x)
        __y = self.find_representative(__y)
        if __x == __y:
            return __x
        x_entry = self.__values[__x]
        y_entry = self.__values[__y]
        if x_entry.rank < y_entry.rank:
            __x, __y = __y, __x
            x_entry, y_entry = y_entry, x_entry
        y_entry.parent = __x
        if x_entry.rank == y_entry.rank:
            x_entry.rank += 1
        return __x

    def __repr__(self):
        # type: () -> str
        sets = defaultdict(
            list)  # type: dict[DisjointSetsItem, list[DisjointSetsItem]]
        values = {}
        for item in self:
            sets[self.find_representative(item)].append(item)
            values[item] = self[item]
        return f"DisjointSets(sets={sets}, values={values})"
