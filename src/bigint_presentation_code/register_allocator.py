"""
Register Allocator for Toom-Cook algorithm generator for SVP64

this uses an algorithm based on:
[Retargetable Graph-Coloring Register Allocation for Irregular Architectures](https://user.it.uu.se/~svenolof/wpo/AllocSCOPES2003.20030626b.pdf)
"""

from dataclasses import dataclass
from functools import lru_cache, reduce
from itertools import combinations, count
from typing import Any, Callable, Container, Iterable, Iterator, Mapping, TextIO, Tuple

from cached_property import cached_property
from nmutil.plain_data import plain_data, replace

from bigint_presentation_code.compiler_ir import (BaseTy, Fn, FnAnalysis, Loc,
                                                  LocSet, Op, ProgramRange,
                                                  SSAVal, SSAValSubReg, Ty)
from bigint_presentation_code.type_util import final
from bigint_presentation_code.util import FMap, Interned, OFSet, OSet


class BadMergedSSAVal(ValueError):
    pass


_CopyRelation = Tuple[SSAValSubReg, SSAValSubReg]


@dataclass(frozen=True, repr=False, eq=False)
@final
class MergedSSAVal(Interned):
    """a set of `SSAVal`s along with their offsets, all register allocated as
    a single unit.

    Definition of the term `offset` for this class:

    Let `locs[x]` be the `Loc` that `x` is assigned to after register
    allocation and let `msv` be a `MergedSSAVal` instance, then the offset
    for each `SSAVal` `ssa_val` in `msv` is defined as:

    ```
    msv.ssa_val_offsets[ssa_val] = (msv.offset
                                    + locs[ssa_val].start - locs[msv].start)
    ```

    Example:
    ```
    v1.ty == <I64*4>
    v2.ty == <I64*2>
    v3.ty == <I64>
    msv = MergedSSAVal({v1: 0, v2: 4, v3: 1})
    msv.ty == <I64*6>
    ```
    if `msv` is allocated to `Loc(kind=LocKind.GPR, start=20, reg_len=6)`, then
    * `v1` is allocated to `Loc(kind=LocKind.GPR, start=20, reg_len=4)`
    * `v2` is allocated to `Loc(kind=LocKind.GPR, start=24, reg_len=2)`
    * `v3` is allocated to `Loc(kind=LocKind.GPR, start=21, reg_len=1)`
    """
    fn_analysis: FnAnalysis
    ssa_val_offsets: "FMap[SSAVal, int]"
    first_ssa_val: SSAVal
    loc_set: LocSet
    first_loc: Loc

    def __init__(self, fn_analysis, ssa_val_offsets, loc_set=None):
        # type: (FnAnalysis, Mapping[SSAVal, int] | SSAVal, LocSet | None) -> None
        object.__setattr__(self, "fn_analysis", fn_analysis)
        if isinstance(ssa_val_offsets, SSAVal):
            ssa_val_offsets = {ssa_val_offsets: 0}
        object.__setattr__(self, "ssa_val_offsets", FMap(ssa_val_offsets))
        first_ssa_val = None
        for ssa_val in self.ssa_vals:
            first_ssa_val = ssa_val
            break
        if first_ssa_val is None:
            raise BadMergedSSAVal("MergedSSAVal can't be empty")
        object.__setattr__(self, "first_ssa_val", first_ssa_val)
        # self.ty checks for mismatched base_ty
        reg_len = self.ty.reg_len
        if loc_set is not None and loc_set.ty != self.ty:
            raise ValueError(
                f"invalid loc_set, type doesn't match: "
                f"{loc_set.ty} != {self.ty}")
        for ssa_val, cur_offset in self.ssa_val_offsets_before_spread.items():
            def locs():
                # type: () -> Iterable[Loc]
                for loc in ssa_val.def_loc_set_before_spread:
                    disallowed_by_use = False
                    for use in fn_analysis.uses[ssa_val]:
                        # calculate the start for the use's Loc before spread
                        # e.g. if the def's Loc before spread starts at r6
                        # and the def's reg_offset_in_unspread is 5
                        # and the use's reg_offset_in_unspread is 3
                        # then the use's Loc before spread starts at r8
                        # because 8 == 6 + 5 - 3
                        start = (loc.start + ssa_val.reg_offset_in_unspread
                                 - use.reg_offset_in_unspread)
                        use_loc = Loc.try_make(
                            loc.kind, start=start,
                            reg_len=use.ty_before_spread.reg_len)
                        if (use_loc is None or
                                use_loc not in use.use_loc_set_before_spread):
                            disallowed_by_use = True
                            break
                    if disallowed_by_use:
                        continue
                    start = loc.start - cur_offset + self.offset
                    loc = Loc.try_make(loc.kind, start=start, reg_len=reg_len)
                    if loc is not None and (loc_set is None or loc in loc_set):
                        yield loc
            loc_set = LocSet(locs())
        assert loc_set is not None, "already checked that self isn't empty"
        first_loc = None
        for loc in loc_set:
            first_loc = loc
            break
        if first_loc is None:
            raise BadMergedSSAVal("there are no valid Locs left")
        object.__setattr__(self, "first_loc", first_loc)
        assert loc_set.ty == self.ty, "logic error somewhere"
        object.__setattr__(self, "loc_set", loc_set)
        self.__mergable_check()

    def __mergable_check(self):
        # type: () -> None
        """ checks that nothing is forcing two independent SSAVals
        to illegally overlap. This is required to avoid copy merging merging
        things that can't be merged.
        spread arguments are one of the things that can force two values to
        illegally overlap.
        """
        ops = OSet()  # type: Iterable[Op]
        for ssa_val in self.ssa_vals:
            ops.add(ssa_val.op)
            for use in self.fn_analysis.uses[ssa_val]:
                ops.add(use.op)
        ops = sorted(ops, key=self.fn_analysis.op_indexes.__getitem__)
        vals = {}  # type: dict[int, SSAValSubReg]
        for op in ops:
            for inp in op.input_vals:
                try:
                    ssa_val_offset = self.ssa_val_offsets[inp]
                except KeyError:
                    continue
                for orig_reg in inp.ssa_val_sub_regs:
                    reg_offset = ssa_val_offset + orig_reg.reg_idx
                    replaced_reg = vals[reg_offset]
                    if not self.fn_analysis.is_always_equal(
                            orig_reg, replaced_reg):
                        raise BadMergedSSAVal(
                            f"attempting to merge values that aren't known to "
                            f"be always equal: {orig_reg} != {replaced_reg}")
            output_offsets = dict.fromkeys(range(
                self.offset, self.offset + self.ty.reg_len))
            for out in op.outputs:
                try:
                    ssa_val_offset = self.ssa_val_offsets[out]
                except KeyError:
                    continue
                for reg in out.ssa_val_sub_regs:
                    reg_offset = ssa_val_offset + reg.reg_idx
                    try:
                        del output_offsets[reg_offset]
                    except KeyError:
                        raise BadMergedSSAVal("attempted to merge two outputs "
                                              "of the same instruction")
                    vals[reg_offset] = reg

    def __hash__(self):
        # type: () -> int
        return hash((self.fn_analysis, self.ssa_val_offsets, self.loc_set))

    def __eq__(self, other):
        # type: (MergedSSAVal | Any) -> bool
        if isinstance(other, MergedSSAVal):
            return self.fn_analysis == other.fn_analysis and \
                self.ssa_val_offsets == other.ssa_val_offsets and \
                self.loc_set == other.loc_set
        return NotImplemented

    @property
    def only_loc(self):
        # type: () -> Loc | None
        return self.loc_set.only_loc

    @cached_property
    def offset(self):
        # type: () -> int
        return min(self.ssa_val_offsets_before_spread.values())

    @property
    def base_ty(self):
        # type: () -> BaseTy
        return self.first_ssa_val.base_ty

    @cached_property
    def ssa_vals(self):
        # type: () -> OFSet[SSAVal]
        return OFSet(self.ssa_val_offsets.keys())

    @cached_property
    def ty(self):
        # type: () -> Ty
        reg_len = 0
        for ssa_val, offset in self.ssa_val_offsets_before_spread.items():
            cur_ty = ssa_val.ty_before_spread
            if self.base_ty != cur_ty.base_ty:
                raise BadMergedSSAVal(
                    f"BaseTy mismatch: {self.base_ty} != {cur_ty.base_ty}")
            reg_len = max(reg_len, cur_ty.reg_len + offset - self.offset)
        return Ty(base_ty=self.base_ty, reg_len=reg_len)

    @cached_property
    def ssa_val_offsets_before_spread(self):
        # type: () -> FMap[SSAVal, int]
        retval = {}  # type: dict[SSAVal, int]
        for ssa_val, offset in self.ssa_val_offsets.items():
            retval[ssa_val] = (
                offset - ssa_val.defining_descriptor.reg_offset_in_unspread)
        return FMap(retval)

    def offset_by(self, amount):
        # type: (int) -> MergedSSAVal
        v = {k: v + amount for k, v in self.ssa_val_offsets.items()}
        return MergedSSAVal(fn_analysis=self.fn_analysis, ssa_val_offsets=v)

    def normalized(self):
        # type: () -> MergedSSAVal
        return self.offset_by(-self.offset)

    def with_offset_to_match(self, target, additional_offset=0):
        # type: (MergedSSAVal | SSAVal, int) -> MergedSSAVal
        if isinstance(target, MergedSSAVal):
            ssa_val_offsets = target.ssa_val_offsets
        else:
            ssa_val_offsets = {target: 0}
        for ssa_val, offset in self.ssa_val_offsets.items():
            if ssa_val in ssa_val_offsets:
                return self.offset_by(
                    ssa_val_offsets[ssa_val] + additional_offset - offset)
        raise ValueError("can't change offset to match unrelated MergedSSAVal")

    def with_loc(self, loc):
        # type: (Loc) -> MergedSSAVal
        if loc not in self.loc_set:
            raise ValueError(
                f"Loc is not allowed -- not a member of `self.loc_set`: "
                f"{loc} not in {self.loc_set}")
        return MergedSSAVal(fn_analysis=self.fn_analysis,
                            ssa_val_offsets=self.ssa_val_offsets,
                            loc_set=LocSet([loc]))

    def merged(self, *others):
        # type: (*MergedSSAVal) -> MergedSSAVal
        retval = dict(self.ssa_val_offsets)
        for other in others:
            if other.fn_analysis != self.fn_analysis:
                raise ValueError("fn_analysis mismatch")
            for ssa_val, offset in other.ssa_val_offsets.items():
                if ssa_val in retval and retval[ssa_val] != offset:
                    raise BadMergedSSAVal(f"offset mismatch for {ssa_val}: "
                                          f"{retval[ssa_val]} != {offset}")
                retval[ssa_val] = offset
        return MergedSSAVal(fn_analysis=self.fn_analysis,
                            ssa_val_offsets=retval)

    @cached_property
    def live_interval(self):
        # type: () -> ProgramRange
        live_range = self.fn_analysis.live_ranges[self.first_ssa_val]
        start = live_range.start
        stop = live_range.stop
        for ssa_val in self.ssa_vals:
            live_range = self.fn_analysis.live_ranges[ssa_val]
            start = min(start, live_range.start)
            stop = max(stop, live_range.stop)
        return ProgramRange(start=start, stop=stop)

    def __repr__(self):
        return (f"MergedSSAVal(ssa_val_offsets={self.ssa_val_offsets}, "
                f"offset={self.offset}, ty={self.ty}, loc_set={self.loc_set}, "
                f"live_interval={self.live_interval})")

    @cached_property
    def copy_related_ssa_vals(self):
        # type: () -> OFSet[SSAVal]
        sets = OSet()  # type: OSet[OFSet[SSAVal]]
        # avoid merging the same sets multiple times
        for ssa_val in self.ssa_vals:
            sets.add(self.fn_analysis.copy_related_ssa_vals[ssa_val])
        return OFSet(v for s in sets for v in s)

    def get_copy_relation(self, other):
        # type: (MergedSSAVal) -> None | _CopyRelation
        for lhs_ssa_val in self.ssa_vals:
            for rhs_ssa_val in other.ssa_vals:
                for lhs in lhs_ssa_val.ssa_val_sub_regs:
                    for rhs in rhs_ssa_val.ssa_val_sub_regs:
                        lhs_src = self.fn_analysis.copies.get(lhs, lhs)
                        rhs_src = self.fn_analysis.copies.get(rhs, rhs)
                        if lhs_src == rhs_src:
                            return lhs, rhs
        return None

    def copy_merged(self, lhs_loc, rhs, rhs_loc, copy_relation):
        # type: (Loc | None, MergedSSAVal, Loc | None, _CopyRelation) -> MergedSSAVal
        retval = self.try_copy_merged(lhs_loc, rhs, rhs_loc, copy_relation)
        if isinstance(retval, MergedSSAVal):
            return retval
        raise retval

    def try_copy_merged(self, lhs_loc,  # type: Loc | None
                        rhs,  # type: MergedSSAVal
                        rhs_loc,  # type: Loc | None
                        copy_relation,  # type: _CopyRelation
                        ):
        # type: (...) -> MergedSSAVal | BadMergedSSAVal
        cr_lhs, cr_rhs = copy_relation
        if cr_lhs.ssa_val not in self.ssa_vals:
            cr_lhs, cr_rhs = cr_rhs, cr_lhs
        return self.__try_copy_merged(lhs_loc=lhs_loc, cr_lhs=cr_lhs,
                                      rhs=rhs, rhs_loc=rhs_loc, cr_rhs=cr_rhs)

    @lru_cache(maxsize=None, typed=True)
    def __try_copy_merged(self, lhs_loc,  # type: Loc | None
                          cr_lhs,  # type: SSAValSubReg
                          rhs,  # type: MergedSSAVal
                          rhs_loc,  # type: Loc | None
                          cr_rhs,  # type: SSAValSubReg
                          ):
        # type: (...) -> MergedSSAVal | BadMergedSSAVal
        try:
            lhs_merged = self.with_offset_to_match(
                cr_lhs.ssa_val, additional_offset=-cr_lhs.reg_idx)
            if lhs_loc is not None:
                lhs_merged = lhs_merged.with_loc(lhs_loc)
            rhs_merged = rhs.with_offset_to_match(
                cr_rhs.ssa_val, additional_offset=-cr_rhs.reg_idx)
            if rhs_loc is not None:
                rhs_merged = rhs_merged.with_loc(rhs_loc)
            return lhs_merged.merged(rhs_merged).normalized()
        except BadMergedSSAVal as e:
            return e


@final
class SSAValToMergedSSAValMap(Mapping[SSAVal, MergedSSAVal]):
    def __init__(self):
        # type: (...) -> None
        self.__map = {}  # type: dict[SSAVal, MergedSSAVal]
        self.__ig_node_map = MergedSSAValToIGNodeMap(
            _private_merged_ssa_val_map=self.__map)

    def __getitem__(self, __key):
        # type: (SSAVal) -> MergedSSAVal
        return self.__map[__key]

    def __iter__(self):
        # type: () -> Iterator[SSAVal]
        return iter(self.__map)

    def __len__(self):
        # type: () -> int
        return len(self.__map)

    @property
    def ig_node_map(self):
        # type: () -> MergedSSAValToIGNodeMap
        return self.__ig_node_map

    def __repr__(self):
        # type: () -> str
        s = ",\n".join(repr(v) for v in self.__ig_node_map)
        return f"SSAValToMergedSSAValMap({{{s}}})"


@final
class MergedSSAValToIGNodeMap(Mapping[MergedSSAVal, "IGNode"]):
    def __init__(
        self, *,
        _private_merged_ssa_val_map,  # type: dict[SSAVal, MergedSSAVal]
    ):
        # type: (...) -> None
        self.__merged_ssa_val_map = _private_merged_ssa_val_map
        self.__map = {}  # type: dict[MergedSSAVal, IGNode]
        self.__next_node_id = 0

    def __getitem__(self, __key):
        # type: (MergedSSAVal) -> IGNode
        return self.__map[__key]

    def __iter__(self):
        # type: () -> Iterator[MergedSSAVal]
        return iter(self.__map)

    def __len__(self):
        # type: () -> int
        return len(self.__map)

    def add_node(self, merged_ssa_val):
        # type: (MergedSSAVal) -> IGNode
        node = self.__map.get(merged_ssa_val, None)
        if node is not None:
            return node
        added = 0  # type: int | None
        try:
            for ssa_val in merged_ssa_val.ssa_vals:
                if ssa_val in self.__merged_ssa_val_map:
                    raise ValueError(
                        f"overlapping `MergedSSAVal`s: {ssa_val} is in both "
                        f"{merged_ssa_val} and "
                        f"{self.__merged_ssa_val_map[ssa_val]}")
                self.__merged_ssa_val_map[ssa_val] = merged_ssa_val
                added += 1
            retval = IGNode(
                node_id=self.__next_node_id, merged_ssa_val=merged_ssa_val,
                edges={}, loc=merged_ssa_val.only_loc, ignored=False)
            self.__map[merged_ssa_val] = retval
            self.__next_node_id += 1
            added = None
            return retval
        finally:
            if added is not None:
                # remove partially added stuff
                for idx, ssa_val in enumerate(merged_ssa_val.ssa_vals):
                    if idx >= added:
                        break
                    del self.__merged_ssa_val_map[ssa_val]

    def merge_into_one_node(self, final_merged_ssa_val):
        # type: (MergedSSAVal) -> IGNode
        source_nodes = OSet()  # type: OSet[IGNode]
        edges = {}  # type: dict[IGNode, IGEdge]
        for ssa_val in final_merged_ssa_val.ssa_vals:
            merged_ssa_val = self.__merged_ssa_val_map[ssa_val]
            source_node = self.__map[merged_ssa_val]
            if source_node.ignored:
                raise ValueError(f"can't merge ignored nodes: {source_node}")
            source_nodes.add(source_node)
            for i in merged_ssa_val.ssa_vals - final_merged_ssa_val.ssa_vals:
                raise ValueError(
                    f"SSAVal {i} appears in source IGNode's merged_ssa_val "
                    f"but not in merged IGNode's merged_ssa_val: "
                    f"source_node={source_node} "
                    f"final_merged_ssa_val={final_merged_ssa_val}")
            if source_node.loc != source_node.merged_ssa_val.only_loc:
                raise ValueError(
                    f"can't merge IGNodes: loc != merged_ssa_val.only_loc: "
                    f"{source_node.loc} != "
                    f"{source_node.merged_ssa_val.only_loc}")
            for n, edge in source_node.edges.items():
                if n in edges:
                    edge = edge.merged(edges[n])
                edges[n] = edge
        if len(source_nodes) == 1:
            return source_nodes.pop()  # merging a single node is a no-op
        # we're finished checking validity, now we can modify stuff
        for n in source_nodes:
            edges.pop(n, None)
        loc = final_merged_ssa_val.only_loc
        for n, edge in edges.items():
            if edge.copy_relation is None or not edge.interferes:
                continue
        retval = IGNode(
            node_id=self.__next_node_id, merged_ssa_val=final_merged_ssa_val,
            edges=edges, loc=loc, ignored=False)
        self.__next_node_id += 1
        empty_e = IGEdge()
        for node in edges:
            edge = reduce(IGEdge.merged,
                          (node.edges.pop(n, empty_e) for n in source_nodes))
            if edge == empty_e:
                node.edges.pop(retval, None)
            else:
                node.edges[retval] = edge
        for node in source_nodes:
            del self.__map[node.merged_ssa_val]
        self.__map[final_merged_ssa_val] = retval
        for ssa_val in final_merged_ssa_val.ssa_vals:
            self.__merged_ssa_val_map[ssa_val] = final_merged_ssa_val
        return retval

    def __repr__(self, repr_state=None):
        # type: (None | IGNodeReprState) -> str
        if repr_state is None:
            repr_state = IGNodeReprState()
        s = ",\n".join(v.__repr__(repr_state) for v in self.__map.values())
        return f"MergedSSAValToIGNodeMap({{{s}}})"


@plain_data(frozen=True, repr=False)
@final
class InterferenceGraph:
    __slots__ = "fn_analysis", "merged_ssa_val_map", "nodes"

    def __init__(self, fn_analysis, merged_ssa_vals):
        # type: (FnAnalysis, Iterable[MergedSSAVal]) -> None
        self.fn_analysis = fn_analysis
        self.merged_ssa_val_map = SSAValToMergedSSAValMap()
        self.nodes = self.merged_ssa_val_map.ig_node_map
        for i in merged_ssa_vals:
            self.nodes.add_node(i)

    def merge_preview(self, ssa_val1, ssa_val2, additional_offset=0):
        # type: (SSAVal, SSAVal, int) -> MergedSSAVal
        merged1 = self.merged_ssa_val_map[ssa_val1]
        merged2 = self.merged_ssa_val_map[ssa_val2]
        merged = merged1.with_offset_to_match(ssa_val1)
        return merged.merged(merged2.with_offset_to_match(
            ssa_val2, additional_offset=additional_offset)).normalized()

    def merge(self, ssa_val1, ssa_val2, additional_offset=0):
        # type: (SSAVal, SSAVal, int) -> IGNode
        return self.nodes.merge_into_one_node(self.merge_preview(
            ssa_val1=ssa_val1, ssa_val2=ssa_val2,
            additional_offset=additional_offset))

    def copy_merge(self, node1, node2):
        # type: (IGNode, IGNode) -> IGNode
        return self.nodes.merge_into_one_node(node1.copy_merge_preview(node2))

    def local_colorability_score(self, node, merged_in_copy=None):
        # type: (IGNode, None | IGNode) -> int
        """ returns a positive integer if node is locally colorable, returns
        zero or a negative integer if node isn't known to be locally
        colorable, the more negative the value, the less colorable.

        if `merged_in_copy` is not `None`, then the node used is what would be
        the result of `self.copy_merge(node, merged_in_copy)`.
        """
        if node.ignored:
            raise ValueError(
                "can't get local_colorability_score of ignored node")
        loc_set = node.loc_set
        edges = node.edges
        if merged_in_copy is not None:
            if merged_in_copy.ignored:
                raise ValueError(
                    "can't get local_colorability_score of ignored node")
            loc_set = node.copy_merge_preview(merged_in_copy).loc_set
            edges = edges.copy()
            for neighbor, edge in merged_in_copy.edges.items():
                edges[neighbor] = edge.merged(edges.get(neighbor))
        retval = len(loc_set)
        for neighbor, edge in edges.items():
            if neighbor.ignored or not edge.interferes:
                continue
            if neighbor == merged_in_copy or neighbor == node:
                continue
            retval -= loc_set.max_conflicts_with(neighbor.loc_set)
        return retval

    @staticmethod
    def minimally_merged(fn_analysis):
        # type: (FnAnalysis) -> InterferenceGraph
        retval = InterferenceGraph(fn_analysis=fn_analysis, merged_ssa_vals=())
        for op in fn_analysis.fn.ops:
            for inp in op.input_uses:
                if inp.unspread_start != inp:
                    retval.merge(inp.unspread_start.ssa_val, inp.ssa_val,
                                 additional_offset=inp.reg_offset_in_unspread)
            for out in op.outputs:
                retval.nodes.add_node(MergedSSAVal(fn_analysis, out))
                if out.unspread_start != out:
                    retval.merge(out.unspread_start, out,
                                 additional_offset=out.reg_offset_in_unspread)
                if out.tied_input is not None:
                    retval.merge(out.tied_input.ssa_val, out)
        return retval

    def __repr__(self, repr_state=None):
        # type: (None | IGNodeReprState) -> str
        if repr_state is None:
            repr_state = IGNodeReprState()
        s = self.nodes.__repr__(repr_state)
        return f"InterferenceGraph(nodes={s}, <...>)"

    def dump_to_dot(
            self, highlighted_nodes=(),  # type: Container[IGNode]
            node_scores=None,  # type: None | dict[IGNode, int]
            edge_scores=None,  # type: None | dict[tuple[IGNode, IGNode], int]
    ):
        # type: (...) -> str

        def quote(s):
            # type: (object) -> str
            s = str(s)
            s = s.replace('\\', r'\\')
            s = s.replace('"', r'\"')
            s = s.replace('\n', r'\n')
            return f'"{s}"'

        if node_scores is None:
            node_scores = {}
        if edge_scores is None:
            edge_scores = {}

        edges = {}  # type: dict[tuple[IGNode, IGNode], IGEdge]
        node_ids = {}  # type: dict[IGNode, str]
        for node in self.nodes.values():
            node_ids[node] = quote(node.node_id)
            for neighbor, edge in node.edges.items():
                edge_key = (node, neighbor)
                # ensure we only insert each edge once by checking for
                # both directions
                if edge_key not in edges and edge_key[::-1] not in edges:
                    edges[edge_key] = edge
        lines = [
            "graph {",
            "    graph [pack = true]",
        ]
        for node, node_id in node_ids.items():
            label_lines = []  # type: list[str]
            score = node_scores.get(node)
            if score is not None:
                label_lines.append(f"score={score}")
            for k, v in node.merged_ssa_val.ssa_val_offsets.items():
                label_lines.append(f"{k}: {v}")
            label = quote("\n".join(label_lines))
            style = "dotted" if node.ignored else "solid"
            color = "black"
            if node in highlighted_nodes:
                style = "bold"
                color = "green"
            style = quote(style)
            color = quote(color)
            lines.append(f"    {node_id} ["
                         f"label = {label}, "
                         f"style = {style}, "
                         f"color = {color}]")

        def append_edge(node1, node2, label, color, style):
            # type: (IGNode, IGNode, str, str, str) -> None
            label = quote(label)
            color = quote(color)
            style = quote(style)
            lines.append(f"    {node_ids[node1]} -- {node_ids[node2]} ["
                         f"label = {label}, "
                         f"color = {color}, "
                         f"style = {style}, "
                         f"decorate = true]")
        for (node1, node2), edge in edges.items():
            score = edge_scores.get((node1, node2))
            if score is None:
                score = edge_scores.get((node2, node1))
            label_prefix = ""
            if score is not None:
                label_prefix = f"score={score}\n"
            if edge.interferes:
                append_edge(node1, node2, label=label_prefix + "interferes",
                            color="darkred", style="bold")
            if edge.copy_relation is not None:
                append_edge(node1, node2, label=label_prefix + "copy related",
                            color="blue", style="dashed")
        lines.append("}")
        return "\n".join(lines)


@plain_data(repr=False)
class IGNodeReprState:
    __slots__ = "did_full_repr",

    def __init__(self):
        super().__init__()
        self.did_full_repr = OSet()  # type: OSet[IGNode]


@plain_data(frozen=True, unsafe_hash=True)
@final
class IGEdge:
    """ interference graph edge """
    __slots__ = "interferes", "copy_relation"

    def __init__(self, interferes=False, copy_relation=None):
        # type: (bool, None | _CopyRelation) -> None
        self.interferes = interferes
        self.copy_relation = copy_relation

    def merged(self, other):
        # type: (IGEdge | None) -> IGEdge
        if other is None:
            return self
        copy_relation = self.copy_relation
        if copy_relation is None:
            copy_relation = other.copy_relation
        interferes = self.interferes | other.interferes
        return IGEdge(interferes=interferes, copy_relation=copy_relation)


@final
class IGNode:
    """ interference graph node """
    __slots__ = "node_id", "merged_ssa_val", "edges", "loc", "ignored"

    def __init__(self, node_id, merged_ssa_val, edges, loc, ignored):
        # type: (int, MergedSSAVal, dict[IGNode, IGEdge], Loc | None, bool) -> None
        self.node_id = node_id
        self.merged_ssa_val = merged_ssa_val
        self.edges = edges
        self.loc = loc
        self.ignored = ignored

    def merge_edge(self, other, edge):
        # type: (IGNode, IGEdge) -> None
        if self == other:
            raise ValueError("can't have self-loops")
        old_edge = self.edges.get(other, None)
        assert old_edge is other.edges.get(self, None), "inconsistent edges"
        edge = edge.merged(old_edge)
        if edge == IGEdge():
            self.edges.pop(other, None)
            other.edges.pop(self, None)
        else:
            self.edges[other] = edge
            other.edges[self] = edge

    def __eq__(self, other):
        # type: (object) -> bool
        if isinstance(other, IGNode):
            return self.node_id == other.node_id
        return NotImplemented

    def __hash__(self):
        # type: () -> int
        return hash(self.node_id)

    def __repr__(self, repr_state=None, short=False):
        # type: (None | IGNodeReprState, bool) -> str
        rs = repr_state
        del repr_state
        if rs is None:
            rs = IGNodeReprState()
        if short or self in rs.did_full_repr:
            return f"<IGNode #{self.node_id}>"
        rs.did_full_repr.add(self)
        edges = ", ".join(
            f"{k.__repr__(rs, True)}: {v}" for k, v in self.edges.items())
        return (f"IGNode(#{self.node_id}, "
                f"merged_ssa_val={self.merged_ssa_val}, "
                f"edges={{{edges}}}, "
                f"loc={self.loc}, "
                f"ignored={self.ignored})")

    @property
    def loc_set(self):
        # type: () -> LocSet
        return self.merged_ssa_val.loc_set

    def loc_conflicts_with_neighbors(self, loc):
        # type: (Loc) -> bool
        for neighbor, edge in self.edges.items():
            if not edge.interferes:
                continue
            if neighbor.loc is not None and neighbor.loc.conflicts(loc):
                return True
        return False

    def copy_merge_preview(self, rhs_node):
        # type: (IGNode) -> MergedSSAVal
        try:
            copy_relation = self.edges[rhs_node].copy_relation
        except KeyError:
            raise ValueError("nodes aren't copy related")
        if copy_relation is None:
            raise ValueError("nodes aren't copy related")
        return self.merged_ssa_val.copy_merged(
            lhs_loc=self.loc,
            rhs=rhs_node.merged_ssa_val, rhs_loc=rhs_node.loc,
            copy_relation=copy_relation)


class AllocationFailedError(Exception):
    def __init__(self, msg, node, interference_graph):
        # type: (str, IGNode, InterferenceGraph) -> None
        super().__init__(msg, node, interference_graph)
        self.node = node
        self.interference_graph = interference_graph

    def __repr__(self, repr_state=None):
        # type: (None | IGNodeReprState) -> str
        if repr_state is None:
            repr_state = IGNodeReprState()
        return (f"{__class__.__name__}({self.args[0]!r}, "
                f"node={self.node.__repr__(repr_state, True)}, "
                f"interference_graph="
                f"{self.interference_graph.__repr__(repr_state)})")

    def __str__(self):
        # type: () -> str
        return self.__repr__()


def allocate_registers(
    fn,  # type: Fn
    debug_out=None,  # type: TextIO | None
    dump_graph=None,  # type: Callable[[str, str], None] | None
):
    # type: (...) -> dict[SSAVal, Loc]

    # inserts enough copies that no manual spilling is necessary, all
    # spilling is done by the register allocator naturally allocating SSAVals
    # to stack slots
    fn.pre_ra_insert_copies()

    if debug_out is not None:
        print(f"After pre_ra_insert_copies():\n{fn.ops}",
              file=debug_out, flush=True)

    fn_analysis = FnAnalysis(fn)
    interference_graph = InterferenceGraph.minimally_merged(fn_analysis)

    if debug_out is not None:
        print(f"After InterferenceGraph.minimally_merged():\n"
              f"{interference_graph}", file=debug_out, flush=True)

    for i, j in combinations(interference_graph.nodes.values(), 2):
        copy_relation = i.merged_ssa_val.get_copy_relation(j.merged_ssa_val)
        i.merge_edge(j, IGEdge(copy_relation=copy_relation))

    for pp, ssa_vals in fn_analysis.live_at.items():
        live_merged_ssa_vals = OSet()  # type: OSet[MergedSSAVal]
        for ssa_val in ssa_vals:
            live_merged_ssa_vals.add(
                interference_graph.merged_ssa_val_map[ssa_val])
        for i, j in combinations(live_merged_ssa_vals, 2):
            if i.loc_set.max_conflicts_with(j.loc_set) == 0:
                continue
            node_i = interference_graph.nodes[i]
            node_j = interference_graph.nodes[j]
            if node_j in node_i.edges:
                if node_i.edges[node_j].copy_relation is not None:
                    try:
                        _ = node_i.copy_merge_preview(node_j)
                        continue  # doesn't interfere if copy merging succeeds
                    except BadMergedSSAVal:
                        pass
            node_i.merge_edge(node_j, edge=IGEdge(interferes=True))
        if debug_out is not None:
            print(f"processed {pp} out of {fn_analysis.all_program_points}",
                  file=debug_out, flush=True)

    if debug_out is not None:
        print(f"After adding interference graph edges:\n"
              f"{interference_graph}", file=debug_out, flush=True)
    if dump_graph is not None:
        dump_graph("initial", interference_graph.dump_to_dot())

    node_stack = []  # type: list[IGNode]

    debug_node_scores = {}  # type: dict[IGNode, int]
    debug_edge_scores = {}  # type: dict[tuple[IGNode, IGNode], int]

    def find_best_node(has_copy_relation):
        # type: (bool) -> None | IGNode
        best_node = None  # type: None | IGNode
        best_score = 0
        for node in interference_graph.nodes.values():
            if node.ignored:
                continue
            node_has_copy_relation = False
            for neighbor, edge in node.edges.items():
                if neighbor.ignored:
                    continue
                if edge.copy_relation is not None:
                    node_has_copy_relation = True
                    break
            if node_has_copy_relation != has_copy_relation:
                continue
            score = interference_graph.local_colorability_score(node)
            debug_node_scores[node] = score
            if best_node is None or score > best_score:
                best_node = node
                best_score = score
            if best_score > 0:
                # it's locally colorable, no need to find a better one
                break
        if debug_out is not None:
            print(f"find_best_node(has_copy_relation={has_copy_relation}):\n"
                  f"{best_node}", file=debug_out, flush=True)
        return best_node
    # copy-merging algorithm based on Iterated Register Coalescing, section 5:
    # https://dl.acm.org/doi/pdf/10.1145/229542.229546
    # Build step is above.
    for step in count():
        debug_node_scores.clear()
        debug_edge_scores.clear()
        # Simplify:
        best_node = find_best_node(has_copy_relation=False)
        if best_node is not None:
            if dump_graph is not None:
                dump_graph(
                    f"step_{step}_simplify", interference_graph.dump_to_dot(
                        highlighted_nodes=[best_node],
                        node_scores=debug_node_scores,
                        edge_scores=debug_edge_scores))
            node_stack.append(best_node)
            best_node.ignored = True
            continue
        # Coalesce (aka. do copy-merges):
        did_any_copy_merges = False
        for node in interference_graph.nodes.values():
            if node.ignored:
                continue
            for neighbor, edge in node.edges.items():
                if neighbor.ignored:
                    continue
                if edge.copy_relation is None:
                    continue
                try:
                    score = interference_graph.local_colorability_score(
                        node, merged_in_copy=neighbor)
                except BadMergedSSAVal:
                    continue
                if (neighbor, node) in debug_edge_scores:
                    debug_edge_scores[(neighbor, node)] = score
                else:
                    debug_edge_scores[(node, neighbor)] = score
                if score > 0:  # merged node is locally colorable
                    if dump_graph is not None:
                        dump_graph(
                            f"step_{step}_copy_merge",
                            interference_graph.dump_to_dot(
                                highlighted_nodes=[node, neighbor],
                                node_scores=debug_node_scores,
                                edge_scores=debug_edge_scores))
                    if debug_out is not None:
                        print(f"\nCopy-merging:\n{node}\nwith:\n{neighbor}",
                              file=debug_out, flush=True)
                    merged_node = interference_graph.copy_merge(node, neighbor)
                    if dump_graph is not None:
                        dump_graph(
                            f"step_{step}_copy_merge_result",
                            interference_graph.dump_to_dot(
                                highlighted_nodes=[merged_node]))
                    if debug_out is not None:
                        print(f"merged_node:\n"
                              f"{merged_node}", file=debug_out, flush=True)
                    did_any_copy_merges = True
                    break
            if did_any_copy_merges:
                break
        if did_any_copy_merges:
            continue
        # Freeze:
        best_node = find_best_node(has_copy_relation=True)
        if best_node is not None:
            if dump_graph is not None:
                dump_graph(f"step_{step}_freeze",
                           interference_graph.dump_to_dot(
                               highlighted_nodes=[best_node],
                               node_scores=debug_node_scores,
                               edge_scores=debug_edge_scores))
            # no need to clear copy relations since best_node won't be
            # considered since it's now ignored.
            node_stack.append(best_node)
            best_node.ignored = True
            continue
        break

    if dump_graph is not None:
        dump_graph("final", interference_graph.dump_to_dot())
    if debug_out is not None:
        print(f"After deciding node allocation order:\n"
              f"{node_stack}", file=debug_out, flush=True)

    retval = {}  # type: dict[SSAVal, Loc]

    while len(node_stack) > 0:
        node = node_stack.pop()
        if node.loc is not None:
            if node.loc_conflicts_with_neighbors(node.loc):
                raise AllocationFailedError(
                    "IGNode is pre-allocated to a conflicting Loc",
                    node=node, interference_graph=interference_graph)
        else:
            # Locs to try allocating, ordered from most preferred to least
            # preferred
            locs = OSet()
            # prefer eliminating copies
            for neighbor, edge in node.edges.items():
                if neighbor.loc is None or edge.copy_relation is None:
                    continue
                try:
                    merged = node.copy_merge_preview(neighbor)
                except BadMergedSSAVal:
                    continue
                # get merged_loc if merged.loc_set has a single Loc
                merged_loc = merged.only_loc
                if merged_loc is None:
                    continue
                ssa_val = node.merged_ssa_val.first_ssa_val
                ssa_val_loc = merged_loc.get_subloc_at_offset(
                    subloc_ty=ssa_val.ty,
                    offset=merged.ssa_val_offsets[ssa_val])
                node_loc = ssa_val_loc.get_superloc_with_self_at_offset(
                    superloc_ty=node.merged_ssa_val.ty,
                    offset=node.merged_ssa_val.ssa_val_offsets[ssa_val])
                assert node_loc in node.merged_ssa_val.loc_set, "logic error"
                locs.add(node_loc)
            # add node's allowed Locs as fallback
            for loc in node.loc_set:
                # TODO: add in order of preference
                locs.add(loc)
            # pick the first non-conflicting register in locs, since locs is
            # ordered from most preferred to least preferred register.
            for loc in locs:
                if not node.loc_conflicts_with_neighbors(loc):
                    node.loc = loc
                    break
            if node.loc is None:
                raise AllocationFailedError(
                    "failed to allocate Loc for IGNode",
                    node=node, interference_graph=interference_graph)

        if debug_out is not None:
            print(f"After allocating Loc for node:\n{node}",
                  file=debug_out, flush=True)

        for ssa_val, offset in node.merged_ssa_val.ssa_val_offsets.items():
            retval[ssa_val] = node.loc.get_subloc_at_offset(ssa_val.ty, offset)

    if debug_out is not None:
        print(f"final Locs for all SSAVals:\n{retval}",
              file=debug_out, flush=True)

    return retval
