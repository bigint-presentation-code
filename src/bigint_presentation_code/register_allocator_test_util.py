import unittest
import shutil

from nmutil.get_test_path import get_test_path
from bigint_presentation_code.type_util import final


@final
class GraphDumper:
    def __init__(self, test_case):
        # type: (unittest.TestCase) -> None
        self.test_case = test_case
        self.base_path = get_test_path(test_case, "dumped_graphs")
        shutil.rmtree(self.base_path, ignore_errors=True)
        self.base_path.mkdir(parents=True, exist_ok=True)

    def __call__(self, name, dot):
        # type: (str, str) -> None
        path = self.base_path / name
        dot_path = path.with_suffix(".dot")
        dot_path.write_text(dot)
