from enum import Enum, unique
from fractions import Fraction
import operator
from typing import Any, Callable, Generic, Iterable, Iterator, Type, TypeVar

from bigint_presentation_code.type_util import final

_T = TypeVar("_T")
_T2 = TypeVar("_T2")


@final
@unique
class SpecialMatrix(Enum):
    Zero = 0
    Identity = 1


@final
class Matrix(Generic[_T]):
    __slots__ = "__height", "__width", "__data", "__element_type"

    @property
    def height(self):
        # type: () -> int
        return self.__height

    @property
    def width(self):
        # type: () -> int
        return self.__width

    @property
    def element_type(self):
        # type: () -> Type[_T]
        return self.__element_type

    def __init__(self, height, width, data=SpecialMatrix.Zero,
                 element_type=Fraction):
        # type: (int, int, Iterable[_T | int | Any] | SpecialMatrix, Type[_T]) -> None
        if width < 0 or height < 0:
            raise ValueError("matrix size must be non-negative")
        self.__height = height
        self.__width = width
        self.__element_type = element_type
        if isinstance(data, SpecialMatrix):
            self.__data = [element_type(0) for _ in range(height * width)]
            if data is SpecialMatrix.Identity:
                for i in range(min(width, height)):
                    self[i, i] = element_type(1)
            else:
                assert data is SpecialMatrix.Zero
        else:
            self.__data = [element_type(v) for v in data]
            if len(self.__data) != height * width:
                raise ValueError("data has wrong length")

    def cast(self, element_type):
        # type: (Type[_T2]) -> Matrix[_T2]
        data = self  # type: Iterable[Any]
        return Matrix(self.height, self.width, data, element_type=element_type)

    def __idx(self, row, col):
        # type: (int, int) -> int
        if 0 <= col < self.width and 0 <= row < self.height:
            return row * self.width + col
        raise IndexError()

    def __getitem__(self, row_col):
        # type: (tuple[int, int]) -> _T
        row, col = row_col
        return self.__data[self.__idx(row, col)]

    def __setitem__(self, row_col, value):
        # type: (tuple[int, int], _T | int) -> None
        row, col = row_col
        self.__data[self.__idx(row, col)] = self.__element_type(value)

    def copy(self):
        # type: () -> Matrix[_T]
        return Matrix(self.width, self.height, data=self.__data,
                      element_type=self.element_type)

    def indexes(self):
        # type: () -> Iterable[tuple[int, int]]
        for row in range(self.height):
            for col in range(self.width):
                yield row, col

    def __mul__(self, rhs):
        # type: (_T | int) -> Matrix[_T]
        retval = self.copy()
        for i in self.indexes():
            retval[i] *= rhs  # type: ignore
        return retval

    def __rmul__(self, lhs):
        # type: (_T | int) -> Matrix[_T]
        retval = self.copy()
        for i in self.indexes():
            retval[i] = lhs * retval[i]  # type: ignore
        return retval

    def __truediv__(self, rhs):
        # type: (_T | int) -> Matrix[_T]
        retval = self.copy()
        for i in self.indexes():
            retval[i] /= rhs  # type: ignore
        return retval

    def __matmul__(self, rhs):
        # type: (Matrix[_T]) -> Matrix[_T]
        if self.width != rhs.height:
            raise ValueError(
                "lhs width must equal rhs height to multiply matrixes")
        retval = Matrix(self.height, rhs.width, element_type=self.element_type)
        for row in range(retval.height):
            for col in range(retval.width):
                sum = self.element_type()
                for i in range(self.width):
                    sum += self[row, i] * rhs[i, col]  # type: ignore
                retval[row, col] = sum
        return retval

    def __rmatmul__(self, lhs):
        # type: (Matrix[_T]) -> Matrix[_T]
        return lhs.__matmul__(self)

    def __elementwise_bin_op(self, rhs, op):
        # type: (Matrix[_T], Callable[[_T | int, _T | int], _T | int]) -> Matrix[_T]
        if self.height != rhs.height or self.width != rhs.width:
            raise ValueError(
                "matrix dimensions must match for element-wise operations")
        retval = self.copy()
        for i in retval.indexes():
            retval[i] = op(retval[i], rhs[i])
        return retval

    def __add__(self, rhs):
        # type: (Matrix[_T]) -> Matrix[_T]
        return self.__elementwise_bin_op(rhs, operator.add)

    def __radd__(self, lhs):
        # type: (Matrix[_T]) -> Matrix[_T]
        return lhs.__add__(self)

    def __sub__(self, rhs):
        # type: (Matrix[_T]) -> Matrix[_T]
        return self.__elementwise_bin_op(rhs, operator.sub)

    def __rsub__(self, lhs):
        # type: (Matrix[_T]) -> Matrix[_T]
        return lhs.__sub__(self)

    def __iter__(self):
        # type: () -> Iterator[_T]
        return iter(self.__data)

    def __reversed__(self):
        # type: () -> Iterator[_T]
        return reversed(self.__data)

    def __neg__(self):
        # type: () -> Matrix[_T]
        retval = self.copy()
        for i in retval.indexes():
            retval[i] = -retval[i]  # type: ignore
        return retval

    def __repr__(self):
        # type: () -> str
        if self.height == 0 or self.width == 0:
            return f"Matrix(height={self.height}, width={self.width})"
        lines = []  # type: list[str]
        line = []  # type: list[str]
        for row in range(self.height):
            line.clear()
            for col in range(self.width):
                el = self[row, col]
                if isinstance(el, Fraction) and el.denominator == 1:
                    line.append(str(el.numerator))
                else:
                    line.append(repr(el))
            lines.append(", ".join(line))
        lines_str = ",\n    ".join(lines)
        element_type = ""
        if self.element_type is not Fraction:
            element_type = f"element_type={self.element_type}, "
        return (f"Matrix(height={self.height}, width={self.width}, "
                f"{element_type}data=[\n"
                f"    {lines_str},\n])")

    def __eq__(self, rhs):
        # type: (Matrix[Any] | Any) -> bool
        if not isinstance(rhs, Matrix):
            return NotImplemented
        return (self.height == rhs.height
                and self.width == rhs.width
                and self.__data == rhs.__data
                and self.element_type == rhs.element_type)

    def inverse(self  # type: Matrix[Fraction]
                ):
        # type: () -> Matrix[Fraction]
        size = self.height
        if size != self.width:
            raise ValueError("can't invert a non-square matrix")
        if self.element_type is not Fraction:
            raise TypeError("can't invert a matrix with element_type that "
                            "isn't Fraction")
        inp = self.copy()
        retval = Matrix(size, size, data=SpecialMatrix.Identity)
        # the algorithm is adapted from:
        # https://rosettacode.org/wiki/Gauss-Jordan_matrix_inversion#C
        for k in range(size):
            f = abs(inp[k, k])  # Find pivot.
            p = k
            for i in range(k + 1, size):
                g = abs(inp[k, i])
                if g > f:
                    f = g
                    p = i
            if f == 0:
                raise ZeroDivisionError("Matrix is singular")
            if p != k:  # Swap rows.
                for j in range(k, size):
                    f = inp[j, k]
                    inp[j, k] = inp[j, p]
                    inp[j, p] = f
                for j in range(size):
                    f = retval[j, k]
                    retval[j, k] = retval[j, p]
                    retval[j, p] = f
            f = 1 / inp[k, k]  # Scale row so pivot is 1.
            for j in range(k, size):
                inp[j, k] *= f
            for j in range(size):
                retval[j, k] *= f
            for i in range(size):  # Subtract to get zeros.
                if i == k:
                    continue
                f = inp[k, i]
                for j in range(k, size):
                    inp[j, i] -= inp[j, k] * f
                for j in range(size):
                    retval[j, i] -= retval[j, k] * f
        return retval
